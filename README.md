# ALT-L

A repository of code connected to London International Academy's Advanced Learning Techniques course. 

**Advanced Learning Techniques at LIA**

*A short course integrating computer programming with mathematics and science.*

**Background and Rationale**

The potentialities of the human mind are limitless! The aim of this collection of brief courses is to generate a learning community that assists participants to appreciate the power of the human mind and to find practical ways to develop it. School curricula have become fragmented and disorganized over time, leaving many students without a sense of purpose or motivation. Educational research has amply demonstrated that learning may be accelerated and deepened by assisting students to appreciate a few core concepts known as “keystone” concepts, which link to many other ideas within a particular discipline. This course aims to identify and develop such keystone concepts in mathematics and science using computer programming as a catalyst.  Participants will be well prepared to excel in computer science, mathematics and physics courses at the secondary school level.   

A potential list of topics to be covered in the course are as follows:
* Introduction to Computer Programming in Python
    * Variables
    * Operators
    * Mathematical Expressions
    * Conditional Statements
    * Loops
    * Functions
    * Data Structures
    * Classes
    * Graphing
    * Mathematical Modelling
* Computer Algebra and Geometry
    * Using Lists to Represent Polynomial Functions
    * Solving Equations
    * Solving Systems of Equations
    * Plotting Points and Lines
* The Concept of the Limit 
    * Calculating Slopes
    * Calculating Slopes as two Points Converge
    * Developing the Rules of Differential Calculus Through Pattern Recognition
    * Generating Limit Graphs and Animations
*The Concept of the Area Under A Curve
    *Adding Areas of Rectangles
    *Adding Areas of Ever More Rectangles
    *Finding Areas Under Curves
    *Finding Areas of Solids
*Applications in Physics
    *Using Integration to Simplify Physics Formulas

Specific activities under the above headings are to be selected in consultation with the participants in the course. 

