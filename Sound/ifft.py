#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 11:35:57 2024

@author: wneal
"""

import numpy as np
from scipy.io import wavfile
from scipy.signal import chirp, spectrogram

# Function to reverse FFT from frequency and intensity pairs
def reverse_fft(freqs, intensities, duration, sampling_rate):
    # Calculate the number of samples
    num_samples = int(duration * sampling_rate)
    
    # Initialize the signal in frequency domain
    signal_freq_domain = np.zeros(num_samples, dtype=np.complex64)
    
    # Fill the signal with frequency and intensity information
    for freq, intensity in zip(freqs, intensities):
        if intensity > 0:
            signal_freq_domain[int(freq * num_samples)] = intensity
    
    # Inverse FFT to get the time-domain signal
    signal_time_domain = np.fft.ifft(signal_freq_domain)
    
    # Normalize to ensure it fits within the 16-bit range
    signal_time_domain = np.int16(signal_time_domain.real / np.max(np.abs(signal_time_domain.real)) * 32767)
    
    return signal_time_domain

# Example usage
if __name__ == "__main__":
    # Example frequency and intensity pairs
    frequencies = [100, 200, 300, 400, 500]
    intensities = [1, 0.5, 0.3, 0.2, 0.1]
    
    # Duration of the signal in seconds
    duration = 1.0
    
    # Sampling rate (samples per second)
    sampling_rate = 44100
    
    # Reverse FFT to get the time-domain signal
    signal = reverse_fft(frequencies, intensities, duration, sampling_rate)
    
    # Save the signal as a .wav file
    wavfile.write("output1.wav", sampling_rate, signal)
    
    print("Output saved as output1.wav")