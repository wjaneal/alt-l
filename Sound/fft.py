import numpy as np
import scipy.io.wavfile as wavfile

def fft_wav_file(filename):
    # Read the .wav file
    sample_rate, data = wavfile.read(filename)
    
    # Take one channel if stereo
    if len(data.shape) > 1:
        data = data[:, 0]
    
    # Calculate the FFT
    fft_result = np.fft.fft(data)
    
    # Compute the frequencies corresponding to the FFT result
    n = len(data)
    freq = np.fft.fftfreq(n, d=1/sample_rate)
    
    return freq, fft_result

# Example usage:
filename = 'example.wav'
freq, fft_result = fft_wav_file(filename)

# Print the frequencies and corresponding FFT result
for f, mag in zip(freq, fft_result):
    print(f, mag)