#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 19:00:13 2024

@author: wneal
"""
from matplotlib.pyplot import *
import numpy as np
import wave
import pandas as pd

def read_wav_file(filename):
    with wave.open(filename, 'rb') as wf:
        # Get basic information
        sample_width = wf.getsampwidth()
        framerate = wf.getframerate()
        nframes = wf.getnframes()
        
        # Read audio data
        frames = wf.readframes(nframes)
        
    # Convert binary data to numpy array
    dtype_map = {1: np.int8, 2: np.int16, 4: np.int32}
    dtype = dtype_map[sample_width]
    audio_data = np.frombuffer(frames, dtype=dtype)
    
    return audio_data, framerate

def fft_audio_data(audio_data, framerate):
    n = len(audio_data)
    T = 1.0 / framerate
    
    # Compute the frequencies
    freqs = np.fft.fftfreq(n, T)[:n//2]
    
    # Compute the FFT
    fft_vals = np.fft.fft(audio_data)
    fft_vals = 2.0/n * np.abs(fft_vals[:n//2])
    
    return freqs, fft_vals

def fft_wav_file(filename):
    # Read the .wav file
    audio_data, framerate = read_wav_file(filename)
    
    # Perform FFT on audio data
    freqs, fft_vals = fft_audio_data(audio_data, framerate)
    
    return freqs, fft_vals

# Example usage:
filename = 'synthPiano.wav'
f = open('fft_output_2.txt',"w")
freqs, fft_vals = fft_wav_file(filename)
plotData=[]

# Print the frequencies and corresponding FFT values
for freq, val in zip(freqs, fft_vals):
    if freq < 3000 and freq > 20:
        plotData.append([freq,val])
    #text = str(freq)+" "+str(val)
    #f.write(text)
f.close()
df = pd.DataFrame(plotData, columns = ['Frequency', 'Intensity']) 
plott = df.plot.scatter(x='Frequency', y='Intensity', title= "Frequency Profile");
plott
