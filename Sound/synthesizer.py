#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 10:40:10 2024

@author: wneal
"""

from math import *
from random import *
from matplotlib.pyplot import *
import numpy as np
import wave
import pandas as pd
from scipy.io import wavfile
from scipy.signal import chirp, spectrogram


baseIntensity = 800
baseFrequency = 440
harmonicProfile = [1, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.025, 0.0125]
#harmonicProfile = [0.1, 0.3, 0.5, 0.7, 0.9, 0.7, 0.5, 0.3, 0.1]
#harmonicProfile = [1]

packetCurve = 'exponential'
frequencyCollection = []
frequencies = []
intensities = []
packetWidth = 0.001


# Function to reverse FFT from frequency and intensity pairs
def reverse_fft(freqs, intensities, duration, sampling_rate):
    # Calculate the number of samples
    num_samples = int(duration * sampling_rate)
    
    # Initialize the signal in frequency domain
    signal_freq_domain = np.zeros(num_samples, dtype=np.complex64)
    
    # Fill the signal with frequency and intensity information
    for freq, intensity in zip(freqs, intensities):
        if intensity > 0:
            try:
                signal_freq_domain[int(freq * num_samples)] = intensity
            except:
                print(freq)
    
    # Inverse FFT to get the time-domain signal
    signal_time_domain = np.fft.ifft(signal_freq_domain)
    
    # Normalize to ensure it fits within the 16-bit range
    signal_time_domain = np.int16(signal_time_domain.real / (np.max(np.abs(signal_time_domain.real+0.00000001))) * 32767)
    
    return signal_time_domain


def exponential(x, width, offset):
    return exp(-width*(x-offset)*(x-offset))

def randomSpread(spread):
    return spread*random()-spread/2

samples = 20000
minFrequency = 200
maxFrequency = 3000
k = 0.005

for f in range(samples):
    currentFrequency = minFrequency +(maxFrequency-minFrequency)*(f/samples)
    currentIntensity = 0
    for harmonic in range(len(harmonicProfile)):
        harmonicOffset = baseFrequency+harmonic*baseFrequency
        currentIntensity += baseIntensity*harmonicProfile[harmonic]*exponential(currentFrequency, packetWidth, harmonicOffset)
    frequencyCollection.append([currentFrequency, currentIntensity])
    frequencies.append(currentFrequency)
    intensities.append(currentIntensity)                           

'''
for f in range(samples):
    currentFrequency = minFrequency +(maxFrequency-minFrequency)*(f/samples)
    currentIntensity = baseIntensity*exp(-k*f*10)+randomSpread(80)*exp(-k*f/100)
    for harmonic in range(len(harmonicProfile)):
        harmonicOffset = baseFrequency+harmonic*baseFrequency
        currentIntensity += baseIntensity*harmonicProfile[harmonic]*exponential(currentFrequency, packetWidth, harmonicOffset)
    harmonicOffset = baseFrequency+harmonic*baseFrequency
    currentIntensity += baseIntensity*harmonicProfile[harmonic]*exponential(currentFrequency, packetWidth, harmonicOffset)
    if currentIntensity < 0:
        currentIntensity = 0
    frequencyCollection.append([currentFrequency, currentIntensity])
    frequencies.append(currentFrequency)
    intensities.append(currentIntensity)  
'''

df = pd.DataFrame(frequencyCollection, columns = ['Frequency', 'Intensity']) 
plott = df.plot.scatter(x='Frequency', y='Intensity', title= "Frequency Profile");
plott

# Duration of the signal in seconds
duration = 1
    
# Sampling rate (samples per second)
sampling_rate = 44100
    
# Reverse FFT to get the time-domain signal
#signal = reverse_fft(frequencies, intensities, duration, sampling_rate)

num_samples = int(duration * sampling_rate)

# Initialize the signal in frequency domain
signal_freq_domain = np.zeros(num_samples, dtype=np.complex64)

for freq, intensity in zip(frequencies, intensities):
     if intensity > 0:
             signal_freq_domain[int(freq*duration)] = intensity
         


signal_time_domain = np.fft.ifft(signal_freq_domain)
    
# Normalize to ensure it fits within the 16-bit range
signal = np.int16(signal_time_domain.real / np.max(np.abs(signal_time_domain.real)) * 32767)

# Save the signal as a .wav file
wavfile.write("synthPiano.wav", sampling_rate, signal)
    
