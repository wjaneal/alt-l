
#Chess game - main program - kivy display
from random import random
from kivy.app import App 
from kivy.uix.button import Button
from kivy.uix.widget import Widget
from kivy.graphics import Color, Line, Rectangle
from kivy.uix.filechooser import FileChooserListView, FileChooserIconView
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import Image
from kivy.graphics.svg import Svg
from kivy.config import Config
from kivy.uix.widget import Widget, ListProperty
from kivy.graphics.instructions import Callback
from kivy.uix.floatlayout import FloatLayout
from chessGame import *
#Set the dimensions of the main window:
Config.set('graphics', 'width', '1200')
Config.set('graphics', 'height', '1200')



class chessBoard(FloatLayout):
    def __init__(self, **kwargs):
        super(chessBoard, self).__init__(**kwargs)
        self.board = []
        self.boardSize = 100
        self.size=(8*self.boardSize,8*self.boardSize)
        
        self.boardx = self.boardSize
        self.boardy = self.boardSize
        self.game = chessGame()
        self.pieceImages = {1:"wp.png",2:"wr.png",3:"wkn.png",4:"wb.png",5:"wq.png",6:"wk.png",7:"bp.png",8:"br.png",9:"bkn.png",10:"bb.png",11:"bq.png",12:"bk.png"}
        self.setBoard()
    def setBoard(self):
        for j in range(0,8):
            for i in range(0,8):
                if (i+j) % 2 == 0:
                    with self.canvas:
                         if self.game.highlights[j][i] == 0:
                            Rectangle(source="black.png",size=(self.boardSize,self.boardSize),pos=(i*self.boardSize,j*self.boardSize))
                         else:
                            Rectangle(source="green.png",size=(self.boardSize,self.boardSize),pos=(i*self.boardSize,j*self.boardSize))
                         #Callback(self.board_pressed(i,j))
                else:
                    with self.canvas:
                        if self.game.highlights[j][i] == 0:
                            Rectangle(source="white.png",size=(self.boardSize,self.boardSize),pos=(i*self.boardSize,j*self.boardSize))
                        else:
                            Rectangle(source="green.png",size=(self.boardSize,self.boardSize),pos=(i*self.boardSize,j*self.boardSize))
                        #Callback(self.board_pressed(i,j))
        for row in range(0,8):
            for col in range(0,8):
                if self.game.board[row][col] != 0:
                    cb = CustomBtn(source=self.pieceImages[self.game.board[row][col]],pos=(self.boardy*col,self.boardx*row),size=(50,50), size_hint=(0.125,0.125)) 
            	    #self.board.append(Rectangle(source=self.pieceImages[self.game.board[row][col]],size=(self.boardx,self.boardy),pos=(self.boardy*col,self.boardx*row))) 
                    #cb.bind(pressed=self.btn_pressed)
                    self.add_widget(cb)
    def on_touch_down(self, touch):
         col = int(touch.x/self.boardSize)
         row = int(touch.y/self.boardSize)
         self.game.click(row,col)
         self.setBoard()	     
    '''def btn_pressed(self, instance, pos):
         col=int(pos[1]/100)
         row=int(pos[0]/100)
         print(row,col)
         self.game.click(row,col)
         print(self.game.board)
         self.setBoard()
         print ('pos: printed from root widget: {pos}'.format(pos=pos))
         '''
                

class CustomBtn(Image):
     pressed = ListProperty([0, 0])
     def __init__(self,**kwargs):
         super(CustomBtn, self).__init__(**kwargs)
         self.selected = 0
     def on_touch_down(self, touch):
         if self.collide_point(*touch.pos):
             if self.selected == 0:
                 self.selected = touch.pos
             else:
                 print(self.selected)
             self.pressed = touch.pos
             return True
         return super(CustomBtn, self).on_touch_down(touch)

     def on_pressed(self, instance, pos):
         print ('pressed at {pos}'.format(pos=pos))


     def chessMove(self, position):
         print("Now moving piece at ", position)
         return("green.png")
        
class ChessApp(App):
    def build(self):
        return chessBoard()


if __name__ == '__main__':
    ChessApp().run()
