#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 16:22:28 2024

@author: wneal
"""

class Chessboard:
    def __init__(self, board):
        self.board = board

    def is_valid_position(self, row, col):
        return 0 <= row < 8 and 0 <= col < 8

    def get_opponent_color(self, color):
        return 'black' if color == 'white' else 'white'

    def get_pawn_moves(self, row, col, color):
        moves = []
        opponent_color = self.get_opponent_color(color)

        if color == 'white':
            direction = 1
        else:
            direction = -1

        # Check diagonal captures
        for d_col in [-1, 1]:
            new_row = row + direction
            new_col = col + d_col
            if self.is_valid_position(new_row, new_col):
                piece_at_target = self.board[new_row][new_col]
                if piece_at_target is not None and piece_at_target.startswith(opponent_color[0]):
                    moves.append((new_row, new_col))

        # Check if moving forward one square is possible
        new_row = row + direction
        new_col = col
        if self.is_valid_position(new_row, new_col) and self.board[new_row][new_col] is None:
            moves.append((new_row, new_col))

        return moves

    def suggest_next_move(self, color):
        best_move = None
        best_value = -float('inf') if color == 'white' else float('inf')

        for row in range(8):
            for col in range(8):
                piece = self.board[row][col]
                if piece is not None and piece.startswith(color[0]):  # It's a pawn of the correct color
                    pawn_moves = self.get_pawn_moves(row, col, color)
                    for move in pawn_moves:
                        move_row, move_col = move
                        # Here you can add more sophisticated logic to evaluate the "best" move
                        # For simplicity, we'll just return the first valid move found
                        return (row, col), move_row, move_col

        return None  # No valid move found


# Example usage:
if __name__ == "__main__":
    # Example chessboard setup
    initial_board = [
        [None, None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None, None],
        [None, None, None, None, None, None, None, None]
    ]

    chessboard = Chessboard(initial_board)

    # Add pieces to the chessboard (for example)
    chessboard.board[1][3] = 'wp'  # White pawn at (1, 3)
    chessboard.board[6][2] = 'bp'  # Black pawn at (6, 2)

    # Example of suggesting a move for white pawns
    pawn_start, move_row, move_col = chessboard.suggest_next_move('white')
    if pawn_start is not None and (move_row, move_col) is not None:
        print(f"Suggested move for white pawn at {pawn_start}: Move to ({move_row}, {move_col})")
    else:
        print("No valid move found.")