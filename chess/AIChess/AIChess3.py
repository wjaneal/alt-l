#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 16:26:02 2024

@author: wneal
"""

class Chessboard:
    def __init__(self, board):
        self.board = board

    def is_valid_position(self, row, col):
        return 0 <= row < 8 and 0 <= col < 8

    def get_opponent_color(self, color):
        return 'black' if color == 'white' else 'white'

    def get_piece_value(self, piece):
        if piece is None:
            return 0
        elif piece.endswith('p'):
            return 1
        elif piece.endswith('r'):
            return 5
        elif piece.endswith('n'):
            return 3
        elif piece.endswith('b'):
            return 3
        elif piece.endswith('q'):
            return 9
        else:
            return 0  # Should not happen in a standard chess game

    def get_all_moves(self, color):
        moves = []
        for row in range(8):
            for col in range(8):
                piece = self.board[row][col]
                if piece is not None and piece.startswith(color[0]):
                    piece_moves = self.get_possible_moves(row, col, color)
                    for move in piece_moves:
                        moves.append(((row, col), move))
        return moves

    def get_possible_moves(self, row, col, color):
        moves = []
        piece = self.board[row][col]

        if piece.endswith('p'):
            moves.extend(self.get_pawn_moves(row, col, color))
        # Add more piece move calculations here (rooks, knights, bishops, queens, kings)

        return moves

    def get_pawn_moves(self, row, col, color):
        moves = []
        opponent_color = self.get_opponent_color(color)

        if color == 'white':
            direction = 1
        else:
            direction = -1

        # Check diagonal captures
        for d_col in [-1, 1]:
            new_row = row + direction
            new_col = col + d_col
            if self.is_valid_position(new_row, new_col):
                piece_at_target = self.board[new_row][new_col]
                if piece_at_target is not None and piece_at_target.startswith(opponent_color[0]):
                    moves.append((new_row, new_col))

        # Check if moving forward one square is possible
        new_row = row + direction
        new_col = col
        if self.is_valid_position(new_row, new_col) and self.board[new_row][new_col] is None:
            moves.append((new_row, new_col))

        return moves

    def evaluate_move(self, start_pos, end_pos, color):
        # Evaluate the move based on prioritizing checks and attacking most valuable pieces
        # This function can be expanded based on specific evaluation criteria
        start_row, start_col = start_pos
        end_row, end_col = end_pos

        start_piece = self.board[start_row][start_col]
        end_piece = self.board[end_row][end_col]

        # Priority 1: Check opponent's king if possible
        # (This requires more sophisticated logic involving checking for checkmate)

        # Priority 2: Attack the most valuable piece if possible
        if end_piece is not None and end_piece.startswith(self.get_opponent_color(color)[0]):
            return self.get_piece_value(end_piece)

        # Priority 3: Move a valuable piece if no attack is possible
        return self.get_piece_value(start_piece)

    def suggest_next_move(self, color):
        best_move = None
        best_value = -float('inf') if color == 'white' else float('inf')

        all_moves = self.get_all_moves(color)
        
        for start_pos, end_pos in all_moves:
            move_value = self.evaluate_move(start_pos, end_pos, color)
            if (color == 'white' and move_value > best_value) or (color == 'black' and move_value < best_value):
                best_value = move_value
                best_move = (start_pos, end_pos)

        return best_move


# Example usage:
if __name__ == "__main__":
    # Example chessboard setup
    initial_board = [
        ['br', 'bn', 'bb', 'bq', 'bk', 'bb', 'bn', 'br'],
        ['bp', 'bp', 'bp', 'bp', None, 'bp', 'bp', 'bp'],
        [None, None, None, None, 'bp', None, None, None],
        [None, None, None, 'wp', None, None, None, None],
        [None, None, None, None, 'wp', None, None, None],
        [None, None, None, None, None, None, None, None],
        ['wp', 'wp', 'wp', 'wp', None, 'wp', 'wp', 'wp'],
        ['wr', 'wn', 'wb', 'wq', 'wk', 'wb', 'wn', 'wr']
    ]

    chessboard = Chessboard(initial_board)

    # Example of suggesting a move for white
    best_move = chessboard.suggest_next_move('white')
    if best_move is not None:
        start_pos, end_pos = best_move
        print(f"Suggested move for white: Move {start_pos} to {end_pos}")
    else:
        print("No valid move found.") 