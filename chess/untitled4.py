#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 15:31:42 2020

@author: wneal
"""

'''Binary search program

(1) Make a binary search program in a class.
(a) One method, binary, should take a list of sorted items and use a binary technique 
to find an item in that list. 
The method should return the number of steps required to find the item.
(b) One method, setList, should generate a sorted list of n numeric (or text, if you like) items
(c) The main method should do the following:
    0 - Generate the list
    i - Select an item from the list at random.
    ii - Search for that item using a binary technique
    iii - record the number of steps taken to find the item
    iv - Find the average number of searches taken in x trials.
    v - Display the number of items, the number of trials and 
    the average number of search steps taken.
'''
from random import*

class searchProgram:
    def __init__(self):
        self.list = []
        
    def listset(self):
        self.list=[]
        for i in range(0,20):
            k=int(random()*100)
            self.list.append(k)
            #insertion sort
        for i in range (1,len(self.list)):
            storevalue=self.list[i]
            while self.list[i-1]>storevalue and i>0:
                self.list[i]=self.list[i-1]
                i-=1
            self.list[i]=storevalue
            #select a random item in list
        n=int(random()*(len(self.list)))
        self.x=self.list[n]
        
    def binarySearch(self,a,b,x,list1):
#select an item from the list at random
        #list=self.list
        #x=self.x
        #lenList=len(list)
#binary search
#Compare x with the middle element.
        nMid=(b-a)//2+a
        print(a,b, nMid,len(list1))

        midList=list1[nMid]
        self.step=1
#If x matches with middle element, we return the mid index.
#Else If x is greater than the mid element, then x can only lie in right half subarray after the mid element. So we recur for right half.
#Else (x is smaller) recur for the left half
        if x==midList:
            return nMid
        elif x>midList:
            a=midList
            return self.binarySearch(a,b,x,list1)
            self.step+=1
        elif x<midList:
            b=midList
            return self.binarySearch(a,b,x,list1)
            self.step+=1
        else:#prevent errors
            return "Element not found!"
        return self.step
    
    def mainloop(self,n):
        self.stepNumber=0
        self.listset()
        for i in range(0,n):
            self.binarySearch(0,len(self.list),self.x,self.list)
            self.stepNumber+=self.step
        averageStepNumber=(self.stepNumber)/n
        print("number of items:",len(self.list),"number of trials:",n,"average step number",averageStepNumber)
        

r=searchProgram()
r.mainloop(5)