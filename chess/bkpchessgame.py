
class chessGame():
    def __init__(self):
	self.board = [[0 for i in range(8)]for j in range(8)]
	self.board[0] = [2,3,4,5,6,4,3,2]
	self.board[1] = [1,1,1,1,1,1,1,1]
	self.board[6] = [7,7,7,7,7,7,7,7]
	self.board[7] = [8,9,10,12,11,10,9,8]
	self.turn = 0
	self.highlighted=False
        self.selected = [-1,-1]
        self.highlights = [[0 for i in range(8)] for j in range(8)]
        self.lastMoveColumn = -1
        self.lastMoveRow = -1

    def pawn(self,row,column,board):
        print("Selecting moves for a pawn")
        #If white pieces:
        direction = -int(((self.turn%2)-0.5)*2)
        print(direction, " direction")
	if self.turn % 2 in range(0,2): #Modified to handle both white and black pieces
            print("Now moving pawn")
            #Check for first move
            if row == int(3.5-2.5*direction):
                if board[row+direction][column] == 0:
                    self.highlights[row+direction][column] = 1
                    if column == 0:
                        if board[row+direction][column+1] in range(1+6*((self.turn+1)%2),7+6*((self.turn+1)%2)):
                            self.highlights[row+direction][column+1] = 1
		    elif column == 7:
                        if board[row+direction][column-1] in range(1+6*((self.turn+1)%2),7+6*((self.turn+1)%2)):
                            self.highlights[row+direction][column-1] = 1
                    else:
                        if board[row+direction][column-1] in range(1+6*((self.turn+1)%2),7+6*((self.turn+1)%2)):
                            self.highlights[row+direction][column-1] = 1
                        if board[row+direction][column+1] in range(1+6*((self.turn+1)%2),7+6*((self.turn+1)%2)):
                            self.highlights[row+direction][column+1] = 1
                    if board[row+2*direction][column] == 0:
                        self.highlights[row+2*direction][column] = 1
            #Moves on other squares
            elif row in range(int(1+direction),int(6+direction)):
            #Check pawn moves on edges and in centre of board:
                if column == 0:
                    if board[row+direction][column] == 0:
                        self.highlights[row+direction][column] = 1
                    if board[row+direction][column+1] in range(1+6*((self.turn+1)%2),7+6*((self.turn+1)%2)):
                        self.highlights[row+direction][column+1] = 1
                elif column == 7:
                    if board[row+direction][column-1] in range(1+6*((self.turn+1)%2),7+6*((self.turn+1)%2)):
                        self.higlights[row+direction][column-1] = 1
                    if board[row+direction][column] == 0:
                        self.highlights[row+direction][column] = 1
                else:
                    if board[row+direction][column-1] in range(1+6*((self.turn+1)%2),7+6*((self.turn+1)%2)):
                        self.highlights[row+direction][column-1] = 1
                    if board[row+direction][column] == 0:
                        self.highlights[row+direction][column] = 1
                    if board[row+direction][column+1] in range(1+6*((self.turn+1)%2),7+6*((self.turn+1)%2)):
                        self.highlights[row+direction][column+1] = 1
                print("EP Test ", int(3.5+direction/2.0))
                if abs(self.lastMoveColumn-column) == 1 and self.lastMoveRow == int(3.5+direction/2.0):
                     print("En Passant Possible", 4+3*direction)
                     if board[row][self.lastMoveColumn] == int(4+3*direction):
                         self.highlights[row+direction][self.lastMoveColumn] = 2
        '''else:
            print("Now checking for black pawn",row," ",column)
            #Check for first move
            if row == 6:
                if self.board[row-1][column] == 0:
                    self.highlights[row-1][column] = 1
                    if self.board[row-2][column] == 0:
                        self.highlights[row-2][column] = 1
                if column == 0:
                    if self.board[row-1][column+1] in range(1,7):
                        self.highlights[row-1][column+1] = 1
		elif column == 7:
                    if self.baord[row-1][column+1] in range(1,7):
                        self.highlights[row-1][column+1] = 1
                else:
                    if self.board[row-1][column-1] in range(1,7):
                        self.highlights[row-1][column-1] = 1
                    if self.board[row-1][column+1] in range(1,7):
                        self.highlights[row-1][column+1] = 1
		
            #Moves on other squares
            elif row <= 6 and row > 0:
            #Check pawn moves on edges and in centre of board:
                if column == 0:
                    if self.board[row-1][column] == 0:
                        self.highlights[row-1][column] = 1
                    if self.board[row-1][column+1] in range(1,7):
                        self.highlights[row-1][column+1] = 1
                elif column == 7:
                    if self.board[row-1][column-1] in range(1,7):
                        self.highlights[row-1][column-1] = 1
                    if self.board[row-1][column] == 0:
                        self.highlights[row-1][column] = 1
                else: 
                    if self.board[row-1][column-1] in range(1,7):
                        self.highlights[row-1][column-1] = 1
                    if self.board[row-1][column] == 0:
                        self.highlights[row-1][column] = 1
                    if self.board[row-1][column+1] in range(1,7):
                        self.highlights[row-1][column+1] = 1
                if abs(self.lastMoveColumn-column)==1 and self.lastMoveRow == 3:
                    print("En Passant Possible")
                    if self.board[row][self.lastMoveColumn] == 1:
                        self.highlights[row-1][self.lastMoveColumn] = 2
           '''     
        

    def select(self,row,column):
        print("Now selecting")
        #If the piece is a pawn:
	if self.board[row][column] == 1 or self.board[row][column] == 7:
            print("The piece is a pawn")
            self.pawn(row,column,self.board)
        self.selected = [row,column]

    def resetHighlight(self):
        self.highlights = [[0 for i in range(8)] for j in range(8)]
               

    def move(self,row,column,piece):
        print("Moving...")
        #Determine the direction of movement (for pawns)
        direction = int(((self.turn%2)-0.5)*2)
        #Check if selected square is highlighted and empty
        if self.highlights[row][column]==2:
            #This is en passant.
            #Place the new pawn
            print (self.board, self.selected)
            self.board[row][column] = self.board[self.selected[0]][self.selected[1]]
            #Capture the other pawn using the direction as a reference
	    self.board[row+direction][column] = 0
            #Move the pawn out of its square
            self.board[self.selected[0]][self.selected[1]] = 0
 	    self.turn+=1
            self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            return
        if self.highlights[row][column]==1:
            print("Empty square")
            #Place the piece in a new location:
            self.board[row][column] = self.board[self.selected[0]][self.selected[1]]
            #Remove the piece from its location:
            self.board[self.selected[0]][self.selected[1]] = 0
            self.turn+=1
            self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            return
        #If it is an opposing piece, remove it:
        elif self.board[row][column] in range(1+6*self.turn%2,7+6*self.turn%2):
            self.board[row][column] = self.board[self.selected[0]][self.selected[1]]
            self.turn+=1
            self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            return
        else:
            print("Passing")

    def getState(self,):
	return board

    def click(self, row,col):
        print("Clicked; row:",row," column:",col)
	if self.highlighted==False:
	    #Check if the click is on a valid piece:
            print("Highlighting")
            if self.board[row][col] in range(1+6*(self.turn%2),7+6*(self.turn%2)):
                #The clicked piece is the right colour
                print("Now selecting:",row," ",col)
                self.select(row, col)
                #Check whether there are any valid moves.
                for i in range(0,8):
                    for j in range(0,8):
                        if self.highlights[i][j] != 0:
                            self.highlighted = True
                if self.highlighted==False:
                    print("Please try again")
            else:
                print("Wrong colour")
	else:
            if self.highlights[row][col] != 0:
            	print("Moving")
            	print(self.highlights)
            	self.move(row,col,self.board[row][col])
            	self.highlighted = False
            	self.selected = [-1,-1]
            else:
                print("Please click a valid square")
        return self.board
