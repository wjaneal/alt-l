import copy
#import chessai #The ai functions are stored in this module   
from random import *
######################################################################################################################
#Major redesign of core functionality of chess game to have potential moves stored in an appended list format 
#rather than in a grid. 
######################################################################################################################


######################################################################################################################
#This is the class for a chess move.  It stores data about the move for use in AI strategy
######################################################################################################################

class chessMove:
    
    def __init__(self,initialSquare,finalSquare,capture,value):
        self.initialSquare = initialSquare
        self.finalSquare = finalSquare
        self.capture = capture
        self.value = value
        self.castle = False
        self.enPassant = False
        self.fork = False
        self.pin = False
        self.checkMate = False
        self.check = False
        self.pawnQueen = False
        self.piece = None
        
    def displayDetails(self):
        print("Initial Square: ", self.initialSquare, "Final Square: ", self.finalSquare)
    
######################################################################################################################
#This is the class for a chess game
######################################################################################################################

class chessGame():
    ######################################################################################################################
    #The constructor method initializes the chessboard as an 8x8 two dimensional list
    ######################################################################################################################
    def __init__(self):
        self.resetBoard()
        #The following variables do not change and do not need to be reset when a game is reset.
        self.pieceLookup = {1:"", 2:"R", 3:"N",4:"B",5:"Q",6:"K",7:"",8:"R",9:"N",10:"B",11:"Q",12:"K"} #Returns the letter of each piece to be used in chess notation.
        self.letterLookup = {0:"a",1:"b",2:"c",3:"d",4:"e",5:"f",6:"g",7:"h"} #Returns the letter of a square used in chess notation.
        self.numGames = 2
        self.numPlayers = 4 #How many players in the tournament?
        self.turnLimit = 200 #Limit the number of moves in AI play
        #The following dictionary seclects a strategy for selecting moves:
        self.playerStrategy = {0:self.selectRandom(self.movesList), 1:self.selectRandom(self.movesList),2:self.selectRandom(self.movesList),3:self.selectRandom(self.movesList),4:self.selectRandom(self.movesList)}
    #*********************************************************************************************************************
    #Utilities
    #*********************************************************************************************************************

    ######################################################################################################################
    #Returns the numeric part of the square in chess notation.
    ######################################################################################################################
    def numberLookup(self,row):
        return str(8-row)
    
    ######################################################################################################################
    #Converts the square from rows and colunms in the list to chess notation.
    ######################################################################################################################
    def squareLookup(self,row,column):
        return self.letterLookup[column]+self.numberLookup(row)
    
    ######################################################################################################################
    #Resets the board for a new game.
    ######################################################################################################################
    def resetBoard(self):
        self.board = [[0 for i in range(8)]for j in range(8)]
        self.board[0] = [2,3,4,5,6,4,3,2]
        self.board[1] = [1,1,1,1,1,1,1,1]
        self.board[6] = [7,7,7,7,7,7,7,7]
        self.board[7] = [8,9,10,11,12,10,9,8]
        self.pieces = []
        self.highlighted=False
        self.selected = [-1,-1]
        self.highlights = [[0 for i in range(8)] for j in range(8)]
        self.lastMoveColumn = -1
        self.lastMoveRow = -1
        self.castled = [0,0]
        self.movesList = []
        self.AIMove = 0
        self.kingLocation=[[0,4],[7,4]]
        self.checkMate = False
        self.inCheck = False
        self.staleMate = False
        self.kingGone = False
        self.moves = ""
        self.pieceValues = {1:1, 2:5, 3:3, 4:3, 5:10, 6:1000, 7:1, 8:5, 9:3, 10:3, 11:10, 12:1000}  #Set 1000 as the arbitrary value for a king.
        self.turn = 1
    ######################################################################################################################
    # Determine whether the king is in check.
    ######################################################################################################################
    
    def checkCheck(self,turn,board):
        possibleMoves = []
        bcopy = copy.deepcopy(board)
        direction = -int((((turn+1)%2)-0.5)*2)
        #print("Direction: **************************************************************",direction, turn)
        #Generate moves from the current board.
        #If the king is in check, return True
        for i in range(0,8): #Scan through each row and column of the chess board. 
            for j in range(0,8):                
                #Check for possible subsequent moves:
                if bcopy[i][j] == 1+6*((turn+1) % 2):  #Is it a pawn?  Add pawn moves
                    possibleMoves = self.pawn(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 2+6*((turn+1) % 2):  #Etc. for rooks, knights, bishops, queens, kings.
                    possibleMoves = self.rook(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 3+6*((turn+1) % 2):
                    possibleMoves = self.knight(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 4+6*((turn+1) % 2):
                    possibleMoves = self.bishop(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 5+6*((turn+1) % 2):
                    possibleMoves = self.queen(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 6+6*((turn+1) % 2):
                    possibleMoves = self.king(i,j,bcopy,possibleMoves,turn+1)
        for m in possibleMoves:  #Check all the possible subsequent moves.  Is the king still in check? Not allowed. Return False
            if bcopy[m.finalSquare[0]][m.finalSquare[1]] == 9+3*direction:
                return True
        return False  #If no threat to the king is found, return true.
  
    #*********************************************************************************************************************
    #Functions for pieces' moves
    #*********************************************************************************************************************

    ######################################################################################################################
    #select: responds to a click to determine which piece is proposed for a move
    #highlights all possible squares to which the piece may move.
    ######################################################################################################################
    def pawn(self,row,column,board,possibleMoves,turn):
        #Determine the direction based on the current turn:
        #print(row,column, "possible moves: ",len(possibleMoves))
        #print("Row, column: ", row, column)
        direction = -int(((turn%2)-0.5)*2)
        #Check for first move
        if row == int(3.5-2.5*direction): #This is the first time to move the pawn
            if board[row+direction][column] == 0 and board[row+2*direction][column] == 0:  #Move twice:
                move = chessMove([row,column],[row+2*direction,column], False, 0)
                possibleMoves.append(move)
        #Check for other move - not first move
        if row == int(3.5+2.5*direction):  #This pawn is about to queen
            pawnQueen = True
        else:
            pawnQueen = False
        #Forward Moves:
        try:
            if board[row+direction][column] == 0:
                move = chessMove([row,column],[row+1*direction,column],False, 0)
                move.pawnQueen = pawnQueen
                possibleMoves.append(move)
        except:
            print("Range Error")
        #Diagonal Moves, including en passant:
        if column == 0:  #Check first and last column separately to avoid range errors:
            if board[row+direction][column+1] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                move = chessMove([row,column],[row+direction,column+1],True, self.pieceValues[board[row+direction][column+1]])
                move.pawnQueen = pawnQueen
                possibleMoves.append(move)
            if board[row+direction][column+1] == 0:  #The space is empty
                if board[row][column+1] == 3.5+2.5*direction:  #An opposite pawn is beside...
                    move = chessMove([row,column],[row+direction,column+1],True, 1)
                    move.enPassant = True
                    possibleMoves.append(move)

        elif column == 7: #Avoiding range errors in the last column 
            if board[row+direction][column-1] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                
                move = chessMove([row,column],[row+direction,column-1],True, self.pieceValues[board[row+direction][column-1]])
                move.pawnQueen = pawnQueen
                possibleMoves.append(move)
            if board[row+direction][column-1] == 0:  #The space is empty
                if board[row][column-1] == 3.5+2.5*direction:  #An opposite pawn is beside...
                    move = chessMove([row,column],[row+direction,column-1],True, 1)
                    move.enPassant = True
                    possibleMoves.append(move)
        else:  #In the inner columns, check in both directions.
            try:
                if board[row+direction][column-1] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                    move = chessMove([row,column],[row+direction,column-1],True, self.pieceValues[board[row+direction][column-1]])
                    move.pawnQueen = pawnQueen
                    possibleMoves.append(move)
                    if board[row+direction][column-1] == 0:  #The space is empty
                        if board[row][column-1] == 3.5+2.5*direction:  #An opposite pawn is beside...
                            move = chessMove([row,column],[row+direction,column-1],True, 1)
                            move.enPassant = True  
                            possibleMoves.append(move)

                if board[row+direction][column+1] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                    move = chessMove([row,column],[row+direction,column+1],True, self.pieceValues[board[row+direction][column+1]])
                    move.pawnQueen = pawnQueen
                    possibleMoves.append(move)
                    if board[row+direction][column+1] == 0:  #The space is empty
                        if board[row][column+1] == 3.5+2.5*direction:  #An opposite pawn is beside...
                            move = chessMove([row,column],[row+direction,column+1],True, 1)
                            move.enPassant = True
                            possibleMoves.append(move)
            except:
                print("Range Error!")
        #print("Possible pawn moves: ",len(possibleMoves))
        return possibleMoves

    ######################################################################################################################
    #knight: uses a nested loop to determine possible moves 
    ######################################################################################################################
    def knight(self,row,column,board,possibleMoves,turn):
        xyrange = [-2,-1,1,2]  #Set the possible movements in the x (and y) directions
        for x in xyrange: #Iterate through all possibilities
            for y in xyrange:
                if abs(x)!=abs(y):#knights always move twice in one direction and once in the other...
                    if row+y in range(0,8) and column+x in range(0,8):
                        if board[row+y][column+x] == 0:
                            move = chessMove([row, column],[row+y,column+x],False,0)  #No piece in the way.
                            possibleMoves.append(move)
                        if board[row+y][column+x] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)): #Opposing piece in the way.
                            move = chessMove([row, column],[row+y,column+x], True,self.pieceValues[board[row+y][column+x]])
                            possibleMoves.append(move)
        #print("Knight Moves: ",possibleMoves)
        return possibleMoves
    ######################################################################################################################
    #bishop:  uses a nested loop and direction vectors to find possible moves.
    ######################################################################################################################
    def bishop(self,row,column,board,possibleMoves,turn):
        done = 0
        for xdir in range(-1,3,2):
            for ydir in range(-1,3,2):  #This nested loop selects (-1,-1), (-1,1), (1,-1) and (1,1) as direction vectors
                done = 0
                step = 1
                while done == 0:
                    if row+step*xdir in range(0,8) and column+step*ydir in range(0,8):  #Make sure to stay on the board!
                        if board[row+step*xdir][column+step*ydir] == 0:  #An empty square has been reached.
                            move = chessMove([row,column],[row+step*xdir,column+step*ydir],False, 0)
                            possibleMoves.append(move)
                        if board[row+step*xdir][column+step*ydir] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)): #An opposing piece is in the way.
                            move = chessMove([row,column],[row+step*xdir,column+step*ydir],True,self.pieceValues[board[row+step*xdir][column+step*ydir]])
                            possibleMoves.append(move)
                            done = 1
                        if board[row+step*xdir][column+step*ydir] in range(1+6*((turn)%2),7+6*((turn)%2)): #A friendly piece is in the way
                            done = 1
                    else:
                        done = 1  #We have gone off the board. 
                    if board[row][column] == 6 or board[row][column] == 12:
                        done = 1 #This is a king - only one level required
                    else: 
                        step +=1 #Increase the number of steps until a piece is found or the end of the board is reached
        #print("Bishop moves: ",possibleMoves)
        return possibleMoves



    ######################################################################################################################
    #rook: check horizontal and vertical directions separately.
    ######################################################################################################################
    def rook(self,row,column,board,possibleMoves,turn):
        #print("Now moving rook")
        done = 0
        for xdir in range(-1,3,2):
            done = 0
            step = 1
            while done == 0:
                if row+step*xdir in range(0,8):  #Be sure to stay on the board
                    if board[row+step*xdir][column] == 0:  #There is no piece in the way
                        move = chessMove([row,column],[row+step*xdir,column], False, 0)
                        possibleMoves.append(move)
                    if board[row+step*xdir][column] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)): #There is an opposing piece in the way
                        move = chessMove([row,column],[row+step*xdir,column],True,self.pieceValues[board[row+step*xdir][column]])
                        possibleMoves.append(move)
                        done = 1
                    if board[row+step*xdir][column] in range(1+6*((turn)%2),7+6*((turn)%2)): #There is a friendly piece in the way
                        done = 1
                else:
                    done = 1 #We moved off the board
                if board[row][column] == 6 or board[row][column] == 12:
                    done = 1 #This is a king - only one level required
                else: 
                    step +=1

        for ydir in range(-1,3,2):
            done = 0
            step = 1
            while done == 0:
                if column+step*ydir in range(0,8):
                    if board[row][column+step*ydir] == 0:
                        move = chessMove([row,column],[row,column+step*ydir],False,0)
                        possibleMoves.append(move)
                    if board[row][column+step*ydir] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                        move = chessMove([row,column],[row,column+step*ydir],True,self.pieceValues[board[row][column+step*ydir]])
                        possibleMoves.append(move)
                        done = 1
                    if board[row][column+step*ydir] in range(1+6*((turn)%2),7+6*((turn)%2)):
                        done = 1
                else:
                    done = 1
                if board[row][column] == 6 or board[row][column] == 12:
                    done = 1 #This is a king - only one level required
                else: 
                    step +=1
        #print("Rook moves:", possibleMoves)
        return possibleMoves


    ######################################################################################################################
    #queen - a combination of rook and bishop moves
    ######################################################################################################################
    def queen(self,row,column,board,possibleMoves,turn):
        #print("Now moving queen")
        possibleMoves=self.bishop(row,column,board,possibleMoves,turn)
        possibleMoves=self.rook(row,column,board,possibleMoves,turn)
        return possibleMoves

    ######################################################################################################################
    #king - a queen with moves of length 1
    ######################################################################################################################
    def king(self,row,column,board,possibleMoves,turn):
        #print("Now moving king")
        possibleMoves = self.bishop(row,column,board,possibleMoves,turn)
        possibleMoves = self.rook(row,column,board,possibleMoves,turn)
        #Check for castling:
        #Determine the direction of movement based on the current turn:
        direction = -int(((self.turn%2)-0.5)*2)
        #print(direction, " direction for castling")
        if self.castled[int(direction/2.0+0.5)] == 0: #Check if castling has already happened or been ruled out
            #print("Castle:", direction, row, column)
            if row == int(3.5-3.5*direction) and column == 4: #Check that this is the right king
                #print(row, int(3.5-3.5*direction), "check row")
                #Short side castle:
                if board[row][column+1] == 0 and board[row][column+2]==0: #Check for two empty squares
                    move = chessMove([row,column],[row,column+2],False,0)
                    move.castle = True
                    possibleMoves.append(move)
                    #print("Set the castling highlight for the ",direction," king")
                #Long side castle:
                if board[row][column-1] == 0 and board[row][column-2]==0 and board[row][column-3]==0: #Check for three empty squares
                    move = chessMove([row,column],[row,column-2],False,0)   
                    move.castle = True
                    possibleMoves.append(move)
        #print("Highlights - moving king - ",highlights)
        return possibleMoves


    #*********************************************************************************************************************
    #Functions for executing a game.
    #*********************************************************************************************************************
    
    
    ######################################################################################################################
    #Checks if a given move is allowed and returns true or false.
    ######################################################################################################################
    
    def moveAllowed(self, turn, board, move):
        #print("In-check Status: ",self.inCheck )
        #print("Checking if the move is allowed", move.displayDetails())
        possibleMoves = []
        bcopy = copy.deepcopy(board)
        direction = -int((((turn+1)%2)-0.5)*2)
        if not(move.castle == True and self.inCheck == True):  #Do not allow castling if the king is in check
            bcopy[move.finalSquare[0]][move.finalSquare[1]] = bcopy[move.initialSquare[0]][move.initialSquare[1]]
            bcopy[move.initialSquare[0]][move.initialSquare[1]] = 0
        #Is this en Passant? - Extra piece does not matter....
        if move.enPassant == True:
            #print("En Passant: ",move.initialSquare,move.finalSquare)
            bcopy[move.initialSquare[0]][move.finalSquare[1]] = 0 #Remove the opposing pawn
        else:
            pass
            #print("Other move:", move.initialSquare,move.finalSquare)
        #Is this castling? Only allowed if the king is not in check or will not be in check afterward.
        if move.castle == True:
            if self.inCheck == True:
                pass
            else:
                rookValue = 5-3*direction  #Determine which rook - white (2) or black (8)
                #Move the rook too; determine the final location by moving it one square away from the king.
                if move.finalSquare[1] > 3.5:
                    directionFactor = 1
                else:
                    directionFactor = -1
                bcopy[move.finalSquare[0]][move.finalSquare[1]-directionFactor] = rookValue
                
        for i in range(0,8): #Scan through each row and column of the chess board. 
            for j in range(0,8):                
                #Check for possible subsequent moves:
                if bcopy[i][j] == 1+6*((turn+1) % 2):  #Is it a pawn?  Add pawn moves
                    possibleMoves = self.pawn(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 2+6*((turn+1) % 2):  #Etc. for rooks, knights, bishops, queens, kings.
                    possibleMoves = self.rook(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 3+6*((turn+1) % 2):
                    possibleMoves = self.knight(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 4+6*((turn+1) % 2):
                    possibleMoves = self.bishop(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 5+6*((turn+1) % 2):
                    possibleMoves = self.queen(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 6+6*((turn+1) % 2):
                    possibleMoves = self.king(i,j,bcopy,possibleMoves,turn+1)
        #print("King code: ",9+3*direction)
        for m in possibleMoves:  #Check all the possible subsequent moves.  Is the king still in check? Not allowed. Return False
            #print ("Possible moves: ", m.initialSquare, m.finalSquare,bcopy[m.finalSquare[0]][m.finalSquare[1]])
            if bcopy[m.finalSquare[0]][m.finalSquare[1]] == 9+3*direction:
                return False
        return True  #If no threat to the king is found, return true.
        
    ######################################################################################################################
    #Generate Moves - Creates a list of possible moves
    #Move Format: coordsFrom - [rowFrom, columnFrom], coordsTo - [rowTo,columnTo], pieceTaken?, pieceValue?, pawnToQueen?,
    #fork? discovery? unprotected piece? check? checkMate?
    ######################################################################################################################
    def generateMoves(self,turn):
        possibleMoves = []
        for i in range(0,8): #Scan through each row and column of the chess board. 
            for j in range(0,8):
                #print(i,j, "Scanning board")
                if self.board[i][j] == 1+6*(turn % 2):  #Is it a pawn?  Add pawn moves
                    possibleMoves = self.pawn(i,j,self.board,possibleMoves,turn)
                if self.board[i][j] == 2+6*(turn % 2):  #Etc. for rooks, knights, bishops, queens, kings.
                    possibleMoves = self.rook(i,j,self.board,possibleMoves,turn)
                if self.board[i][j] == 3+6*(turn % 2):
                    possibleMoves = self.knight(i,j,self.board,possibleMoves,turn)
                if self.board[i][j] == 4+6*(turn % 2):
                    possibleMoves = self.bishop(i,j,self.board,possibleMoves,turn)
                if self.board[i][j] == 5+6*(turn % 2):
                    possibleMoves = self.queen(i,j,self.board,possibleMoves,turn)
                if self.board[i][j] == 6+6*(turn % 2):
                    possibleMoves = self.king(i,j,self.board,possibleMoves,turn)
        #Now, check if each of these moves is possible...
        #print(len(possibleMoves))
        #print("Here are the possible moves before processing:")
        '''for item in possibleMoves:
           #print("Piece: ",g.board[item.initialSquare[0]][item.initialSquare[1]])
           #item.displayDetails()
           #print("Target", g.board[item.finalSquare[0]][item.finalSquare[1]])''' 
        possibleMovesTracker = [0]*len(possibleMoves)
        for m in range(len(possibleMoves)):
            allowed = self.moveAllowed(turn, self.board,possibleMoves[m])
            #print("Allowed: ", allowed)
            if allowed == False:
                possibleMovesTracker[m] = 1
        pm = []
        for i in range(len(possibleMoves)):
            if possibleMovesTracker[i] == 0:
                pm.append(possibleMoves[i])
        #print("Here is the final list of possible moves: ")
        #for item in pm:
        #    item.displayDetails()
        return pm  #Return the final list of allowed moves. 
    
    #*********************************************************************************************************************
    #A collection of functions for Crude AI strategies
    #*********************************************************************************************************************
    
    ######################################################################################################################
    #selects a random move from the moves list
    ######################################################################################################################
    def selectRandom(self, movesList):
        #print("Here is the current moves list:", movesList)
        if len(movesList)==0:
            return "Error!"
        m=int(random()*len(movesList))
        return movesList[m]
    
    ######################################################################################################################
    #prefers to play with a particular piece if possible
    ######################################################################################################################
    def selectPiece(self,movesList,piece):
        if len(movesList)==0:
            return "Error!"
        piece = False
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            m.piece = self.board[m.initialSquare[0]][m.initialSquare[1]]
            if m.piece in [piece,piece+6]:
                piece = True
                m1.append(m)
        if piece == True:
            pieceMove = int(random()*len(m1))
            return m1[pieceMove]
        else:
            return self.selectRandom(movesList)
    ######################################################################################################################
    #plays peaceful moves if possible
    ######################################################################################################################
    def peaceBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        emptySquareMoves = False
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            m.peace = self.board[m.finalSquare[0]][m.finalSquare[1]]
            if m.peace == 0:
                emptySquareMoves = True
                m1.append(m)
        if emptySquareMoves == True:
            peaceMove = int(random()*len(m1))
            return m1[peaceMove]
        else:
            return self.selectRandom(movesList)
            
    ######################################################################################################################
    #plays capture moves if possible
    ######################################################################################################################
    def warBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        captureMoves = False
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            m.war = self.board[m.finalSquare[0]][m.finalSquare[1]]
            if m.war != 0:  #Go for any non-empty square
                captureMoves = True
                m1.append(m)
        if captureMoves == True:
            peaceMove = int(random()*len(m1))
            return m1[peaceMove]
        else:
            return self.selectRandom(movesList)
    
    
    ######################################################################################################################
    #move: Executes a move
    #Special code for en passant - highlights code is 2
    #Regular code for other moves - highlights code is 1
    ######################################################################################################################
    def move(self,move):
        #print("Moving...",self.selected,row,column,self.board)
        #Determine the direction of movement (for pawns)
        #print(move)
        row = move.initialSquare[0]
        column = move.initialSquare[1]
        rowTo = move.finalSquare[0]
        columnTo = move.finalSquare[1]
        direction = int(((self.turn%2)-0.5)*2)
        if self.turn/2 != int(self.turn/2):
            moveString = str(int(self.turn/2-0.5)+1)+". "
        else:
            moveString = " "
        if self.board[rowTo][columnTo] != 0:
            captureString = "x"
        else:
            captureString = ""
        #Check for castling:
        if move.castle == True:
            #print(self.selected, row,column, self.board[row][column],self.board[self.selected[0]][self.selected[1]], "Test Castling")
            #Move the King:
            self.board[rowTo][columnTo] = self.board[row][column] 
            self.board[row][column] = 0
            self.kingLocation[self.turn%2] = [rowTo,columnTo]
            #Move the rook
            if columnTo == 6:
                self.board[rowTo][5] = self.board[rowTo][7]
                self.board[rowTo][7] = 0
                self.moves+=moveString+"o-o"
            if columnTo == 2:
                self.board[rowTo][3] = self.board[rowTo][0]
                self.board[rowTo][0] = 0
                self.moves+=moveString+"o-o-o"
            self.turn+=1
            #self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = rowTo
            self.lastMoveColumn = columnTo
            return
        #Check for en passant
        if move.enPassant == True:
            #This is en passant.
            #Place the new pawn
            #print (self.board, self.selected)
            self.board[rowTo][columnTo] = self.board[row][column]
            #Capture the other pawn using the direction as a reference
            #print("Testing en passant: ",row,column,direction)
            self.board[row+direction][column] = 0  #Remove the adjacent pawn
            #Move the pawn out of its square
            self.board[row][column] = 0  #Remove the original pawn
            #self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = rowTo
            self.lastMoveColumn = column
            self.moves+=moveString+captureString+str(column)+str(columnTo)+"ep"
            return
        if move.pawnQueen == True:  #At the moment, a queen is assumed.
            self.moves+=moveString+self.pieceLookup[self.board[row][column]]+captureString+self.squareLookup(rowTo,columnTo)+"(Q)"
            #Adjust to include knight, bishop, rook or queen
            self.board[rowTo][columnTo] = 8+3*direction
            self.board[row][column] = 0
            self.turn+=1
            #self.resetHighlight()
            #self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            
        if move.castle == False and move.enPassant == False and move.pawnQueen == False:  #Regular one piece move.
            #print("Empty square")
            #Place the piece in a new location:
            self.moves+=moveString+self.pieceLookup[self.board[row][column]]+captureString+self.squareLookup(rowTo,columnTo)
            self.board[rowTo][columnTo] = self.board[row][column]
            if self.board[rowTo][columnTo] in [6,12]:
                self.kingLocation[self.turn%2] = [rowTo,columnTo]
            #Remove the piece from its location:
            self.board[row][column] = 0
            self.turn+=1
            #self.resetHighlight()
            #self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            return
       
    
    ######################################################################################################################
    #click:  Two possibilities:
    #A - the board is not "highlighted" - call the self.select function to determine possible moves
    #B - the board is "highlighted" and there are possible moves. - call the self.move function to execute the move
    ######################################################################################################################
    '''def click(self, row,col):
        print("Clicked; row:",row," column:",col)
        if self.highlighted==False:
        #Check if the click is on a valid piece:
            print("Highlighting")
            if self.board[row][col] in range(1+6*(self.turn%2),7+6*(self.turn%2)):
                #The clicked piece is the right colour
                print("Now selecting:",row," ",col)
                self.select(row, col)
                #Check whether there are any valid moves.
                for i in range(0,8):
                    for j in range(0,8):
                        if self.highlights[i][j] != 0:
                            self.highlighted = True
                if self.highlighted==False:
                    print("Please try again")
            else:
                print("Wrong colour")
        else:
            if self.highlights[row][col] != 0:
                print("Moving")
                print(self.highlights)
                self.move(row,col,self.board[row][col])
                self.highlighted = False
                self.selected = [-1,-1]
            else:
                print("Please click a valid square")
        return self.board'''
    def selectStrategy(self,movesList,players,turn):
        whichPlayer = (turn-1)%2
        if players[whichPlayer] == 0:
            #return self.warBot(movesList)
            return self.selectPiece(movesList,2)  
        if players[whichPlayer] ==1:
            return self.selectPiece(movesList,3)
        if players[whichPlayer] == 2:
            #return self.selectRandom(movesList)
            return self.warBot(movesList)
        if players[whichPlayer] == 3:           
            return self.peaceBot(movesList)  #Move the king if possible
        
    #*********************************************************************************************************************
    #game - Runs a chess game.
    #*********************************************************************************************************************
    
    def game(self,players):
        turn = 1  #Set the turn to 1 - first move
        gameDone = False
        self.resetBoard() #Reset the board
        while gameDone == False:  #Game is done if checkmate, stalemate or too many moves.
            #print(self.board)
            movesList = []
            #Generate a list of moves.
            movesList = self.generateMoves(turn)
            if len(movesList) == 0:
                self.inCheck = self.checkCheck(turn,self.board) #Is the current player in check?
                if self.inCheck == True:
                    print("This is checkmate!")
                    self.checkMate = True
                    if self.turn % 2 == 0:
                        result = "B"
                    else:
                        result = "W"
                    print(result)
                else:
                    print("This is stalemate!")
                    result = "S"
                gameDone = True
            else:
                #m = self.selectRandom(movesList)
                m = self.selectStrategy(movesList,players,turn)
                self.move(m)
                self.inCheck = False
                self.inCheck = self.checkCheck(turn+1,self.board)  #Is the next player in check?
                if self.inCheck == True:  #********************This is not working and it displays a + for each move.
                    self.moves+="+"
                    #print("+++++++++++++++++++")
            self.moves+=" "
            #print("Turn: ", turn)
            if turn>self.turnLimit-1: #If we have exceeded the maximum number of moves, declare a stalemate
                print("Too many moves - stalemate")
                result = "SM"
                gameDone = True
            turn +=1
        return result


   #*********************************************************************************************************************
    #This is the main loop - runs a chess tournament.
    #*********************************************************************************************************************
    
   
    def tournament(self):
        t = []
        teamNames = ["Musha","Boulee St.","Multicultural","Winners!"]
        standings = [[0,0,0] for i in range(self.numPlayers)]
        for i in range(self.numGames):
            print("Round ",i+1)
            for player1 in range(0,self.numPlayers):
                for player2 in range(0,self.numPlayers):
                    if player1 != player2:         
                        print ("Game between player ", player1," and player ",player2)
                        g = self.game([player1,player2])
                        if g == "S" or g == "SM":
                            standings[player1][2]+=1
                            standings[player2][2]+=1
                        if g == "B":
                            standings[player1][0]+=1
                            standings[player2][1]+=1
                        if g == "W":
                            standings[player2][0]+=1
                            standings[player1][1]+=1
                            t.append("White: "+str(player2)+ " Black: "+str(player1)+" "+g)
                        print ("Game: ", i, player1, player2, self.board,self.moves)
        print ("Player \tWins\tLosses\tDraws")
        print("Standings: ",standings)
        i = 0
        for player in standings:
            print(teamNames[i],standings.index(player),"\t",player[0],"\t",player[1],"\t",player[2])
            i+=1
        return t

#Testing
#g = chessGame()
#g.board = [[0, 0, 0, 0, 0, 0, 6, 0], [0, 0, 0, 0, 0, 11, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [7, 0, 0, 0, 0, 0, 0, 8], [0, 0, 7, 7, 7, 7, 0, 0], [0, 0, 0, 0, 0, 0, 0, 7], [0, 0, 0, 9, 12, 0, 10, 7], [0, 0, 0, 0, 0, 0, 8, 0]]
#print(g.checkCheck(1,g.board))
#print(g.checkCheck(2,g.board))
#g.board = [[0, 0, 0, 2, 0, 2, 0, 0], [0, 0, 11, 0, 0, 0, 4, 0], [0, 0, 6, 0, 0, 3, 1, 0], [0, 0, 0, 0, 0, 0, 0, 1], [7, 0, 0, 0, 7, 0, 8, 7], [3, 0, 9, 0, 0, 0, 10, 0], [0, 0, 10, 0, 0, 7, 0, 0], [0, 0, 0, 9, 0, 12, 0, 0]]
#g.turn = 0
g = chessGame()
t = g.tournament()
print(t)