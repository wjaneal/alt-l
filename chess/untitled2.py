#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 14 10:38:55 2020

@author: wneal
"""
def r(x):
    if x == 1 or x == 0:
        return 1
    else:
        print(x)
        return r(x-1)+r(x-2)

x = 8
print(r(x))