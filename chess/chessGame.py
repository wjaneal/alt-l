
sG = [[[] for i in range(8)]for j in range(8)]
print(sG)

#import chessai #The ai functions are stored in this module   
from chessStrategy import *
import copy # Use in testing

######################################################################################################################
#Major redesign of core functionality of chess game to have potential moves stored in an appended list format 
#rather than in a grid. 
######################################################################################################################


    
######################################################################################################################
#This is the class for a chess game - it extends the chessUtils class which extends the chessBase class.
######################################################################################################################

class chessGame(chessStrategy):
    ######################################################################################################################
    #The constructor method initializes the chessboard as an 8x8 two dimensional list
    ######################################################################################################################
    def __init__(self):
        self.resetBoard()
        #The following variables do not change and do not need to be reset when a game is reset.
        self.pieceLookup = {1:"", 2:"R", 3:"N",4:"B",5:"Q",6:"K",7:"",8:"R",9:"N",10:"B",11:"Q",12:"K"} #Returns the letter of each piece to be used in chess notation.
        self.letterLookup = {0:"a",1:"b",2:"c",3:"d",4:"e",5:"f",6:"g",7:"h"} #Returns the letter of a square used in chess notation.
        self.numGames = 2
        self.numPlayers = 8 #How many players in the tournament?
        self.turnLimit = 200 #Limit the number of moves in AI play
        #The following dictionary selects a strategy for selecting moves:
        self.playerStrategy = {0:self.selectRandom(self.movesList), 1:self.selectRandom(self.movesList),2:self.selectRandom(self.movesList),3:self.selectRandom(self.movesList),4:self.selectRandom(self.movesList)}
    
    
    ######################################################################################################################
    #Resets the board for a new game.
    ######################################################################################################################
    def resetBoard(self):
        self.board = [[0 for i in range(8)]for j in range(8)]
        self.board[0] = [2,3,4,5,6,4,3,2]
        self.board[1] = [1,1,1,1,1,1,1,1]
        self.board[6] = [7,7,7,7,7,7,7,7]
        self.board[7] = [8,9,10,11,12,10,9,8]
        self.strategyGrid = []
        self.pieces = []
        self.highlighted=False
        self.selected = [-1,-1]
        self.lastMoveColumn = -1
        self.lastMoveRow = -1
        self.castled = [0,0]
        self.movesList = []
        self.AIMove = 0
        self.kingLocation=[[0,4],[7,4]]
        self.checkMate = False
        self.inCheck = False
        self.staleMate = False
        self.kingGone = False
        self.moves = ""
        self.pieceValues = {1:1, 2:5, 3:3, 4:3, 5:10, 6:1000, 7:1, 8:5, 9:3, 10:3, 11:10, 12:1000}  #Set 1000 as the arbitrary value for a king.
        self.turn = 1
        self.highlights = [[0 for i in range(8)] for j in range(8)]
    
     
       
    
    ######################################################################################################################
    #click:  Two possibilities:
    #A - the board is not "highlighted" - call the self.select function to determine possible moves
    #B - the board is "highlighted" and there are possible moves. - call the self.move function to execute the move
    ######################################################################################################################
    '''def click(self, row,col):
        print("Clicked; row:",row," column:",col)
        if self.highlighted==False:
        #Check if the click is on a valid piece:
            print("Highlighting")
            if self.board[row][col] in range(1+6*(self.turn%2),7+6*(self.turn%2)):
                #The clicked piece is the right colour
                print("Now selecting:",row," ",col)
                self.select(row, col)
                #Check whether there are any valid moves.
                for i in range(0,8):
                    for j in range(0,8):
                        if self.highlights[i][j] != 0:
                            self.highlighted = True
                if self.highlighted==False:
                    print("Please try again")
            else:
                print("Wrong colour")
        else:
            if self.highlights[row][col] != 0:
                print("Moving")
                print(self.highlights)
                self.move(row,col,self.board[row][col])
                self.highlighted = False
                self.selected = [-1,-1]
            else:
                print("Please click a valid square")
        return self.board'''
        
    #*********************************************************************************************************************
    #game - Runs a chess game.
    #*********************************************************************************************************************
    
    def game(self,players):
        turn = 1  #Set the turn to 1 - first move
        gameDone = False
        self.resetBoard() #Reset the board
        while gameDone == False:  #Game is done if checkmate, stalemate or too many moves.
            #print(self.board)
            movesList = []
            #Generate a list of moves.
            movesList = self.generateMoves(turn,self.board)
            counterMovesList = self.generateMoves(turn+1,self.board)
            
            self.strategyGrid = [[[] for i in range(8)]for j in range(8)]
            self.strategyGrid = self.strategyInfo(self.board,self.strategyGrid, movesList,counterMovesList)
            #print("Strategy Grid:                            ..... ... .  ",self.strategyGrid)
            if len(movesList) == 0:
                self.inCheck = self.checkCheck(turn,self.board) #Is the current player in check?
                if self.inCheck == True:
                    print("This is checkmate!")
                    self.checkMate = True
                    if self.turn % 2 == 0:
                        result = "B"
                    else:
                        result = "W"
                    #print(result)
                else:
                    print("This is stalemate!")
                    result = "S"
                gameDone = True
            else:
                #m = self.selectRandom(movesList)
                m = self.selectStrategy(movesList,players,turn)
                #print(self.board)
                self.board = self.move(m,self.board)
                #print(self.board)
                self.inCheck = False
                self.inCheck = self.checkCheck(turn+1,self.board)  #Is the next player in check?
                if self.inCheck == True:  #********************This is not working and it displays a + for each move.
                    self.moves+="+"
                    #print("+++++++++++++++++++")
            self.moves+=" "
            #print("Turn: ", turn)
            if turn>self.turnLimit-1: #If we have exceeded the maximum number of moves, declare a stalemate
                print("Too many moves - stalemate")
                result = "SM"
                gameDone = True
            turn +=1
            
        return result


   #*********************************************************************************************************************
    #This is the main loop - runs a chess tournament.
    #*********************************************************************************************************************
    
   
    def tournament(self):
        t = []
        #Open a new file for writing
        #teamNames = ["Random Bot","Queens First","Bishop Bot","Knight Bot","Rook Bot","One Man Show!","Pawn Storm"]
        teamNames = ["Differential Warbot","Warbot","Discerning Warbot","Check this!","Randombot","Pawn Storm","Protected Warbot","Safe Warbot"]
        standings = [[0,0,0] for i in range(self.numPlayers)]
        for i in range(self.numGames):
             print("Round ",i+1)
             for player1 in range(0,self.numPlayers):
                for player2 in range(0,self.numPlayers):
                    if player1 != player2:         
                        print ("Round ",i+1," Game between ", teamNames[player1]," and ",teamNames[player2])
                        g = self.game([player1,player2])
                        if g == "S" or g == "SM":
                            standings[player1][2]+=1
                            standings[player2][2]+=1
                            print(teamNames[player1],' ties ',teamNames[player2])
                        if g == "B":
                            standings[player1][0]+=1
                            standings[player2][1]+=1
                            print(teamNames[player1],' defeats ',teamNames[player2])
                        if g == "W":
                            standings[player2][0]+=1
                            standings[player1][1]+=1
                            print(teamNames[player2],' defeats ',teamNames[player1])
                            t.append("White: "+str(player2)+ " Black: "+str(player1)+" "+g)
                        print()
                        print(self.moves)
        print ("Player".ljust(20)+"Wins".ljust(10)+"Losses".ljust(10)+"Draws".ljust(10)+"Points".ljust(10))
        #print("Standings: ",standings)
        i = 0
        for player in standings:
            print(teamNames[i].ljust(20)+str(player[0]).ljust(10)+str(player[1]).ljust(10)+str(player[2]).ljust(10)+str(player[0]+player[2]*0.5).ljust(10))
            i+=1
        return t
        
    
    def click(self, row,col):
        print("Clicked; row:",row," column:",col)
        if self.highlighted==False:
            #check if the click is on a valid piece:
            print("Highlighting")
            if self.board[row][col] in range(1+6*(self.turn%2),7+6*(self.turn%2)):
                #The clicked piece is the right colour
                print("Now selecting:",row," ",col)
                self.select(row, col)
                #Check whether there are any valid moves.
                for i in range(0,8):
                    for j in range(0,8):
                        if self.highlights[i][j] != 0:
                            self.highlighted = True
                        if self.highlighted==False:
                            print("Please try again")
                        else:
                            print("Wrong colour")
        else:
            if self.highlights[row][col] != 0:
                print("Moving")
                print(self.highlights)
                self.move(row,col,self.board[row][col])
                self.highlighted = False
                self.selected = [-1,-1]
            else:
                print("Please click a valid square")
        return self.board
#Testing

#print(g.checkCheck(1,g.board))
#print(g.checkCheck(2,g.board))
#g.turn = 0
g = chessGame()
#print(sG)
#g.board = [[0, 0, 0, 2, 0, 2, 0, 0], [0, 0, 11, 0, 0, 0, 4, 0], [0, 0, 6, 0, 0, 3, 1, 0], [0, 2, 0, 0, 0, 0, 0, 1], [7, 0, 0, 0, 7, 0, 8, 7], [3, 0, 9, 0, 0, 0, 10, 0], [0, 0, 10, 0, 3, 7, 0, 0], [0, 0, 4, 9, 0, 12, 0, 0]]
#m1 = g.generateMoves(g.turn,g.board)
#m2 = g.generateMoves(g.turn+1,g.board)
#print("M1:")
#for move in m1:
#    print(move.finalSquare)
#print("M2:")
#for move in m2:
#    print(move.finalSquare)

#strategyGridArray = g.strategyInfo(g.board, sG, g.generateMoves(g.turn,g.board),g.generateMoves(g.turn+1,g.board))
#print(strategyGridArray)
t = g.tournament()
#Test Chess Tree:
#turn = 0
#recursionLevel = 1  #Set the number of moves ahead
#testBoard = copy.deepcopy(g.board)
#testBoard = [[0, 0, 0, 0, 0, 0, 6, 0], [1, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 12, 0, 0 , 7], [0, 0, 0, 0, 0, 0, 0, 0]]
#m = chessMove([-1,-1],[-1,-1],False,0)
#g.T = chessTree(m)
#movesList = g.generateMoves(turn,testBoard)
#g.populateTree(0, movesList, testBoard, recursionLevel,turn)
#g.T.listNodes()
#p = "Base Path "
#pV = 0
#vL = "Value List "
#for moveNode in g.T.nodes:
#    print(moveNode.chessMove.initialSquare, moveNode.chessMove.finalSquare, moveNode.next)

#print(t)
