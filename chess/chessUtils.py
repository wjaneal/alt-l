#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 10:31:19 2020

@author: wneal
"""
from chessBase import *

class chessUtils(chessBase):
    #*********************************************************************************************************************
    #Utilities - These functions are the basic functions required for the chess program to run.  They work with the 
    # move functions defined in the ChessBase class in the chessCore module.
    #*********************************************************************************************************************

    ######################################################################################################################
    #Returns the numeric part of the square in chess notation.
    ######################################################################################################################
    def numberLookup(self,row):
        return str(8-row)
    
    ######################################################################################################################
    #Converts the square from rows and colunms in the list to chess notation.
    ######################################################################################################################
    def squareLookup(self,row,column):
        return self.letterLookup[column]+self.numberLookup(row)
   
    
    
    ######################################################################################################################
    # Determine whether the king is in check.
    ######################################################################################################################
    
    def checkCheck(self,turn,board):
        possibleMoves = []
        bcopy = copy.deepcopy(board)
        direction = -int((((turn+1)%2)-0.5)*2)
        #print("Direction: **************************************************************",direction, turn)
        #Generate moves from the current board.
        #If the king is in check, return True
        for i in range(0,8): #Scan through each row and column of the chess board. 
            for j in range(0,8):                
                #Check for possible subsequent moves:
                if bcopy[i][j] == 1+6*((turn+1) % 2):  #Is it a pawn?  Add pawn moves
                    possibleMoves = self.pawn(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 2+6*((turn+1) % 2):  #Etc. for rooks, knights, bishops, queens, kings.
                    possibleMoves = self.rook(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 3+6*((turn+1) % 2):
                    possibleMoves = self.knight(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 4+6*((turn+1) % 2):
                    possibleMoves = self.bishop(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 5+6*((turn+1) % 2):
                    possibleMoves = self.queen(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 6+6*((turn+1) % 2):
                    possibleMoves = self.king(i,j,bcopy,possibleMoves,turn+1)
        for m in possibleMoves:  #Check all the possible subsequent moves.  Is the king still in check? Not allowed. Return False
            if bcopy[m.finalSquare[0]][m.finalSquare[1]] == 9+3*direction:
                return True
        return False  #If no threat to the king is found, return false.
  
    
    #*********************************************************************************************************************
    #Functions for executing a game.
    #*********************************************************************************************************************
    ######################################################################################################################
    #Strategy Info - Fills a grid with information about potential moves
    ######################################################################################################################
    def strategyInfo(self, board, strategyGrid, movesList, counterMovesList):
        #print(strategyGrid)
        for move in movesList:
                strategyGrid[move.finalSquare[0]][move.finalSquare[1]].append(move.piece)
        for counterMove in counterMovesList:
                strategyGrid[counterMove.finalSquare[0]][counterMove.finalSquare[1]].append(counterMove.piece)
        return strategyGrid
    
            
    
    
    ######################################################################################################################
    #Checks if a given move is allowed and returns true or false.
    ######################################################################################################################
    
    def processMoves(self, turn, board, move):
        #Determines if moves are allowed and assigns values to the moves.
        #print("In-check Status: ",self.inCheck )
        #print("Checking if the move is allowed", move.displayDetails())
        possibleMoves = []
        bcopy = copy.deepcopy(board)
        direction = -int((((turn+1)%2)-0.5)*2)
        if not(move.castle == True and self.inCheck == True):  #Do not allow castling if the king is in check
            bcopy[move.finalSquare[0]][move.finalSquare[1]] = bcopy[move.initialSquare[0]][move.initialSquare[1]]
            bcopy[move.initialSquare[0]][move.initialSquare[1]] = 0
        #Is this en Passant? - Extra piece does not matter....
        if move.enPassant == True:
            #print("En Passant: ",move.initialSquare,move.finalSquare)
            bcopy[move.initialSquare[0]][move.finalSquare[1]] = 0 #Remove the opposing pawn
        else:
            pass
            #print("Other move:", move.initialSquare,move.finalSquare)
        #Is this castling? Only allowed if the king is not in check or will not be in check afterward.
        if move.castle == True:
            if self.inCheck == True:
                pass
            else:
                rookValue = 5-3*direction  #Determine which rook - white (2) or black (8)
                #Move the rook too; determine the final location by moving it one square away from the king.
                if move.finalSquare[1] > 3.5:
                    directionFactor = 1
                else:
                    directionFactor = -1
                bcopy[move.finalSquare[0]][move.finalSquare[1]-directionFactor] = rookValue
                
        for i in range(0,8): #Scan through each row and column of the chess board. 
            for j in range(0,8):                
                #Check for possible subsequent moves:
                if bcopy[i][j] == 1+6*((turn+1) % 2):  #Is it a pawn?  Add pawn moves
                    possibleMoves = self.pawn(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 2+6*((turn+1) % 2):  #Etc. for rooks, knights, bishops, queens, kings.
                    possibleMoves = self.rook(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 3+6*((turn+1) % 2):
                    possibleMoves = self.knight(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 4+6*((turn+1) % 2):
                    possibleMoves = self.bishop(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 5+6*((turn+1) % 2):
                    possibleMoves = self.queen(i,j,bcopy,possibleMoves,turn+1)
                if bcopy[i][j] == 6+6*((turn+1) % 2):
                    possibleMoves = self.king(i,j,bcopy,possibleMoves,turn+1)
        #print("King code: ",9+3*direction)
        for m in possibleMoves:  #Check all the possible subsequent moves.  Is the king still in check? Not allowed. Return False
            #print ("Possible moves: ", m.initialSquare, m.finalSquare,bcopy[m.finalSquare[0]][m.finalSquare[1]])
            if bcopy[m.finalSquare[0]][m.finalSquare[1]] == 9+3*direction:
                return [False, move]
        return [True, move]  #If no threat to the king is found, return true.
        
    ######################################################################################################################
    #Generate Moves - Creates a list of possible moves
    #Move Format: coordsFrom - [rowFrom, columnFrom], coordsTo - [rowTo,columnTo], pieceTaken?, pieceValue?, pawnToQueen?,
    #fork? discovery? unprotected piece? check? checkMate?
    ######################################################################################################################
    def generateMoves(self,turn,board):
        possibleMoves = []
        for i in range(0,8): #Scan through each row and column of the chess board. 
            for j in range(0,8):
                #print(i,j, "Scanning board")
                if board[i][j] == 1+6*(turn % 2):  #Is it a pawn?  Add pawn moves
                    possibleMoves = self.pawn(i,j,board,possibleMoves,turn)
                if board[i][j] == 2+6*(turn % 2):  #Etc. for rooks, knights, bishops, queens, kings.
                    possibleMoves = self.rook(i,j,board,possibleMoves,turn)
                if board[i][j] == 3+6*(turn % 2):
                    possibleMoves = self.knight(i,j,board,possibleMoves,turn)
                if board[i][j] == 4+6*(turn % 2):
                    possibleMoves = self.bishop(i,j,board,possibleMoves,turn)
                if board[i][j] == 5+6*(turn % 2):
                    possibleMoves = self.queen(i,j,board,possibleMoves,turn)
                if board[i][j] == 6+6*(turn % 2):
                    possibleMoves = self.king(i,j,board,possibleMoves,turn)
        for move in possibleMoves:
            move.piece = board[move.initialSquare[0]][move.initialSquare[1]]
        forkCheck = [0]*13
        kingforkCheck = [0]*13
        for move in possibleMoves:
            if move.value > 1:
                #print(move.piece)
                forkCheck[move.piece]+=1
            if move.value == 1000:
                kingforkCheck[move.piece]+=1
        for piece in forkCheck:
            if piece > 2:
                move.fork = True
        for piece in kingforkCheck:
            if piece > 0:
                move.kingFork = True
        #Now, check if each of these moves is possible...
        #print(len(possibleMoves))
        #print("Here are the possible moves before processing:")
        '''for item in possibleMoves:
           #print("Piece: ",g.board[item.initialSquare[0]][item.initialSquare[1]])
           #item.displayDetails()
           #print("Target", g.board[item.finalSquare[0]][item.finalSquare[1]])''' 
        possibleMovesTracker = [0]*len(possibleMoves)
        for m in range(len(possibleMoves)):
            processed = self.processMoves(turn, board,possibleMoves[m])
            possibleMoves[m] = processed[1] #Adjust the value of the move
            #print("Allowed: ", allowed)
            if processed[0] == False:
                possibleMovesTracker[m] = 1
        pm = []
        for i in range(len(possibleMoves)):
            if possibleMovesTracker[i] == 0:
                pm.append(possibleMoves[i])
        #print("Here is the final list of possible moves: ")
        #for item in pm:
        #    item.displayDetails()
        return pm  #Return the final list of allowed moves. 

 ######################################################################################################################
    #testMove: Executes a test move.
    #Special code for en passant - highlights code is 2
    #Regular code for other moves - highlights code is 1
    ######################################################################################################################
    def testMove(self,m,b):
        board = copy.deepcopy(b)
        #print("Moving...",self.selected,row,column,self.board)
        #Determine the direction of movement (for pawns)
        #print(move)
        row = m.initialSquare[0]
        column = m.initialSquare[1]
        rowTo = m.finalSquare[0]
        columnTo = m.finalSquare[1]
        direction = int(((self.turn%2)-0.5)*2)
               #Check for castling:
        if m.castle == True:
            #print(self.selected, row,column, self.board[row][column],self.board[self.selected[0]][self.selected[1]], "Test Castling")
            #Move the King:
            board[rowTo][columnTo] = board[row][column] 
            board[row][column] = 0
            self.kingLocation[self.turn%2] = [rowTo,columnTo]
            #Move the rook
            if columnTo == 6:
                board[rowTo][5] = board[rowTo][7]
                board[rowTo][7] = 0
            if columnTo == 2:
                board[rowTo][3] = board[rowTo][0]
                board[rowTo][0] = 0
            #self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = rowTo
            self.lastMoveColumn = columnTo
            return board
        #Check for en passant
        if m.enPassant == True:
            #This is en passant.
            #Place the new pawn
            #print (self.board, self.selected)
            board[rowTo][columnTo] = board[row][column]
            #Capture the other pawn using the direction as a reference
            #print("Testing en passant: ",row,column,direction)
            board[row+direction][column] = 0  #Remove the adjacent pawn
            #Move the pawn out of its square
            board[row][column] = 0  #Remove the original pawn
            #self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = rowTo
            self.lastMoveColumn = column
            return board
        if m.pawnQueen == True:  #At the moment, a queen is assumed.
            #Adjust to include knight, bishop, rook or queen
            board[rowTo][columnTo] = 8+3*direction
            board[row][column] = 0
            #self.resetHighlight()
            #self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            return board
            
        if m.castle == False and m.enPassant == False and m.pawnQueen == False:  #Regular one piece move.
            #print("Before Move:")
            #print(board)
            #print(rowTo,columnTo,row, column,board[rowTo][columnTo])
            #print("Empty square")
            #Place the piece in a new location:
            if board[rowTo][columnTo] not in [6,12]:
                board[rowTo][columnTo] = board[row][column]
            else:
                pass
                #print("Error!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            if board[rowTo][columnTo] in [6,12]:
                self.kingLocation[self.turn%2] = [rowTo,columnTo]
            #Remove the piece from its location:
            board[row][column] = 0
            #self.resetHighlight()
            #self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            #print("After move:")
            #print(board)
            return board
    
    ######################################################################################################################
    #move: Executes a move
    #Special code for en passant - highlights code is 2
    #Regular code for other moves - highlights code is 1
    ######################################################################################################################
    def move(self,m,board):
        #print("Moving...",self.selected,row,column,self.board)
        #Determine the direction of movement (for pawns)
        #print(move)
        #print("Move - self.board: ", board)
        row = m.initialSquare[0]
        column = m.initialSquare[1]
        rowTo = m.finalSquare[0]
        columnTo = m.finalSquare[1]
        direction = int(((self.turn%2)-0.5)*2)
        if self.turn/2 != int(self.turn/2):
            moveString = str(int(self.turn/2-0.5)+1)+". "
        else:
            moveString = " "
        if board[rowTo][columnTo] != 0:
            captureString = "x"
        else:
            captureString = ""
        #Check for castling:
        if m.castle == True:
            #print(self.selected, row,column, self.board[row][column],self.board[self.selected[0]][self.selected[1]], "Test Castling")
            #Move the King:
            board[rowTo][columnTo] = board[row][column] 
            board[row][column] = 0
            self.kingLocation[self.turn%2] = [rowTo,columnTo]
            #Move the rook
            if columnTo == 6:
                board[rowTo][5] = board[rowTo][7]
                board[rowTo][7] = 0
                self.moves+=moveString+"o-o"
            if columnTo == 2:
                board[rowTo][3] = board[rowTo][0]
                board[rowTo][0] = 0
                moves+=moveString+"o-o-o"
            self.turn+=1
            #self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = rowTo
            self.lastMoveColumn = columnTo
            return board
        #Check for en passant
        if m.enPassant == True:
            #This is en passant.
            #Place the new pawn
            #print (self.board, self.selected)
            board[rowTo][columnTo] = board[row][column]
            #Capture the other pawn using the direction as a reference
            #print("Testing en passant: ",row,column,direction)
            board[row+direction][column] = 0  #Remove the adjacent pawn
            #Move the pawn out of its square
            board[row][column] = 0  #Remove the original pawn
            #self.resetHighlight()
            self.selected = [-1,-1]
            self.lastMoveRow = rowTo
            self.lastMoveColumn = column
            self.moves+=moveString+captureString+str(column)+str(columnTo)+"ep"
            return board
        if m.pawnQueen == True:  #At the moment, a queen is assumed.
            self.moves+=moveString+self.pieceLookup[board[row][column]]+captureString+self.squareLookup(rowTo,columnTo)+"(Q)"
            #Adjust to include knight, bishop, rook or queen
            board[rowTo][columnTo] = 8+3*direction
            board[row][column] = 0
            self.turn+=1
            #self.resetHighlight()
            #self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            return board
            
        if m.castle == False and m.enPassant == False and m.pawnQueen == False:  #Regular one piece move.
            #print("Empty square")
            #Place the piece in a new location:
            self.moves+=moveString+self.pieceLookup[board[row][column]]+captureString+self.squareLookup(rowTo,columnTo)
            board[rowTo][columnTo] = board[row][column]
            if board[rowTo][columnTo] in [6,12]:
                self.kingLocation[self.turn%2] = [rowTo,columnTo]
            #Remove the piece from its location:
            board[row][column] = 0
            self.turn+=1
            #self.resetHighlight()
            #self.selected = [-1,-1]
            self.lastMoveRow = row
            self.lastMoveColumn = column
            return board
        return board
   
