#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 11:09:20 2020

@author: wneal
"""


list1 = ['cat','dog','lizard','fish']
numbers = [4,6,5,7]
months = ["Jan.", "Feb","Mar.","Apr."]
for i in range(len(list1)):
    print(list1[i].ljust(20),str(numbers[i]).ljust(5),months[i].ljust(10))