#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 15:31:42 2020
Chess Core - Core Chess Class to be extended in chessGame program.
@author: wneal
"""
import copy
from trees import *
from random import *
######################################################################################################################
#This is the class for a chess move.  It stores data about the move for use in AI strategy
######################################################################################################################

class chessNode(node):
    def __init__(self,previous, move):
        node.__init__(self,previous)
        self.chessMove = move #A slight modification of the base class, node
        self.value = move.value

class chessTree(tree):
    def __init__(self, move): #This class inherits the characteristics of a tree data structure.
        tree.__init__(self)
        self.nodes = [chessNode(None,move)] #This is an override - we redefine the self.nodes to contain chessNodes.
    
    def addChessNode(self, currentNode,cN): #cN - a move
        self.nodes.append(chessNode(currentNode, cN)) #Append a chess node
        self.nodes[currentNode].next.append(len(self.nodes)-1)
        
class chessMove:
    
    def __init__(self,initialSquare,finalSquare,capture,value):
        self.initialSquare = initialSquare
        self.finalSquare = finalSquare
        self.capture = capture
        self.value = value
        self.castle = False
        self.enPassant = False
        self.fork = False
        self.kingFork = False
        self.pin = False
        self.checkMate = False
        self.check = False
        self.pawnQueen = False
        self.piece = None
        
    def displayDetails(self):
        print("Initial Square: ", self.initialSquare, "Final Square: ", self.finalSquare)


class chessBase:
    #*********************************************************************************************************************
    #Functions for pieces' moves
    #*********************************************************************************************************************

    ######################################################################################################################
    #select: responds to a click to determine which piece is proposed for a move
    #highlights all possible squares to which the piece may move.
    ######################################################################################################################
    def pawn(self,row,column,board,possibleMoves,turn):
        #Determine the direction based on the current turn:
        #print(row,column, "possible moves: ",len(possibleMoves))
        #print("Row, column: ", row, column)
        direction = -int(((turn%2)-0.5)*2)
        #Check for first move
        if row == int(3.5-2.5*direction): #This is the first time to move the pawn
            if board[row+direction][column] == 0 and board[row+2*direction][column] == 0:  #Move twice:
                move = chessMove([row,column],[row+2*direction,column], False, 0)
                move
                possibleMoves.append(move)
        #Check for other move - not first move
        if row == int(3.5+2.5*direction):  #This pawn is about to queen
            pawnQueen = True
        else:
            pawnQueen = False
        #Forward Moves:
        try:
            if board[row+direction][column] == 0:
                move = chessMove([row,column],[row+1*direction,column],False, 0)
                move.pawnQueen = pawnQueen
                move.piece = "pawn"
                possibleMoves.append(move)
        except:
            print("Range Error")
        #Diagonal Moves, including en passant:
        if column == 0:  #Check first and last column separately to avoid range errors:
            if board[row+direction][column+1] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                move = chessMove([row,column],[row+direction,column+1],True, self.pieceValues[board[row+direction][column+1]])
                move.pawnQueen = pawnQueen
                move.piece = "pawn"
                possibleMoves.append(move)
            if board[row+direction][column+1] == 0:  #The space is empty
                if board[row][column+1] == 3.5+2.5*direction:  #An opposite pawn is beside...
                    move = chessMove([row,column],[row+direction,column+1],True, 1)
                    move.enPassant = True
                    move.piece = "pawn"
                    possibleMoves.append(move)

        elif column == 7: #Avoiding range errors in the last column 
            if board[row+direction][column-1] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                
                move = chessMove([row,column],[row+direction,column-1],True, self.pieceValues[board[row+direction][column-1]])
                move.pawnQueen = pawnQueen
                move.piece = "pawn"
                possibleMoves.append(move)
            if board[row+direction][column-1] == 0:  #The space is empty
                if board[row][column-1] == 3.5+2.5*direction:  #An opposite pawn is beside...
                    move = chessMove([row,column],[row+direction,column-1],True, 1)
                    move.enPassant = True
                    move.piece = "pawn"
                    possibleMoves.append(move)
        else:  #In the inner columns, check in both directions.
            try:
                if board[row+direction][column-1] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                    move = chessMove([row,column],[row+direction,column-1],True, self.pieceValues[board[row+direction][column-1]])
                    move.pawnQueen = pawnQueen
                    move.piece = "pawn"
                    possibleMoves.append(move)
                    if board[row+direction][column-1] == 0:  #The space is empty
                        if board[row][column-1] == 3.5+2.5*direction:  #An opposite pawn is beside...
                            move = chessMove([row,column],[row+direction,column-1],True, 1)
                            move.enPassant = True  
                            move.piece = "pawn"
                            possibleMoves.append(move)

                if board[row+direction][column+1] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                    move = chessMove([row,column],[row+direction,column+1],True, self.pieceValues[board[row+direction][column+1]])
                    move.pawnQueen = pawnQueen
                    move.piece = "pawn"
                    possibleMoves.append(move)
                    if board[row+direction][column+1] == 0:  #The space is empty
                        if board[row][column+1] == 3.5+2.5*direction:  #An opposite pawn is beside...
                            move = chessMove([row,column],[row+direction,column+1],True, 1)
                            move.enPassant = True
                            move.piece = "pawn"
                            possibleMoves.append(move)
            except:
                print("Range Error!")
        #print("Possible pawn moves: ",len(possibleMoves))
        
        return possibleMoves

    ######################################################################################################################
    #knight: uses a nested loop to determine possible moves 
    ######################################################################################################################
    def knight(self,row,column,board,possibleMoves,turn):
        xyrange = [-2,-1,1,2]  #Set the possible movements in the x (and y) directions
        for x in xyrange: #Iterate through all possibilities
            for y in xyrange:
                if abs(x)!=abs(y):#knights always move twice in one direction and once in the other...
                    if row+y in range(0,8) and column+x in range(0,8):
                        if board[row+y][column+x] == 0:
                            move = chessMove([row, column],[row+y,column+x],False,0)  #No piece in the way.
                            move.piece = "knight"
                            possibleMoves.append(move)
                        if board[row+y][column+x] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)): #Opposing piece in the way.
                            move = chessMove([row, column],[row+y,column+x], True,self.pieceValues[board[row+y][column+x]])
                            move.piece = "knight"
                            possibleMoves.append(move)
                            #print("Knight Moves: ",possibleMoves)
        return possibleMoves
    ######################################################################################################################
    #bishop:  uses a nested loop and direction vectors to find possible moves.
    ######################################################################################################################
    def bishop(self,row,column,board,possibleMoves,turn):
        done = 0
        for xdir in range(-1,3,2):
            for ydir in range(-1,3,2):  #This nested loop selects (-1,-1), (-1,1), (1,-1) and (1,1) as direction vectors
                done = 0
                step = 1
                while done == 0:
                    if row+step*xdir in range(0,8) and column+step*ydir in range(0,8):  #Make sure to stay on the board!
                        if board[row+step*xdir][column+step*ydir] == 0:  #An empty square has been reached.
                            move = chessMove([row,column],[row+step*xdir,column+step*ydir],False, 0)
                            move.piece = "bishop"
                            possibleMoves.append(move)
                        if board[row+step*xdir][column+step*ydir] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)): #An opposing piece is in the way.
                            move = chessMove([row,column],[row+step*xdir,column+step*ydir],True,self.pieceValues[board[row+step*xdir][column+step*ydir]])
                            move.piece = "bishop"
                            possibleMoves.append(move)
                            done = 1
                        if board[row+step*xdir][column+step*ydir] in range(1+6*((turn)%2),7+6*((turn)%2)): #A friendly piece is in the way
                            done = 1
                    else:
                        done = 1  #We have gone off the board. 
                    if board[row][column] == 6 or board[row][column] == 12:
                        done = 1 #This is a king - only one level required
                    else: 
                        step +=1 #Increase the number of steps until a piece is found or the end of the board is reached
        #print("Bishop moves: ",possibleMoves)
        return possibleMoves



    ######################################################################################################################
    #rook: check horizontal and vertical directions separately.
    ######################################################################################################################
    def rook(self,row,column,board,possibleMoves,turn):
        #print("Now moving rook")
        done = 0
        for xdir in range(-1,3,2):
            done = 0
            step = 1
            while done == 0:
                if row+step*xdir in range(0,8):  #Be sure to stay on the board
                    if board[row+step*xdir][column] == 0:  #There is no piece in the way
                        move = chessMove([row,column],[row+step*xdir,column], False, 0)
                        move.piece = "rook"
                        possibleMoves.append(move)
                    if board[row+step*xdir][column] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)): #There is an opposing piece in the way
                        move = chessMove([row,column],[row+step*xdir,column],True,self.pieceValues[board[row+step*xdir][column]])
                        move.piece = "rook"
                        possibleMoves.append(move)
                        done = 1
                    if board[row+step*xdir][column] in range(1+6*((turn)%2),7+6*((turn)%2)): #There is a friendly piece in the way
                        done = 1
                else:
                    done = 1 #We moved off the board
                if board[row][column] == 6 or board[row][column] == 12:
                    done = 1 #This is a king - only one level required
                else: 
                    step +=1

        for ydir in range(-1,3,2):
            done = 0
            step = 1
            while done == 0:
                if column+step*ydir in range(0,8):
                    if board[row][column+step*ydir] == 0:
                        move = chessMove([row,column],[row,column+step*ydir],False,0)
                        move.piece = "rook"
                        possibleMoves.append(move)
                    if board[row][column+step*ydir] in range(1+6*((turn+1)%2),7+6*((turn+1)%2)):
                        move = chessMove([row,column],[row,column+step*ydir],True,self.pieceValues[board[row][column+step*ydir]])
                        move.piece = "rook"
                        possibleMoves.append(move)
                        done = 1
                    if board[row][column+step*ydir] in range(1+6*((turn)%2),7+6*((turn)%2)):
                        done = 1
                else:
                    done = 1
                if board[row][column] == 6 or board[row][column] == 12:
                    done = 1 #This is a king - only one level required
                else: 
                    step +=1
        #print("Rook moves:", possibleMoves)
        return possibleMoves


    ######################################################################################################################
    #queen - a combination of rook and bishop moves
    ######################################################################################################################
    def queen(self,row,column,board,possibleMoves,turn):
        #print("Now moving queen")
        possibleMoves=self.bishop(row,column,board,possibleMoves,turn)
        possibleMoves=self.rook(row,column,board,possibleMoves,turn)
        return possibleMoves

    ######################################################################################################################
    #king - a queen with moves of length 1
    ######################################################################################################################
    def king(self,row,column,board,possibleMoves,turn):
        #print("Now moving king")
        #Check for movelength of 1!
        possibleMoves = self.bishop(row,column,board,possibleMoves,turn)
        possibleMoves = self.rook(row,column,board,possibleMoves,turn)
        #Check for castling:
        #Determine the direction of movement based on the current turn:
        direction = -int(((self.turn%2)-0.5)*2)
        #print(direction, " direction for castling")
        if self.castled[int(direction/2.0+0.5)] == 0: #Check if castling has already happened or been ruled out
            #print("Castle:", direction, row, column)
            if row == int(3.5-3.5*direction) and column == 4: #Check that this is the right king
                #print(row, int(3.5-3.5*direction), "check row")
                #Short side castle:
                if board[row][column+1] == 0 and board[row][column+2]==0: #Check for two empty squares
                    move = chessMove([row,column],[row,column+2],False,0)
                    move.castle = True
                    move.type = "Castle"
                    possibleMoves.append(move)
                    #print("Set the castling highlight for the ",direction," king")
                #Long side castle:
                if board[row][column-1] == 0 and board[row][column-2]==0 and board[row][column-3]==0: #Check for three empty squares
                    move = chessMove([row,column],[row,column-2],False,0)   
                    move.castle = True
                    move.type = "Castle"
                    possibleMoves.append(move)
        #print("Highlights - moving king - ",highlights)
        return possibleMoves


