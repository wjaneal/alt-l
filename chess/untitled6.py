#Earth Simulator
#Copyleft 2020, William Neal
#Uses Python Visual Module to Model the flight of the earth around the sun


#Import the required modules for math and graphics:
from math import *
from vpython import *


#Data - Earth and Sun
G = 6.67*10**(-11)  #m3 kg–1 s−2 
M1 = 1.989*10**30  #kg #This is the mass of the sun
M2 = 5.972*10**24  #kg #This is the mass of the earth
R = 1.49*10**11  #m #This is the typical radius of earth orbit

#Time Increment
dt=60*60*24  #One day of seconds


######################################################
#Initial Conditions
######################################################
#Speed of the Earth around the Sun:
v = 29000 #metres per second

#Speed in the time increment
#vt=v*dt #Number of metres the Earth travels in one time increment.

Vx = 0
Vy = v
                                                                                                                                                                                                                                                                                                                                                                                                                                            
#Sun Coordinates:
sunX = 0
sunY = 0

#Earth Coordinates
earthX = R
earthY = 0


#Functions


#Celestial Force:
#Force between two celestial bodies
def F(G,M1,M2,R):
    return -G*M1*M2/(R**2)

#Routine - Find the angle from x and y
def getAngle(x,y):
    yabs = abs(y) #Take the "absolute value" of y
    xabs = abs(x) #Take the "absolute value" of x
    A = atan(yabs/xabs) #This is in radians
    if x<0:
        if y<0:
            return rad(180)+A
        else:
            return rad(180)-A
    else:
        if y<0:
            return -A
        else:
            return A
    
#Convert from radians to degrees:
def deg(r): #Takes a radian quantity and returns degrees    
   return 180*r/pi 
    
def rad(d): #Takes a radian quantity and returns degrees    
   return pi*d/180 
    
scale = 0.0001
#Draw points to show where the fly has been:
print("Earth and Sun Simulation")
P = points(pos = [(0.000001,0,0)], color = color.red,radius = 5)
#Main Loop:
#For one year of seconds, do the following:
for i in range(360):
    RCurrent = sqrt(earthX**2+earthY**2) #Current distance from the earth to the sun
    print("RCurrent",RCurrent)
    Fx = F(G,M1, M2, RCurrent)*cos(getAngle(earthX,earthY)) #Force of the sun on the earth, x component
    Fy = F(G,M1, M2, RCurrent)*sin(getAngle(earthX,earthY)) #Force of the sun on the earth, y component
    ax = Fx/M2 #X Acceleration of the Earth 
    ay = Fy/M2 #Y Acceleration of the Earth
    dvx = ax*dt #Change in Earth's Velocity in one time increment - X Component
    dvy = ay*dt #Change in Earth's Velocity in one time increment - Y Component
    #New Valocity of the Earth
    Vx = Vx+dvx 
    Vy = Vy+dvy
    #New Position of the Earth due to velocity
    earthX = earthX+Vx*dt
    earthY = earthY+Vy*dt
    #Add the accelerated speed to the current speed
    #Calculate to find the w speed vector.
    print(earthX, earthY, Vx, Vy, ax, ay, Fx, Fy, getAngle(earthX, earthY) )
    Coords = (earthX, earthY, 0)
    P.append(Coords)
    
    
