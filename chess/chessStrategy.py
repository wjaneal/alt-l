#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 16 10:36:26 2020

ChatGPT Prompt:

Write a Python function that takes a chess board position as input and outputs a suggested move. 
The chessboard is represented by an eight by eight two dimensional array of integers. 
The white pieces are represented as follows: 
pawns, 1; rooks, 2; knights, 3; bishops. 4; queen, 5 and the king is 6.  
Each of the black pieces has a value six greater than the corresponding white piece.  
The strategy should suggest a move by a bishop where possible and offer a random legal move otherwise.  
@author: wneal
"""
from chessUtils import *
from random import *

class chessStrategy(chessUtils):
  #*********************************************************************************************************************
    #A collection of functions for Crude AI strategies
    #*********************************************************************************************************************
    
    def getMoveValue(self, board, turn):
        #Placeholder value for the moment:
        return int(random()*100)
    
    def populateTree(self, currentNode, movesList, testBoard, recursionLevel,turn):
        currentPointer = currentNode
        if currentNode > 0:
                currentPointer = currentNode-1
            
        #If recursion level == 0: return the final tree
        if recursionLevel == 0:
            pass
            #print("Recursion Depth Reached")
        else:        #Otherwise add each possible move to the tree
            #Iterate through all possible moves:
            moveNum = 0
            for move in movesList:
                #Find the next move:
                r = move.initialSquare[0]
                c = move.initialSquare[1]
                print("Move", moveNum, "r: ",r, "c:", c, "Recursion: ", recursionLevel)
                moveNum+=1
                self.turn+=1 #Adjust the current turn temporarily to make a new move
                testBoard1 = copy.deepcopy(self.testMove(move,testBoard))
                #print(testBoard1)
                #print(testBoard)
                self.turn-=1 #Set the turn back
                possibleMoves = self.generateMoves(turn+1, testBoard1)
                #Adding a node with a move.
                print("Adding a move", move.initialSquare,"-->",move.finalSquare," turn: ",turn, " node:",currentNode, " recursion level: ",recursionLevel)
                self.T.addChessNode(currentPointer,move)
                #Determine the value of the move
                testBoardNextMove = self.testMove(move,testBoard1)
                moveValue = self.getMoveValue(testBoardNextMove,turn)
                #Set the value of the move in the chess node
                self.T.nodes[-1].value = moveValue
                if recursionLevel >= 1:
                        print("Next level reached: ", len(possibleMoves))
                        self.populateTree(len(self.T.nodes),possibleMoves, testBoardNextMove, recursionLevel-1, turn+1 )
                currentNode = len(self.T.nodes)
           
    ######################################################################################################################
    #selects a random move from the moves list
    ######################################################################################################################
    def selectRandom(self, movesList):
        #print("Here is the current moves list:", movesList)
        if len(movesList)==0:
            return "Error!"
        m=int(random()*len(movesList))
        return movesList[m]
    
    ######################################################################################################################
    #prefers to play with a particular piece if possible
    ######################################################################################################################
    def selectPiece(self,movesList,piece):
        if len(movesList)==0:
            return "Error!"
        piece = False
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            m.piece = self.board[m.initialSquare[0]][m.initialSquare[1]]
            if m.piece in [piece,piece+6]:
                piece = True
                m1.append(m)
        if piece == True:
            pieceMove = int(random()*len(m1))
            return m1[pieceMove]
        else:
            return self.selectRandom(movesList)
    ######################################################################################################################
    #plays peaceful moves if possible
    ######################################################################################################################
    def peaceBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        emptySquareMoves = False
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            m.peace = self.board[m.finalSquare[0]][m.finalSquare[1]]
            if m.peace == 0:
                emptySquareMoves = True
                m1.append(m)
        if emptySquareMoves == True:
            peaceMove = int(random()*len(m1))
            return m1[peaceMove]
        else:
            return self.selectRandom(movesList)
            
    ######################################################################################################################
    #plays capture moves if possible
    ######################################################################################################################
    def warBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        captureMoves = False
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            m.war = self.board[m.finalSquare[0]][m.finalSquare[1]]
            if m.war != 0:  #Go for any non-empty square
                captureMoves = True
                m1.append(m)
        if captureMoves == True:
            warMove = int(random()*len(m1))
            return m1[warMove]
        else:
            return self.selectRandom(movesList)


    ######################################################################################################################
    #plays the most valuable capture move
    #####################################################################################################################
    def discerningWarBot(self,movesList):
         if len(movesList)==0:
             return "Error!"
         captureMoves = False
         m1 = [] #Create a special list of moves that involve a particular piece
         for m in movesList:
             m.war = self.board[m.finalSquare[0]][m.finalSquare[1]]
             if m.war != 0:  #Go for any non-empty square
                 captureMoves = True
                 m1.append(m)
         if captureMoves == True:
             bestIndex = 0
             bestValue = m1[0].value
             for i in range(len(m1)):
                 if m1[i].value>bestValue:
                     bestIndex = i
                     bestValue = m1[i].value
             discerningWarMove = bestIndex
             return m1[discerningWarMove]
         else:
             return self.selectRandom(movesList)
    
    ######################################################################################################################
    #plays the most valuable capture move
    #####################################################################################################################
    def differentialWarBot(self,movesList):
         if len(movesList)==0:
             return "Error!"
         captureMoves = False
         m1 = [] #Create a special list of moves that involve a particular piece
         for m in movesList:
             pieceValue = self.pieceValues[self.board[m.initialSquare[0]][m.initialSquare[1]]]
             m.war = self.board[m.finalSquare[0]][m.finalSquare[1]]
             if m.war != 0:  #Go for any non-empty square
                 captureMoves = True
                 m1.append(m)
         if captureMoves == True:
             bestIndex = 0
             bestValue = m1[0].value-pieceValue
             for i in range(len(m1)):
                 if m1[i].value>bestValue:
                     bestIndex = i
                     bestValue = m1[i].value-pieceValue
             differentialWarMove = bestIndex
             return m1[differentialWarMove]
         else:
             return self.selectRandom(movesList)
    
    
    ######################################################################################################################
    #prefers to move pawns
    ######################################################################################################################
    def pawnBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            if m.piece == 1 or m.piece == 7:
                m1.append(m)
        if len(m1)>0:
            checkMove = int(random()*len(m1))
            return m1[checkMove]
        else:
            return self.randomBot(movesList)
    
    
    ######################################################################################################################
    #prefers to move knights
    ######################################################################################################################
    def knightBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            if m.piece == 4 or m.piece == 10:
                m1.append(m)
        if len(m1)>0:
            checkMove = int(random()*len(m1))
            return m1[checkMove]
        else:
            return self.randomBot(movesList)
    
    
    ######################################################################################################################
    #prefers to move queens
    ######################################################################################################################
    def queenBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            if m.piece == 5 or m.piece == 11:
                m1.append(m)
        if len(m1)>0:
            checkMove = int(random()*len(m1))
            return m1[checkMove]
        else:
            return self.randomBot(movesList)
    ######################################################################################################################
    #One Man Show! The king does not know how to delegate!
    ######################################################################################################################
    def kingBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            if m.piece == 6 or m.piece == 12:
                m1.append(m)
        if len(m1)>0:
            checkMove = int(random()*len(m1))
            return m1[checkMove]
        else:
            return self.randomBot(movesList)
    ######################################################################################################################
    #prefers to move rooks
    ######################################################################################################################
    def rookBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            if m.piece == 2 or m.piece == 8:
                m1.append(m)
        if len(m1)>0:
            checkMove = int(random()*len(m1))
            return m1[checkMove]
        else:
            return self.randomBot(movesList)
    
    ######################################################################################################################
    #prefers to move bishops
    ######################################################################################################################
    def bishopBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            if m.piece == 4 or m.piece == 10:
                m1.append(m)
        if len(m1)>0:
            checkMove = int(random()*len(m1))
            return m1[checkMove]
        else:
            return self.randomBot(movesList)
    
    ######################################################################################################################
    #prefers to place a player in check
    ######################################################################################################################
    def checkBot(self,movesList):
        if len(movesList)==0:
            return "Error!"
        checkMoves = False
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            #Deepcopy the self.board into a variable "testBoard"
            testBoard = copy.deepcopy(self.board)
            #Make the proposed move with the testBoard (use testMove - a modified version of Move)
            newBoard = self.testMove(m, testBoard)
            #Check whether the move results in a check self.checkCheck
            if self.checkCheck(self.turn, newBoard):
                m1.append(m)
            #If so, append that move to m1
            CheckMoves = True
        if checkMoves == True:
            checkMove = int(random()*len(m1))
            return m1[checkMove]
        else:
            return self.warBot(movesList)
        
    ######################################################################################################################
    #Checks for safe squares
    ######################################################################################################################
    def safeWarBot(self, movesList, turn):
        m1 = []
        sG = [[[] for i in range(8)]for j in range(8)]
        counterMovesList = self.generateMoves(turn+1,self.board)
        strategyGrid = self.strategyInfo(self.board,sG,movesList, counterMovesList)
        for move in movesList:
            row = move.finalSquare[0]
            column = move.finalSquare[1]
            if len(strategyGrid[row][column]) == 0:
                m1.append(move)
        if len(m1)>0:
            safeMove = int(random()*len(m1))
            return m1[safeMove]
        else:
            return self.warBot(movesList)
    
    
    ######################################################################################################################
    #Checks for protected squares - squares with coverage from other pieces
    ######################################################################################################################
    def protectedWarBot(self, movesList, turn):
        m1 = []
        sG = [[[] for i in range(8)]for j in range(8)]
        protected = True
        counterMovesList = self.generateMoves(turn+1,self.board)
        strategyGrid = self.strategyInfo(self.board,sG,movesList, counterMovesList)
        for move in movesList:
            row = move.finalSquare[0]
            column = move.finalSquare[1]
            if len(strategyGrid[row][column]) >0:
                attacksOnSquare = strategyGrid[row][column]
                for i in range(len(attacksOnSquare)):
                    if (turn%2) == 0:
                        if attacksOnSquare[i] < 7:
                            protected = False
                    else:
                        if attacksOnSquare[i] > 6:
                            protected = False
                if protected == True:    
                    m1.append(move)
        if len(m1)>0:
            protectedMove = int(random()*len(m1))
            return m1[protectedMove]
        else:
            return self.safeWarBot(movesList,turn)
    
    ######################################################################################################################
    #uses a chess Tree
    ######################################################################################################################
    def treeBot(self, movesList, turn):
        recursionLevel = 3  #Set the number of moves ahead
        ''' (1) Call populateTree to recursively generate the tree of possible moves.
        '''
        
        testBoard = copy.deepcopy(self.board)
        m = chessMove([-1,-1],[-1,-1],False,0)
        self.T = chessTree(m)
        self.populateTree(0, movesList, testBoard, recursionLevel,turn)
       
        for item in self.T.nodes:
            print("Chess tree display: ", item.value)
            
        #(2)  For each move in the tree, generate a new moves list for the other player and
        #add it to the tree.
        #Call 
       
        #print(self.T.nodes[0].next)
        #self.T.listPaths(0, "", 0, "")
        
        '''
        (3)  For each of these moves (lots!!!!) generate a moves list and append to the tree
        (4) Assign values to each move in the tree
        (5)  List all paths in the tree and find the path of greatest value.
        (6) Select the move that leads to the path of greatest value'''
        
        #During testing, behave as a random bot.
        return self.selectRandom(movesList)
        
    
    def randomBot(self,movesList):
        return self.selectRandom(movesList)


    ######################################################################################################################
    #Queens are cloning everywhere!
    ######################################################################################################################
    def queenCloneBot(self,movesList,turn):
        for i in range(0,8):
            for j in range(0,8):
                if self.board[i][j] == 0:
                    self.board[i][j] = 5+6*(turn % 2)
        if len(movesList)==0:
            return "Error!"
        captureMoves = False
        m1 = [] #Create a special list of moves that involve a particular piece
        for m in movesList:
            m.war = self.board[m.finalSquare[0]][m.finalSquare[1]]
            if m.war != 0:  #Go for any non-empty square
                captureMoves = True
                m1.append(m)
        if captureMoves == True:
            peaceMove = int(random()*len(m1))
            return m1[peaceMove]
        else:
            return self.selectRandom(movesList)
        
        
    def selectStrategy(self,movesList,players,turn):
        whichPlayer = (turn-1)%2
        
        if players[whichPlayer] == 0:
            return self.differentialWarBot(movesList)
        if players[whichPlayer] == 1:
            return self.warBot(movesList)
        if players[whichPlayer] == 2:
            return self.discerningWarBot(movesList)  
        if players[whichPlayer] == 3:
            return self.checkBot(movesList)
        if players[whichPlayer] == 4:
            return self.selectRandom(movesList)
        if players[whichPlayer] == 5:
            return self.pawnBot(movesList)
        if players[whichPlayer] == 6:           
            return self.protectedWarBot(movesList,turn)
        if players[whichPlayer] == 7:
            return self.safeWarBot(movesList,turn)
        '''
        if players[whichPlayer] == 0:
            return self.randomBot(movesList)
        if players[whichPlayer] == 1:
            return self.queenBot(movesList)
        if players[whichPlayer] == 2:
            return self.bishopBot(movesList)  
        if players[whichPlayer] == 3:
            return self.knightBot(movesList)
        if players[whichPlayer] == 4:
            return self.rookBot(movesList)
        if players[whichPlayer] == 5:
            return self.kingBot(movesList)
        if players[whichPlayer] == 6:           
            return self.pawnBot(movesList)
        '''
        
"""
Testing Section
"""
