#Import kivy modules:
from kivy.app import App
from kivy.graphics import Color, Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import AsyncImage
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.widget import Widget, ListProperty
from kivy.cache import Cache 

from kivy.config import Config
Config.set('graphics', 'width', '800')
Config.set('graphics', 'height', '800')

from chessgame import *

from random import *
#Main App
class MainApp(App):

    def __init__(self, **kwargs):
        super(MainApp,self).__init__(**kwargs)
        self.board = ""
        self.game = chessGame()
        self.Pieces = {0:"white.png",1:"wp.png",2:"wb.png",3:"wkn.png",4:"wr.png",5:"wq.png",6:"wk.png",7:"black.png",8:"wp1.png",9:"wb1.png",10:"wkn1.png",11:"wr1.png",12:"wq1.png",13:"wk1.png", 14:"bp.png",16:"bb.png",17:"bkn.png",18:"br.png",19:"bq.png",20:"bk.png",22:"bp1.png",23:"bb1.png",24:"bkn1.png",25:"br1.png",26:"bq1.png",15:"bk1.png"}

    def build(self):
        self.board = GridLayout(cols=8)
        for i in range(0,64):
                row = 7-int(i/8)
                column = 7-i%8
                if i % 2 == 0: 
                        if int(i/8)%2==0:
                            cb = CustomBtn(source=self.Pieces[self.game.board[row][column]])
                            cb.bind(pressed=self.btn_pressed)
                            self.board.add_widget(cb)
                        else:                	
                            cb = CustomBtn(source=self.Pieces[self.game.board[row][column]+7])
                            cb.bind(pressed=self.btn_pressed)
                            self.board.add_widget(cb)
                else:
                        if int(i/8)%2==1:
                            cb = CustomBtn(source=self.Pieces[self.game.board[row][column]])
                            cb.bind(pressed=self.btn_pressed)
                            self.board.add_widget(cb)
                        else:                	
                            cb = CustomBtn(source=self.Pieces[self.game.board[row][column]+7])
                            cb.bind(pressed=self.btn_pressed)
                            self.board.add_widget(cb)
	
        return self.board

    def updateBoard(self):
        print(self.game.turn,self.game.board)
        for i in range(0,64):
            row = int(i/8)
            column = i%8
            piece = self.game.board[row][column]
            if piece > 100:
                Rectangle(pos=(row*100,column*100),size=(100,100))
                piece-=100
            if (row+column)%2 == 0:
            	self.board.children[i].source = self.Pieces[piece]
            else:
                self.board.children[i].source = self.Pieces[piece+7]

    def btn_pressed(self, instance, pos):
        col=int(pos[1]/100)
        row=int(pos[0]/100)
        print(row,col)
        self.game.click(row,col)
        print(self.game.board)
        self.updateBoard()
        print ('pos: printed from root widget: {pos}'.format(pos=pos))




class CustomBtn(Image):
     pressed = ListProperty([0, 0])
     def __init__(self,**kwargs):
         super(CustomBtn, self).__init__(**kwargs)
         self.selected = 0
     def on_touch_down(self, touch):
         if self.collide_point(*touch.pos):
             if self.selected == 0:
                 self.selected = touch.pos
             else:
                 print(self.selected)
             self.pressed = touch.pos
             #self.source = self.chessMove(touch.pos) 
             # we consumed the touch. return False here to propagate
             # the touch further to the children.
             return True
         return super(CustomBtn, self).on_touch_down(touch)

     def on_pressed(self, instance, pos):
         print ('pressed at {pos}'.format(pos=pos))


     def chessMove(self, position):
         print("Now moving piece at ", position)
         return("green.png")
if __name__ == '__main__':
    MainApp().run()
