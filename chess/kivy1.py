from kivy.app import App
from kivy.graphics import Color, Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.image import AsyncImage
from kivy.uix.gridlayout import GridLayout
from kivy.uix.button import Button





class MainApp(App):

    def build(self):
        board = GridLayout(cols=8)
        for i in range(0,64):
                #root.add_widget(board)
                if i % 2 == 0: 
                        if int(i/8)%2==0:
                	        board.add_widget(Button(background_normal="",background_color=[1,1,1,1]))
                        else:                	
                                board.add_widget(Button(background_normal="",background_color=[0,0,0,1]))
                else:
                        if int(i/8)%2==1:
                	        board.add_widget(Button(background_normal="",background_color=[1,1,1,1]))
                        else:                	
                                board.add_widget(Button(background_normal="",background_color=[0,0,0,1]))
	
        return board

if __name__ == '__main__':
    MainApp().run()
