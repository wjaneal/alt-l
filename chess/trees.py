'''Data Structures - Trees

(1) Generate a random tree with 10-30 branches at each node.  Give each node a random value between 0.0 and 10.0
'''
from random import *

class node:
    def __init__(self,previous):
        self.value = 0
        self.previous = previous
        self.next = []

class tree:
    def __init__(self):
        self.nodes = [node(None)] #Initialize a node with no previous node.

    def addNode(self, currentNode):
        self.nodes.append(node(currentNode))
        self.nodes[currentNode].next.append(len(self.nodes)-1)
        print(len(self.nodes))
    

    def listNodes(self):
        print("Now Listing Nodes",len(self.nodes))
        for n in range(len(self.nodes)):
            print("Node #",n, "Value:  ", self.nodes[n].value, "   Previous: ",self.nodes[n].previous, "   Next:",self.nodes[n].next)

    def listPaths(self,currentNode,path,pathValue,valueList):
        path+=" "+str(currentNode)
        pathValue+=self.nodes[currentNode].value
        valueList+=str(self.nodes[currentNode].value)+" "
        if self.nodes[currentNode].next == []:
            print("Path: ", path, "  Value: ",pathValue, "Values",valueList)
        else:
            for i in self.nodes[currentNode].next:
                self.listPaths(i, path,pathValue,valueList)

'''        
T = tree()
for i in range(0,10):
    T.addNode(0)
    print("Added a Node, ", i)
T.listNodes()


for n in range(1,len(T.nodes)):
    for i in range(1,10):
        T.addNode(n)
T.listNodes()
T.listPaths(0, "", 0, "")
'''


