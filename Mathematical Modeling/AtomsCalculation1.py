#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 21:06:53 2020

@author: wneal
"""

v = 100 #This is the volume of the water in mL
#"ml"
p = 1  #This is the density of water in g/(mL**3)
#"g/cm^3"
M = 18 
#"g/mol"
NA = 6.02*10**23
m = p*v 
#"g"
N =m/M 
#"mol"
n =N*NA
print ("the number of the water atom:", n)


r=0.005
from math import *
pi
v=(4/3)* pi*r*r*r
V=n*v
print ("the total volume:", V)

R=6.4*10**6
S=4*pi*R**2  #This is the surface area of the earth
h=V/S #Assume that the surface of the earth is a box
print ("the depth if spread the marble over the earth :", h,)
