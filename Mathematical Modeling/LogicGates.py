#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  4 12:47:25 2020

@author: wneal
"""

def andGate(A,B):
    if A == 1 and B == 1: 
        return 1
    else:
        return 0

    I1 = andGate(A1, A2)
    I5 = orGate(I1, A1)

#Logic Program Components:
# (1) Decide how many inputs (n)
# (2) Connect inputs to an arbitrary logic gate circuit
# (3) For each possible combination of inputs, print the expected output.
    
#Inputs A1, A2, A3, A4.....An
#Example A1, A2, A3, A4
# 0000 Output: 1
# 0001 Output: 0
# 0010 Output: 1
# 0011 Output: 1
# 0100 Output: 0
# 0101
# 0110
# 0111
# 1000
# 1001
# 1010
# 1011
# 1100
# 1101
# 1110
# 0111 Output: 1