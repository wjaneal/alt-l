#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 21:11:26 2020

@author: wneal
"""

from math import *
#v stands for the volume of the water
"""
When all of the atoms inside the cup of water become 
a marble with 0.5cm radius.
The volume of a ball is 4/3*Pi*r**3
"""
#the unit in the calculation is L and a stands for Pi
a=pi
v=(0.1*3*4/3*a*0.05**3)/(8/3*a*(3.7*10**(-10))**3+4/3*a*(0.74*10**(-9))**3)

print("The volume of the water would be ",v/1000," m^3")

#The radius of the Earth is around 6371000 m
#d refers to the depth of the water when it is poured evenly on the surface of the Earth
#4/3*Pi*(r+d)**3-4/3*Pi*r**3=v
b=v/1000  #the volume in m**3

 #4/3*a*((6371000+d)**3-6371000**3)=b
d=((b+4/3*a*6371000**3)/a/(4/3))**(1/3)-6371000
print("the depth of the water would be ",d/1000," km")
