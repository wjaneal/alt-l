#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 21:13:31 2020

@author: wneal
"""

from math import * #This command imports the mathematics library - more commands available
a=0.037**(3)*10**(-27) #This is the volume of a hydrogen atom
b=0.074**(3)*10**(-27) #This is the volume of an oxygen atom
c=2*a+b
d=125*10**(-9)
V=(d/c)*10**(-4)*3
print ("the total volume is:",V)

S=510067866*10**(6)
e=V/S
print ("The depthis:", e)
