#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 21:10:44 2020

@author: wneal
"""

# Water volume
V1 = 100 #ml
# Density of water
ρ = 1 #g/cm**3
# Therelative molecular weight of water
M = 18 #g/mol
# Surface area of earth
S = 51*10**7 #km**2
# The radius of the water molecule
R = 4*10**-10 #m
# Avogadro constant
NA = 6.02214076*10**23 #mol**-1
# The depth
d = (V1*ρ) /M*NA/S*R*10**-10 
print("The depth(m) of the atoms is",d,".")