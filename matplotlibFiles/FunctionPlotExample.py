#This file illustrates the plotting of a 3D function 
# z = f(x,y)
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import numpy as np

fig = plt.figure()
ax = plt.axes(projection="3d")

x = np.arange(-10, 10, 0.2)
y = np.arange(-10, 10, 0.2)
x, y = np.meshgrid(x, y)
#Here is the function: z = x**2*y**3 + x**3*y
z = (x**2*y**3 + x**3*y)
z = (x)*(y)
ax.plot_surface(x,y,z)
ax.set_xlabel('X Axes')
ax.set_ylabel('Y Axes')
ax.set_zlabel('Z Axes')

plt.show()
