#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 20:05:37 2020

@author: wneal
"""
from random import *
n = 1000000
count = 0
for i in range(n):
    d1 = int(random()*6+1)
    d2 = int(random()*6+1)
    d3 = int(random()*6+1)
    d4 = int(random()*6+1)
    d5 = int(random()*6+1)
    if d1+d2 > d3+d4+d5:
        count+=1
print(count)