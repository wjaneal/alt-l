#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 11:30:07 2020

@author: wneal
"""

#Programming Challenge:

#Create a program that has a function that calculates the total cost of 'n' fruit
#at price 'p' with a sales tax of 15%

#First function:
#salesTax(money) 1 argument: money (numeric)
    #Calculate tax 15% of that money and return the money + tax
    
#Second Function:
#buyFruit(fruitName, n, c)  fruitName: string - print out the name of the fruit
                            #n number of fruit
                            #cost for each fruit.
#Calculate the subTotal (cost of the fruit without tax)
#Calculate the Total (subTotal with the tax added - call the saleTax function)
                            
#Example
fruit = "apple"
n = 30
p = 1.35
buyFruit(fruit,n,p) #This prints messages to outline the purchase of the fruit