#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 13 17:43:27 2024

@author: wneal
"""
import matplotlib.pyplot as plt
import numpy as np

def f(x, x1, x2, y1, y2):
    m = (y2-y1)/(x2-x1)
    return m*(x-x1)+y1

ax = plt.subplot()

t = np.arange(0.0, 5.0, 0.01)
s = np.cos(2*np.pi*(t/5))
line, = plt.plot(t, s, lw=2)

plt.annotate('What is the slope here?', xy=(3, -0.75), xytext=(3, 1.5),
             arrowprops=dict(facecolor='black', shrink=0.05),
             )

plt.annotate('x1', xy=(3, -0.75), xytext=(3, -1.5),
             arrowprops=dict(facecolor='black', shrink=0.05),
             )

plt.annotate('x2', xy=(4, 0.5), xytext=(4, -1.5),
             arrowprops=dict(facecolor='black', shrink=0.05),
             )

x1 = 3
y1 = np.cos(2*np.pi*(x1/5))
plt.plot(x1,y1,'ro')

x2 = 4
y2 = np.cos(2*np.pi*(x2/5))
plt.plot(x2,y2,'ro')

plt.ylim(-2,2)
t1 = np.arange(0.0, 5.0, 0.1)
plt.plot(t1, f(t1, x1, x2, y1, y2), 'b')
