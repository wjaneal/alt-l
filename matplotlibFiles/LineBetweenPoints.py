#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 12 10:31:53 2025

@author: wneal
"""

import matplotlib.pyplot as plt

# Define the coordinates of the two points
point1 = (5, 2)  # x, y for point 1
point2 = (4, 5)  # x, y for point 2

# Extract x and y coordinates
x_coords = [point1[0], point2[0]]
y_coords = [point1[1], point2[1]]

# Create a plot
plt.figure()
plt.plot(x_coords, y_coords, 'b-', linewidth=2)  # 'b-' for blue solid line

# Set the title and labels
plt.title('Line Between Two Points')
plt.xlabel('X-axis')
plt.ylabel('Y-axis')

# Add markers for the points
plt.plot(point1[0], point1[1], 'ro', label='Point 1')  # 'ro' for red circle
plt.plot(point2[0], point2[1], 'go', label='Point 2')  # 'go' for green circle

# Add a legend
plt.legend()

# Display the plot
plt.grid(True)  # Show grid for better visualization
plt.axis('equal')  # Ensure aspect ratio is equal so the line isn't skewed
plt.show()