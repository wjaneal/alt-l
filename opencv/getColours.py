import cv2
import numpy as np
ball_color = ['green','red','blue','yellow']
color_dist = {'red': {'Lower': np.array([156,43, 46]), 'Upper': np.array([180, 255, 255])},
              'blue': {'Lower': np.array([78, 43, 46]), 'Upper': np.array([124, 255, 255])},
              'green': {'Lower': np.array([35, 43, 35]), 'Upper': np.array([90, 255, 255])},
              'yellow': {'Lower': np.array([26,43,46]),'Upper': np.array([34,255,255])}}
cap = cv2.VideoCapture(0)
cv2.namedWindow('camera', cv2.WINDOW_AUTOSIZE)
while cap.isOpened():
    ret, frame = cap.read()
    if ret:
        if frame is not None:
            gs_frame = cv2.GaussianBlur(frame, (5, 5), 0)                     # GaussianBlur so that can easier to get colors
            hsv = cv2.cvtColor(gs_frame, cv2.COLOR_BGR2HSV)                 # change color from BGR to HSV
            erode_hsv = cv2.erode(hsv, None, iterations=2)
            for i in range(len(ball_color)):
                inRange_hsv = cv2.inRange(erode_hsv, color_dist[ball_color[i]]['Lower'], color_dist[ball_color[i]]['Upper'])
                cnts = cv2.findContours(inRange_hsv.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
                c = max(cnts, key=cv2.contourArea)
                rect = cv2.minAreaRect(c)
                box = cv2.boxPoints(rect)
                cv2.drawContours(frame, [np.int0(box)], -1, (0, 255, 255), 2)
                cv2.imshow('camera', frame)
                cv2.waitKey(1)
            if cv2.waitKey(1) == ord('q'):
                break
        else:
            print("no image")
    else:
        print("cannot read the camera!")

cap.release()
cv2.destroyAllWindows()
