#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 18 09:23:55 2018
Fractal Generator
@author: wneal
"""
from tkinter import *
#from PIL import ImageGrab
from math import *
import cv2 
import numpy
from PIL import *


def hexcast(x):
    colourDict = {10:'A',11:'B',12:'C',13:'D',14:'E',15:'F'}
    d1 = int(x/16)
    d2 = x%16
    if d1>=10:
        d1 = colourDict[d1]
    else:
        d1 = str(d1)
    if d2>=10:
        d2 = colourDict[d2]
    else:
        d2 = str(d2)
    return d1+d2

#Takes a complex number in polar form and adds
def fractal_function(c,alpha):
    c = complex_mult_cartesian(c,c)
    c[0]+=alpha[0]
    c[1]+=alpha[1]
    return c

def complex_mult_polar(c1,c2):
    return [c1[0]*c2[0],c1[1]+c2[1]]

def complex_mult_cartesian(c1,c2):
    return [c1[0]*c2[0]-c1[1]*c2[1],c1[0]*c2[1]+c1[1]*c2[0]]

def cartesian_to_polar(c):
    pi = 4*atan(1)
    r = sqrt(c[0]*c[0]+c[1]*c[1])
    try:
        t = atan(c[1]/c[0])
    except:
        t= pi/2
    #Account for angle of complex number in radians
    if c[0]<0:
        if c[1]<0:
            return[r,t+pi]
        else:
            return[r,pi-t]
    else:
        if c[1]<0:
            return[r,2*pi-t]
        else:
            return[r,t]
#Tqkes radius of complex number and maps to a colour
def colour_map(cr):
    magnitude_threshold = 256
    magnitude = int(cr/1000000)  #print(magnitude)
    if magnitude >255:
        return [255,255,255]
    else:
        c1 = int(magnitude)
        return [c1,c1,c1]
    
def complex_magnitude(x):
    return sqrt(x[0]**2+x[1]**2)

xsize = 1000
ysize = 1000

master = Tk()

#w.pack()           
for theta in range(0,360):
    print(theta)  #output
    #w = Canvas(master, width=xsize, height=ysize)
    w = numpy.zeros((xsize,ysize,3), numpy.uint8)
    pi = 4*atan(1)
    r = cos(theta*pi/180)
    c = sin(theta*pi/180)            
    iterations = 10
    alpha = [r,c] #Complex constant for the fractal function
    magnitude_threshold = 100
    for i in range(1,xsize):
        for j in range(1,ysize):
            c = [(i-xsize/2)/100,(j-ysize/2)/100]
            for k in range(0,iterations):
                c = fractal_function(c,alpha)
                if complex_magnitude(c)>1000000:
                    break
            #print(i,c)
            colour = colour_map(sqrt(c[0]*c[0]+c[1]*c[1]))
            #print (i,j,c,colour)
            w[i,j] = colour
    if theta<10:
        filenum = "00"+str(theta)
    elif theta >10 and theta<100:
        filenum = "0"+str(theta)
    else:
        filenum = str(theta)
    cv2.imwrite("file_name"+filenum+".jpg", w)
mainloop()
