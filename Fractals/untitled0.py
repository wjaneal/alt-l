#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 26 11:23:48 2025


Supermarket Simulator!


@author: wneal
"""

items = ["apples","oranges","bananas"] #What items are for sale?
costs = [1.40, 2.56, 1.66] #How much does the item cost?
inventory = [34,54,65]  #How many of each item?

for i in range(len(items)):
    print("Item Number: ", i+1)
    print("Item: ", items[i])
    print("Cost: $", costs[i]) #Shift + 4
    print("Quantity: ", inventory[i])
    print()

maxItem = len(items)+1
print(f"What item would you like to buy (1-{maxItem})?")
itemNumber = input()
itemToBuy = items[int(itemNumber)-1]
print(f"You selected {itemToBuy}.")


