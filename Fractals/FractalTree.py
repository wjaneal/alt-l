#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 17 20:45:07 2018

@author: wneal
"""

#CopyLeft 2011 - William Neal
#This code is released under the terms of the GPL (http://www.gnu.org/copyleft/gpl.html)

from tkinter import *
from math import *


#This is a recursive function used to draw the Christmas tree in a way similar
#to the recursive technique used to draw the Koch Snowflake.
def ChristmasTree(x1,y1,x2,y2,Level,w):
    #Calculate the third and two-thirds coordinates: (x13,y13), (x23,y23)
    x13 = (2*x1+x2)/3
    x23 = (x1+2*x2)/3
    y13 = (2*y1+y2)/3
    y23 = (y1+2*y2)/3
    #Find delta x and delta y used in the vector / slope calculations of the
    #coordinates of additional points.
    deltax = x2-x1
    deltay = y2-y1
    if Level == 1:
        w.create_line(x1,y1,x2,y2,fill="green")
    elif Level % 2 == 0:
        #Determine the coordinates of the tip of the new triangle:(x12, y12)
        x12 = x1+deltax/2-deltay/3*sqrt(3)/2
        y12 = y1+deltay/2 + deltax/3*sqrt(3)/2
        #Draw two lines plus two additional recursive triangle sides.
        w.create_line(x1,y1,x13,y13,fill="red")
        ChristmasTree(x13,y13,x12,y12,Level-1,w)
        ChristmasTree(x12,y12,x23,y23,Level-1,w)
        w.create_line(x23,y23,x2,y2,fill="red")
    elif Level %2 == 1:
        #Determine the coordinates of the corners of the new square:(x13z, y13z), (x23z,y23z)
        x13z = x1+deltax/3-deltay/3
        y13z = y1+deltay/3+deltax/3
        x23z = x1+2*deltax/3-deltay/3
        y23z = y1+2*deltay/3+deltax/3
        #Draw two lines plus three additional recursive box sides.
        w.create_line(x1,y1,x13,y13,fill="darkgreen")
        ChristmasTree(x13,y13,x13z,y13z,Level-1,w)
        ChristmasTree(x13z,y13z, x23z,y23z,Level-1,w)
        ChristmasTree(x23z,y23z,x23,y23,Level-1,w)
        w.create_line(x23,y23,x2,y2,fill="darkgreen")
    return 0
master = Tk()
w = Canvas(master, width=2000, height=1000)
w.pack()
#Draw the base at recursive level 2 for the trunk
Result = ChristmasTree(150.0,750.0,950.0,750.0,2,w)
#Draw the sides at a higher recursive level for decorations
Result = ChristmasTree(950.0,750.0,550.0,750.0-800.0*(sqrt(3.0)/2.0),7,w)
Result = ChristmasTree(550.0,750.0-800.0*(sqrt(3.0)/2.0),150.0,750.0,7,w)




#photo = Tkinter.PhotoImage(width=320, height=320)

#for x in range(0, 100000):
##    y = abs(int(f(x/100.0)))
 #   pixel(photo, (x,y), (255,0,0))  # One lone pixel in the middle...

#label = Tkinter.Label(root, image=photo)
#label.grid()
 
mainloop()

