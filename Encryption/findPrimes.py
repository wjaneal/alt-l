#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 14:37:24 2020
This script finds prime numbers within a given range  (Python range of variable a)
It returns a list of the primes in that range.
@author: wneal
"""
primes = []

for a in range(10000000,10001000):
    #print("Now, we are checking ",a)
    primeCount = 0
    #Checking all numbers up to that number:
    for b in range(1,a+1):
        if a%b==0:
            #print(a," is divisible by ",b)
            primeCount+=1
            #print(a, "divides evenly by ",b)
        if primeCount > 2:
            #print(a, " has been declared 'not prime'; skipping rest of loop")
            break #Skip the rest of the repetition in this loop
    if primeCount == 2:
        #print(a, " is prime")
        primes.append(a)
print(primes)


#1000931
#1000999
#10000993
#10000987