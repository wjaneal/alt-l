#Flower Sketcher
#Copyleft 2020, William Neal
#Uses Python Visual Module to Model A Flower


#Import the required modules for math and graphics:
import math
from vpython import *

#Set a scale factor to adjust the resolution of the drawing
Scale = 0.001
k=0
A=0.1


#Draw points to show where the fly has been:
P = points(pos = [(0.000001,0,0)], color = color.red,radius = 1)

#Calculate the path of the fly using a loop i
for k in range(0,9):
    for t in range(1,4200):
        #Calculate the new angles based on the scale
        Radius = t*Scale
        Theta = A*sin(Radius)+2*pi*k/9
        #Determine the (x,y,z) Coordinates using the transformation function:
        Coords = (Radius*cos(Theta),Radius*sin(Theta),0)
        #Place a new point on the screen where the fly is now
        P.append(Coords)
        #Adjust the rate of the loop so that the animation can be seen:
rate(100000)
    
