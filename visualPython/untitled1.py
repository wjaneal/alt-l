#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 15:07:49 2020
Questions and Answers
@author: wneal
"""
#Python dictionaries

Question  = {"Q":"What is the capital of France?", "A":"Paris", "M":["Paris","Nice", "Marseilles", "Bern"]}
Questions = [{}, 
             {}, 
             {},
             {},
             {}]
print(Question["Q"])
print(Question["A"])
for item in Question["M"]:
    print(item)
    
done = 0
while done == 0:
    a = input("Is the answer correct?")  #At this point in the program you may
    if a == "y":
        print("We will prepare the next question then.")
    else:
        print("Incorrect answer!  You are out!!")
        done = 1
        
    