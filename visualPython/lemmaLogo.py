from __future__ import division, print_function
#Lemma Logo Generator
#Copyleft 2020, William Neal
#Uses Python Visual Module to (Accidentally) Model the Logo of "Lemma Education" 


#Import the required modules for math and graphics:
from vpython import *
from math import *
#Set a scale factor to determine the time interval for each calculation:
Scale = 0.001



#Draw a central point:
P = points(pos = [(0.000001,0,0)], color = color.red,radius = 1)
#sphere(pos=vec(0, 0, 0), radius=2, color=color.cyan)
'''
#Calculate the path using a loop 
for t in range(1,200000):
    #Calculate the new angles based on the scale
    Theta =20*t*Scale
    Radius = sin(t*Scale)
    #Radius = cos(t*Scale)


    #Determine the (x,y,z) Coordinates using the transformation function:
    Coords = (Radius*cos(Theta),Radius*sin(Theta),0)
    #Place a new point on the screen
    P.append(Coords)
    #Adjust the rate of the loop so that the animation can be seen:
    rate(100)

'''
