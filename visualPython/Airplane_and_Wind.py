#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 12:30:49 2020

@author: wneal
"""
from math import *
aspeed = 300 #This is in km/h
adir = 25 #This is in degrees

wspeed = 100 #This is in km/h
wdir = 160 #This is in degrees

def rad(x): #Converts from degrees to radians
    return pi*x/180

def deg(x): #Converts from degrees to radians
    return 180*x/pi


ax = aspeed*cos(rad(adir))

ay = aspeed*sin(rad(adir))

wx = wspeed*cos(rad(wdir))

wy = wspeed*sin(rad(wdir))

rx = ax+wx
ry = ay+wy

r = sqrt(rx**2+ry**2)
rdir = deg(atan(ry/rx))

print("The resulting ground speed is ", r, ".")
print("The resulting direction is: ",rdir)

