#Neural Links
#Copyleft 2020, William Neal
#Uses Python Visual Module to Display a set of spheres interlinked by lines
#Calculations are in cartesian coordinates

#Import the required modules for math and graphics:
import math
from vpython import *
from random import *

#Set a scale factor to determine the time interval for each calculation:
Scale = 0.001
minX = -100
maxX = 100
minY = -100
maxY = 100
minZ = -100
maxZ = 100

numSpheres = 100
spheres = []
for i in range(0,numSpheres):
    xi = int(random()*(maxX-minX))+minX
    yi = int(random()*(maxY-minY))+minY
    zi = int(random()*(maxZ-minZ))+minZ
    spheres.append(vector(xi*yi,yi*zi,zi*xi))

#Draw points to show where the fly has been:
P = points(pos = [(0.000001,0,0)], color = color.white,radius = 3   )
C = curve(color=color.purple, radius=1)
#Draw a vector to show where the fly is:
#Fly = arrow(pos = vector(0.000001,0,0), axis = vector(0,0,1), shaftwidth = 0.1)
#Draw a sphere to represent the surface on which the fly flies:
#ball = sphere(pos=vector(0,0,0), radius=1, opacity = 0.4)

#Calculate the path of the fly using a loop 
for i in range(0,numSpheres):
    Ci = spheres[i]
    P.append(Ci)
    for j in range(0,numSpheres):
        Cj = spheres[j]
        if i != j:
            C = curve(color=color.purple, radius = 1)
            C.append(Ci,Cj)
    
    
