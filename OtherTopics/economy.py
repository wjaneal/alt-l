#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 19:36:00 2020
Economy Simulator
@author: wneal
"""
from random import * #Import random numbers - like dice.
size = 100 #How many people.
money = 10 #Amount of money each person starts with
economy = [money]*size #Generate the "ecnonomy"
print(economy) #Print the initial state of the economy
iterations = 1000000 #How many transactions to simulate
for i in range(iterations): #Repeat the process this many times
    a = int(random()*size) #Select a random person
    b = int(random()*size) #Select another random person
    if a != b: #If they are not the same person
        coinToss = random() #Toss a coin
    if coinToss > 0.5 and economy[a]>0 and economy[b]>0:  #If they both have cash
        economy[a] -=1
        economy[b] +=1
    if coinToss <0.5 and economy[a]>0 and economy[b]>0:
        economy[a] +=1
        economy[b] -=1
economy.sort()
print(economy)
for i in range(0,101):
    count = 0
    for j in range(len(economy)):
        if economy[j] == i:
            count+=1
    print(i, count)