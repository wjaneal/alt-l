#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 09:44:45 2020

@author: wneal
"""
import matplotlib.pyplot as plt
import numpy as np

a= 1
b=6
c=1

xmin = -10
xmax = 10
resolution = 0.01


def quadraticPlot(a,b,c,xvals):
    return np.multiply(np.square(xvals),a)+np.multiply(xvals,b)+np.add(0,c)




xvals = np.arange(xmin,xmax,resolution)      
yvals = quadraticPlot(a,b,c,xvals)    
plt.axhline(0, color='black')
plt.axvline(0, color='black')         
plt.xlabel("x")
plt.ylabel("y")
plt.plot(xvals, yvals)
ax=plt.gca
arrowed_spines(ax)
plt.show() 