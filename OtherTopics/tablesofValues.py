#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 12:30:41 2020

@author: wneal
"""

#Tables of Values - Sine and Cosine
from math import *
print("x, sin(x), (degrees)")
for i in range(0,1500,15):
    print(sin(pi*i/180))#To work in degrees we multiply by Pi and divide by 180
    