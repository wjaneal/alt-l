#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 11:30:02 2020

@author: wneal
"""
'''

#Zeno's paradox - a.......................................b
distance = 10
d = distance
progress = 0
while progress < distance:
    progress +=d/2
    d/=2
    print("Distance target: ",distance, "Curre
    nt Length: ", d, "Progress: ", progress)
    
'''
#Geometric Series Also Illustrate the Idea of a Limit:
from math import *
#Total Sum of a Geometric Series:

#Term by Term Sum a(r^n-1)/(r-1)
def geoTermSum(a, r, n):
    return a*(r**n-1)/(r-1)

def arithTermSum(a, d, n):
    return (n/2)*(2*a+(n-1)*d)

a1=7
d = -8
a2 = 5
r = -9

for n in range(1,10):
    aSum = arithTermSum(a1, d, n)
    gSum = geoTermSum(a2,r,n)
    print(aSum, gSum, gSum-aSum)
    

    






