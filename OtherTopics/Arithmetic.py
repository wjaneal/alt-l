#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 21:42:07 2020
Arithmetic Sequences and Series
@author: wneal
"""

#This generates an arithmetic sequence
a=5
d=3
n=10
for i in range(1,n+1):
    print(a+(i-1)*d)
    
    
#Total = 0
#for i in range(1,n+1):
#    Total+=a+(i-1)*d #Add the general term to the total
#print(Total)

#Apply the formula for the sum of the series:
def arithmeticSeriesSum(a, d, n):
    return (n/2)*(2*a+(n-1)*d)

print(arithmeticSeriesSum(a, d, n))






