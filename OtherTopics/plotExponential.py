import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-10,10,num=100) #Set up 100 points from x=-10 to x=10
a=2
fx = [] #Set up an empty set of points
for i in range(len(x)):
    fx.append(a**(x[i]))
    
plt.plot(x,fx) #Draw a plot of the function.
plt.grid() #Make a grid
plt.axhline() #Draw a horizontal line for the x-axis
plt.axvline() #Draw a vertical line for the y-axis
plt.show()  #Show the graph
