#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 23:06:56 2020
Pascal's/ Yang Hui's Triangle
@author: wneal
"""

#Method 1: Combinations 
def factorial(n):
    if n == 1 or n==0:
        return 1
    else:
        return n*factorial(n-1)

def combination(n,r):
    return factorial(n)/(factorial(r)*factorial(n-r))
numRows = 30
for i in range(numRows):
    for j in range(0,i+1):
        print(combination(i,j)," ",end="")
    print()
    
 
#Method 2: Adding from Above.
Row=[[1]]
numRows = 128
n=1
while n < numRows:
    #Start a new row with a 1.
    Row.append([1])
    #If the length of the previous row is greater than 1, 
    if len(Row[n-1])>1:
        for i in range(0,len(Row[n-1])-1):
            Row[n].append(Row[n-1][i]+Row[n-1][i+1]) 
            #Append new terms added from the previous row
    Row[n].append(1)
    n+=1
for row in Row:
    print(row)
    

for row in Row:
    for item in row:
        if item%2==0:
            print(" ",end='')
        else:
            print("@", end="")
    print()


