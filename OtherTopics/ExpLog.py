"""
Spyder Editor

This is a temporary script file.
"""
from math import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

a1 = 2
a2 = 3
a3 = 4
a4 = 5
dl =a2+0.1
dh = dl + 3

expxint = a1+a4
print("Exponential function y intercept is: ", expxint )
logxint = (0.5**(-a1/a4))/a3+a2
print("Log function y intercept is: ", logxint)
print("2a: Vertical Translation of "+str(a4)+", Horizontal Compression of "+str(a2))
print("2a: Vertical stretch of "+str(a1))
print("2b: Vertical translation of "+str(a1)+", Horizontal Translation of "+str(a2))
print("2b: Horizontal Compression of "+str(a3)+ ", Vertical Stretch of "+str(a4))
print("Log function x intercept: ", logxint)
print("3a: y = (1/"+str(a2)+")log\("+str(a3)+")((x-"+str(a4)+")/"+str(a1)+")")
print("3b: y = (1/"+str(a3)+")(1/2)^((x-"+str(a1)+")/"+str(a4)+")+"+str(a2))
print("4a: No Solution")
print("4b: x = log\("+str(a3)+"/"+str(a1)+") "+str(a1)+"^("+str(a2-a4)+") -"+str(a4)) 
print(log(a1**(a2-a4))/log(a3/a1)-a4)
numteams = 6
numpoints = 10
sumsquares =[[0 for i in range(numpoints)]for j in range(numteams)]
#lookup = {0:f1(x),1:f2(x),2:f3(x),3:f1(x),4:f2(x),5:f3(x)}

#print(lookup)
#Define some functions: f,g,h
def f(x):
    return 0

def g(x):
    return a1*a3**(a2*x)+a4 

def h(x):
    return a4*(log(a3*(x-a2))/log(1/2))+a1

#Set the resolution of the plot
#This determines how many points to plot in each interval of length 1
res = 10 
#points = [1,0.7,0.5,0,-0.55,-0.71,-0.49,0.01,0.47,0.75]
#Create x values and y values for three graphs
x = []
x3=[]
y1 = []
y2 = []
y3 = []


for i in range(-1*res,3*res):
    x.append(i/res)
    y1.append(f(i/res))
    y2.append(g(i/res))
#Calculate the x and y values
for i in range(int(dl*res),int(dh*res)):
    x3.append(i/res)
    y3.append(h(i/res))
#Plot the three sets of x values against the y values:
plt.plot(x,y1)
plt.plot(x,y2)
#plt.plot(x3,y3)
#for i in range(0,len(points)):
#    plt.plot([i], [points[i]], marker='o', markersize=3, color="red")
x1, y1 = [0], [-10]
x2, y2 = [0], [10]
plt.plot([x1,x2],[y1,y2])
#ax = plt.plot(x='x', y='y', ax='ax', style='r-', label='point')
plt.show()
plt
