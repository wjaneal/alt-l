#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  7 11:16:12 2020
Investigating the relationship between the y values and the slope of exponential functions
@author: wneal
"""
from math import * 
h = 0.00000000001 #This is the initial horizontal distance to x2 from x1
def f(a, x):
    return a**x   
def IROC(a,h,x):
    return (f(a,x+h)-f(a,x))/(h) #This is the slope dy / dx
#IROC##############################################################
#You only need to change a and x
a =  2.718281828459045235360287471352662497757247093699959574966  #Select values of a from 2 to 4
b = 3
x=1  #Also try x=2, 3, 4, 5
#############################5####################################
c=(a+b)/2
print(x, f(a,x), IROC(a,h,x), f(a,x)/IROC(a,h,x))
print(x, f(b,x), IROC(b,h,x), f(b,x)/IROC(b,h,x))
print(x, f(c,x), IROC(c,h,x), f(c,x)/IROC(c,h,x))