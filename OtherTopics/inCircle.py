#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 28 12:32:14 2020

@author: wneal
"""
#Extension: Only allow valid input:  Test that x1 and y1 are numbers
#Use try...except structure and a while loop 
#Challenge:  (1)Have the user enter two coordinates:
#x1 and y1
#(2)Typecast them to float()

#(3)
#There are three possible outputs:
#(A) The point (x1,y1) is in the unit circle
#(B) The point (x1,y1) is on the unit circle
#(C) The point (x1, y1) is outside the unit circle

#if (x1**2+y1**2)>1:
    
#elif (x1**2+y1**2)==1:
    
done = False
while done == False:
    print("Please enter an integer from 1 to 10.")
    n = input()
    try:
        n=int(n)
        done = True
        if n>10 or n<1:
            done = False
            print("This integer is out of range!!")
    except:
        print("Please pay attention!!!")
    
print("Here is the number you entered:", n)   
    

    
    
    
