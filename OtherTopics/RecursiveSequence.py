#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 21:58:54 2020
Recursive Sequences
@author: wneal
"""

#A recursive sequence refers to itself.
t=1
while t < 8193:
    print(t)
    t=3*t+2 #Each new term is three times the old term plus 2.
    
    

