#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 14 11:13:13 2020

@author: wneal
"""

#Challenge:  
#Make a loop which generates x values, xi starting at a, moving towards by by the step (b-a)/n
#Calculate f(xi) for each xi value.
#Calculate the area of rectangle ‘i’: A=lw=f(xi)(b-a)/n; add this to the total.
#Print the approximate area under the curve.
from math import *
#Function:
def f(x):
    return x**7

#Basic Data:
n = 1 # Number of rectangles
a = 0
b = 2


while n < 100000000:
    w = (b-a)/n #The width of the rectangle depends on n
    Total = 0
    for i in range(0,n):
        xi = a+i*w #The current x value depends on:
        #a and the number of rectangle widths
        l = f(xi)
        Area = l*w
        Total += Area
    print("The approximate area under the curve is: ", Total, "#Rectangles: ", n)
    n*=10







