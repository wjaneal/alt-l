#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 16 21:42:17 2020
Finding Roots of a Quadratic Equation
@author: wneal
"""
from math import *
a=1
b=6
c=10

#Find the vertex:
print("The vertex of this parabola is: (",-b/(2*a),",",c-(b**2)/(4*a),")")


#Check how many roots there are:
if b**2-4*a*c > 0:
    print("There are two roots")
    print("Root 1: ", (-b+sqrt(b**2-4*a*c))/(2*a))
    print("Root 2: ", (-b-sqrt(b**2-4*a*c))/(2*a))
elif b**2-4*a*c == 0:
    print("There is one root")
    print("The root is: ", -b/(2*a))
else:
    print("There are no roots")
        
