#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  9 23:17:34 2020
Compound Interest
@author: wneal
"""
from math import *
a = 80000
r = 0.0625
n = 365
t = 5

print(a*(1+r/n)**(n*t))  #Compound interest formula


