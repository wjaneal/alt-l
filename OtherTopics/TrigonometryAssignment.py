"""
Spyder Editor

This is a temporary script file.
"""
from math import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

numteams = 6
numpoints = 10
sumsquares =[[0 for i in range(numpoints)]for j in range(numteams)]
#lookup = {0:f1(x),1:f2(x),2:f3(x),3:f1(x),4:f2(x),5:f3(x)}

#print(lookup)
#Define some functions: f,g,h
def f(x):
    return sin(x)

def g(x):
    return x*cos(x)

def h(x):
    return f(g(x))

#Set the resolution of the plot
#This determines how many points to plot in each interval of length 1
res = 10 
points = [1,0.7,0.5,0,-0.55,-0.71,-0.49,0.01,0.47,0.75]
#Create x values and y values for three graphs
x = []
y1 = []
y2 = []
y3 = []

#Calculate the x and y values
for i in range(-1*res,9*res):
    x.append(i/res)
    y1.append(f(i/res))
    y2.append(g(i/res))
    y3.append(h(i/res))
#Plot the three sets of x values against the y values:
plt.plot(x,y1)
plt.plot(x,y2)
plt.plot(x,y3)
for i in range(0,len(points)):
    plt.plot([i], [points[i]], marker='o', markersize=3, color="red")
#ax = plt.plot(x='x', y='y', ax='ax', style='r-', label='point')
plt.show()
for i in range(0,numteams):
    for j in range(0,numpoints):
        sumsquares[i][j].append((lookup[i](i))**2)
for i in range(0,numteams):
    print (sum(sumsquares[i]))