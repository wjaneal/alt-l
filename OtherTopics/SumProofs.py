#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 21 11:42:32 2020

@author: wneal
"""

def sumI2(n):
    Sum=0
    for i in range(1,n+1):
        Sum+=i*i
    return Sum

n=10
for i in range(1,n+1):
    print(i, sumI2(i),i*(i+1)*(2*i+1)/6)