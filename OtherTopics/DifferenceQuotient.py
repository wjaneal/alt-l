#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 12:15:43 2020

@author: wneal
"""

def f(x):
    return x*x

x1=5
h = 5

while h > 0.0000001:
    print(x1+h, (f(x1+h)-f(x1))/(x1+h-x1))#Slope is  m=dY/dX
    h/=10  #Divide x2 by 10
#This is the first step in understanding calculus!!!
#This program will help us to understand the idea of the "Limit"