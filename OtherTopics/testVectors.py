#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 16 18:11:06 2021

@author: wneal
"""

from math import radians, cos, sin
from random import *
from mathutils import Vector


rotationMax = 0.2
layers = 5
baseVector = Vector((0,0,1))
numLayers = 5
NUMVERTS = 5
Dphi = radians(360) / NUMVERTS
height = 1.0
def randomAxis():
    v0 = 2*random()-1
    v1 = 2*random()-1
    v2 = 2*random()-1
    mag = (v0*v0+v1*v1+v2*v2)**0.5
    return (v0/mag,v1/mag,v2/mag)


def ThreeDRotation(n,theta):
    n1 = n[0]
    n2 = n[1]
    n3 = n[2]
    return [[cos(theta)+n1*n1*(1-cos(theta)),n1*n2*(1-cos(theta))-n3*sin(theta),n1*n3*(1-cos(theta))],
            [n1*n2*(1-cos(theta))+n3*sin(theta),cos(theta)+n2*n2*(1-cos(theta)),n2*n3*(1-cos(theta))-n1*sin(theta)],
            [n1*n3*(1-cos(theta))-n2*sin(theta),n2*n3*(1-cos(theta))+n1*sin(theta),cos(theta)+n3*n3*(1-cos(theta))]]


def rotatePoint(p,r):
    c0 = r[0][0]*p[0] + r[0][1]*p[1] + r[0][2]*p[2]
    c1 = r[1][0]*p[0] + r[1][1]*p[1] + r[1][2]*p[2]
    c2 = r[2][0]*p[0] + r[2][1]*p[1] + r[2][2]*p[2]
    return (c0,c1,c2)




# calculate x,y coordinate pairs
coords = []
totalVector = Vector((0,0,0))
coords.append([Vector((cos(i * Dphi), sin(i * Dphi), 0)) for i in range(NUMVERTS)])
c = coords[0]
print("Coords", coords)
#Loop to generate next layer of coordinates
currentVector = baseVector
for i in range(layers):
    #Rotate Original Coorinates
    newLayer = []
    rotationAxis = randomAxis()
    rotationAngle = 0.2*random()-0.1
    for point in c:
        print(point,rotationAngle)
        basePoint = Vector(rotatePoint(point, ThreeDRotation(rotationAxis,rotationAngle)))+totalVector
        basePoint=basePoint+currentVector
        newLayer.append(basePoint)
    totalVector = totalVector+currentVector
    currentVector = Vector(rotatePoint(currentVector,ThreeDRotation(rotationAxis,rotationAngle)))
    coords.append(newLayer)
print(coords)     