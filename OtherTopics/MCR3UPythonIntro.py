#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 14 15:59:51 2020
Using Python to Study Mathematics
Functions
@author: wneal
"""

'''
*** A computer program can act as a mathematical tool making calculations
more efficient, allowing students to spend more time focusing on the ideas. 

*** This video will introduce a few ideas that may be used to improve mathematical
understanding through the use of the Python programming language
'''
#Topics Covered in Other Videos:

#Comments
#Mathematical Expressions
#Variables
#Decision Structures
#Loops
#Lists
#Functions
#Libraries

from math import * #Import the mathematics library
'''
#Basic Calculations - Order of Operations:
print(3*(4+2)/3+2*3+4)  #BEDMAS: Brackets first, then exponents,
#  multiplication, division, addition and then subtraction
print(4-10/5*3+2*(4-21/7))
print(4**2-10/5*3+2*(4-21/7))  # ** - exponent  4**2 = 4*4 = 16
'''

#Polynomial Functions
#This code calculates the values of two functions for
#given values of x:

def f1(x):
    return (1/2)*(x-2)**2-3  #This is a quadratic function

def f2(x):
    return -0.3*x**3-97*x-800#This is a quartic function


print(f1(5))




'''
#Rational Functions - Quotient of polynomial functions.
#Rational functions: (numerator)/(denominator)    

def f3(x):
    return 1/(x-4)

def f4(x):
    return (3*x**2-4*x+1)/(2*x**2-5*x-4)

print(f3(1000))
print(f4(243234.243432423))
'''



'''
#Trigonometric Functions
x = 4.5
print(sin(x)) #Sine
print(cos(x)) #Cosine
print(1/sin(x)) #Cosecant
print(1/cos(x)) #Secant
print(tan(x)) #Tangent
print(1/tan(x)) #Cotangent
'''


#Sequences and Series: Arithmetic Sequence
# 5,7,9,11,13,15,17,19,.....
a = 25  #a is the initial value
d = 12 #d is the difference between values
print("This is an arithmetic sequence.")
for n in range(0,4): #This is a loop - it repeats commands
    print(a+n*d) #Print out a value
'''
#Sequences and Series: Geometric Sequence
a = 5  #a is the initial value
r = 2 #5 is the ratio between values
print("This is a geometric sequence.")
for n in range(1,10):
    print (a*r**(n-1)) #Print out a value
'''

#A series is the sum of a sequence:
#Sequences and Series: Arithmetic Series
'''
def arithmeticSeries(a,d,n):
    return n/2*(2*a+(n-1)*d)

print(arithmeticSeries(5,2,10))
'''

''' 
#Sequences and Series: Geometric Series
def geometricSeries(a,r,n):
    return a*(r**(n)-1)/(r-1)
print(geometricSeries(5,2,10))
'''

#In this course, you will not be asked to program a computer.
#You will be asked to use computer programs.
#You may be interested in learning more about programming
#and feel free to incorporate this into your work.











