"""
Least Squares Fitting Program.
"""
from math import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
res = 10
#####################################################################################
# Enter the coordinates of the points here:
points = [[1,-25],[2,-10],[3,-2],[4,1.4],[5,-7],[6,-19],[7,-8],[8,5],[9,20],[10,100]]
numpoints = 10
###################################################################################### 
######################################################################################
#For this program, change the values of a, r1, r2, r3 to make a guess at the function.
a = 0.8075
r1 = 3.0
r2 = 3.8
r3 = 7.52
######################################################################################
sumSquares =[0 for i in range(numpoints)]
print(sumSquares)
#Define f(x) as a cosine function
def f(a, r1, r2, r3,x):
    return a*(x-r1)*(x-r2)*(x-r3)
for i in range(0,numpoints):
    #print(f(a,k,c,d,j))
    print(i, f(a,r1,r2,r3,points[i][0]))
    sumSquares[i]=(f(a,r1,r2,r3,points[i][0])-points[i][1])**2
print(sumSquares)
print("Results")
print("The sum of the squares of the errors is ", sum(sumSquares))
#Create x values and y values for three graphs
x = []
y1 = []
#Calculate the x and y values
for i in range(-1*res,11*res):
    x.append(i/res)
    y1.append(f(a,r1,r2,r3,i/res))   
#Plot the three sets of x values against the y values:
plt.plot(x,y1)
for i in range(0,len(points)):
    plt.plot(points[i][0], [points[i][1]], marker='o', markersize=3, color="red")
#ax = plt.plot(x='x', y='y', style='r-', label='point')
plt.xlim(0,11)
plt.ylim(-26,101)
plt.show()

    