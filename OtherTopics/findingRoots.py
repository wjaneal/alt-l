#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 22 10:11:54 2020

@author: wneal
"""
from math import *
def f(x):
    return x**3+5*x**2-29*x+5
#In the interval(a,b) there is a root:
a=2
b=10
for i in range(0,20):
    if f(a)*f(b)>0:
        print("Error: This interval does not contain a root or contains multiple roots.")
    else:
        c = (a+b)/2
        if f(a)*f(c)<0:
            b=c
        else:
            a=c
    print("Current Interval: (",a,",",b,"), Best Guess at root: :",(a+b)/2)
    
