#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 31 21:47:25 2020

@author: wneal
"""

t1 = 17
t2 = -31
n1 = 1
n2 = 7

termDifference = n2-n1
sequenceDifference = t2-t1
d=sequenceDifference/termDifference
print("The common difference is ", d, ".")
# tn = a+(n-1)d
# a = tn-(n-1)d
a = t1-(n1-1)*d
print("The first term is: ",a, ".")