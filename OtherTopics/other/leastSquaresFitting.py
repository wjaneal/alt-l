
"""
Least Squares Fitting Program.
"""
from math import *
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
res = 10
###################################################################################
# Enter the coordinates of the points here:
points = [[1,3.05],[2,4.61],[3,6.21],[4,10.07],[5,15.19]]
###################################################################################
#Change these values only:
A=1
a=2
b=3
c=4
###################################################################################
#For this program, change the values of A, a, b and c to make a guess at the function.
data = [[A,a,b,c]]
groupName=["My Guess"]
numGroups = len(groupName)
###################################################################################
numpoints = 5
sumSquares =[0 for i in range(numpoints)]
print(sumSquares)
#Define f(x) as an exponential function:
def f(A, a, b, c, x):
    return A*a**(b*x)+c
#Main Loop:
for group in range(numGroups):
    print(groupName[group])
    A=data[group][0]
    a=data[group][1]
    b=data[group][2]
    c=data[group][3]
    sumSquares =[0 for i in range(numpoints)]
    for i in range(0,numpoints):
        #print(f(a,k,c,d,j))
        #print(i, f(A,a,b,c,points[i][0]))
        sumSquares[i]=(f(A,a,b,c,points[i][0])-points[i][1])**2
    print("Square of the difference for each of the points:")
    print(sumSquares)
    print("Results")
    print("The sum of the squares of the errors is ", sum(sumSquares))