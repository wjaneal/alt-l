#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 30 12:27:48 2020

@author: wneal
"""
import matplotlib as mpl
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

mpl.rcParams['legend.fontsize'] = 10

fig = plt.figure()
ax = fig.gca(projection='3d')
theta = np.linspace(-4*np.pi , 4*np.pi , 10)
print(theta)
z = np.linspace(-2, 2, 1000)
r = z 
x = r * np.sin(theta) #Calculation of many points at once....
y = r * np.cos(theta)

#print(x)
#print(y)
ax.plot(x, y, z, label='parametric curve')
ax.legend()

plt.show()