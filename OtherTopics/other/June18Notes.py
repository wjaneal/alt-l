#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 12:07:08 2020
Problems 1-10
@author: wneal
"""

#Importing the Math Library
from math import * #From math import everything.

#Arithmetic and Calculations
a = (3*5+(3-14)+10)/17 # +,-,*,/ adding, subtracting, multiplication and division
print(a)
#Square Root
print(sqrt(1024)) #sqrt() is the square root function 
#Modulo operator - %
print(45%7) #45/7=6; 3 remainder
print(5%2) #Odd numbers have 1 remainder (%2)
print(6%2) #Even numbers have 0 remainder (%2)
print(7%2)
print(8%2)
x = 88
if x%2==0:
    print("x is even!")
else:
    print("x is odd!")
'''
#An accidental prime number calculator.....
for a in range(1,100000):
    primeCount = 0
    for b in range(1,100000):
        if a%b==0:
            #print(a," is divisible by ",b)
            primeCount+=1
        if primeCount > 2:
            break #Skip the rest of the repetition in this loop
    if primeCount == 2:
        print(a, " is prime")
'''        
#Adding Numbers with Loops

#(1) Set the total = 0
total = 0
#Make a loop
for i in range(1,10001):
    #(3) Add numbers to the total in the loop:
    total += i #(add some calculation of i to the total)
#Display the total
print(total)


#Lines: y=mx+b
#define a line:
def f(x,m,b): #Pass m and b into the function...
    return m*x+b

x = 6
m = 2
b = -1
print("If x is ", x, " m is ", m, "b is ", b, "then y is ",f(x,m,b))


def f1(x):
    return 3*x-2 #Set m and b directly
print(f1(4))