#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 12:01:55 2020

@author: wneal
"""

def f(x):
    return x*x-4

def g(x):
    return 3*sin(4*(x-3))+4

def h(x):
    return 5*log(x-2)
x=5
#These expressions demonstrate the composition of functions
f(g(x))
print(f(x),g(x),h(x),f(g(x)),f(h(x)),g(h(x)),f(g(h(x))))