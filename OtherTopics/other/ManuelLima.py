#Exercise 1

i = 1
while i <= 99:
   i += 1
   print(i)


#Exercise 2

 
sum = 0
for i in range(1,10000001):
	sum += i
print(sum)


#Exercise 3

'''
print("Input some integers to calculate their sum and average. Input 0 to show the result.")

count = 0
sum = 0.0
number = 1

while number != 0:
	number = int(input(""))
	sum = sum + number
	count += 1

if count == 0:
	print("Input some numbers")
else:
	print("Average and Sum of the above numbers are: ", sum / (count-1), sum)
'''
#Exercise 4


nl=[]
for x in range(1, 1001):
    if (x%7==0) and (x%5==0):
        nl.append(str(x))
print (','.join(nl))


#Exercise 5

"""
string = "cows" 
print(len(string)) 
  
# Length of below string is 15 
string = "Gigabytes" 
print(len(string))

string = "Python" 
print(len(string)) 
  
# Length of below string is 15 
string = "Malware" 
print(len(string))

string = "Megabytes"
print(len(string))
"""

#Exercise 6

"""
numbers = range(1, 4097)

sequence_of_numbers = []
for number in numbers:
   if number % 2 in (1, 2):
      sequence_of_numbers.append(number)

print(sequence_of_numbers)


"""

#Exercise 7

"""
print ("1, 3, 6, 10, 15, 21, 28, 36, 45, 55, 66, 78, 91, 105")
"""

#Exercise 8

"""
from math import sqrt

print('Pythagorean theorem calculator! Calculate your triangle sides.')
print('Assume the sides are a, b, c and c is the hypotenuse (the side opposite the right angle')
formula = input('Which side (a, b, c) do you wish to calculate? side> ')

if formula == 'c':
	side_a = int(input('Input the length of side a: '))
	side_b = int(input('Input the length of side b: '))

	side_c = sqrt(side_a * side_a + side_b * side_b)
	
	print('The length of side c is: ' )
	print(side_c)

elif formula == 'a':
    side_b = int(input('Input the length of side b: '))
    side_c = int(input('Input the length of side c: '))
    
    side_a = sqrt((side_c * side_c) - (side_b * side_b))
    
    print('The length of side a is' )
    print(side_a)

elif formula == 'b':
    side_a = int(input('Input the length of side a: '))
    side_b = int(input('Input the length of side c: '))
        
    side_c = sqrt(side_c * side_c - side_a * side_a)
    
    print('The length of side b is')
    print(side_c)

else:
	print('Please select a side between a, b, c')
"""

#Exercise 9

"""
def slope(x1, y1, x2, y2): 
    return (float)(y2-y1)/(x2-x1)  
       
x1 = 6
y1 = 8 
x2 = 4 
y2 = 3 
print ("Slope is :", slope(x1, y1, x2, y2))
"""

#Exercise 10

"""
def findPCSlope(m): 
  
    return -1.0 / m 
  
m = 5000
print(findPCSlope(m))
"""