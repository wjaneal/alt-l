import numpy as np
from matplotlib import pyplot as plt
# 100 linearly spaced numbers
x = np.linspace(-np.pi,np.pi,100)

# the functions, which are y = x**2+x-7 and z = x**2*x+5 here
y = x**2+x-7
z = x**2*x+5 

# setting the axes at the centre
fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)
ax.spines['left'].set_position('center')
ax.spines['bottom'].set_position('center')
ax.spines['right'].set_color('none')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')

# plot the functions
plt.plot(x,y, 'c', label='y=x**2+x-7')
plt.plot(x,z, 'm', label='y=x**(2*x)+5')

plt.legend(loc='upper left')

# show the plot
plt.show()
