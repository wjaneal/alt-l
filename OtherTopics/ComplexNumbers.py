#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 10:35:09 2020
Introducing....Complex Numbers!
@author: wneal
"""
from math import * #Use this when doing math!!!


#Function that adds two complex numbers
def addComplex(a1,b1,a2,b2):
    print("The sum of the numbers is ",a1+a2," + ",b1+b2,"i")
    
#Two complex numbers: 1+5i  and 3-9i - the sum should be 4 - 4i    
addComplex(1,5,3,-9)

#This function takes two numbers - a+bi and c+di 
#and multiplies them: (ac-bd) + (bc + ad) i

def multiplyComplex(c1,c2):
    a1 = c1[0]
    b1 = c1[1]
    a2 = c2[0]
    b2 = c2[1]
    print("The product of the two numbers is: ")
    print(a1*a2-b1*b2,"+",b1*a2+a1*b2,"i")
    return([a1*a2-b1*b2, b1*a2+a1*b2])




#Degrees to Radians
#One circle as 360 degrees and 2pi radians
def d_to_r(a):
    #print("The angle ",a,"degrees expressed in radians is", pi*a/180)
    a = pi*a/180
    return a
    
#Radians to Degrees
def r_to_d(r):
    #print("The angle ",r,"radians expressed in degrees is ",180*r/pi)
    r = 180*r/pi
    return r


print(d_to_r(360))
print(d_to_r(180))
print(r_to_d(6.283185307179586))
print(r_to_d(3.141592653589793))



#Convert Complex Number to Polar Form:
#Complex Number a+bi
def toPolar(c1):
    a = c1[0]
    b = c1[1]
    radius = sqrt(a*a+b*b)
    print("The radius (magnitude) is: ",radius)
    theta = atan(b/a) #This is in radians
    if a>=0 and b>=0:
        print("The angle (argument) is ", r_to_d(theta))
    elif a<0 and b>=0:
        print("The angle (argument) is ", 180+r_to_d(theta))
    elif a<0 and b<0:
        print("The angle (argument) is ", 180+r_to_d(theta))
    elif a>=0 and b<0:
        print("The angle (argument) is ", 360+r_to_d(theta))

def toCartesian(r,theta):
    a=r*cos(theta)
    b=r*sin(theta)
    return [a,b]

#Find the coordinates of a complex number of magnitude 1 with a given angle:
#Theta = (a Pi)/b
#a=3
#b=4
#Theta = a*pi/b
#print("The cartesian coordinates of the complex number with angle ",Theta, " radians is ",toCartesian(1,Theta),"." )


    

z=[1,0]
c=[sqrt(3)/2,1/2]
zc=multiplyComplex(z,c)
print(zc)
zcc=multiplyComplex(zc,c)
print(zcc)
zc3=multiplyComplex(zcc,c)
print(zc3)
zc4=multiplyComplex(zc3,c)
print(zc4)
zc5=multiplyComplex(zc4,c)
print(zc5)











