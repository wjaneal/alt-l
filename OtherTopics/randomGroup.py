#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 22:31:31 2020

@author: wneal
"""

from random import *
groupList = [1,2,3,4,5]
Done = False
while Done == False:
    nextGroup = int(random()*5)
    if groupList[nextGroup]!=0:
        print(groupList[nextGroup])
        groupList[nextGroup] = 0
    Done = True
    for i in range(len(groupList)):
        if groupList[i]!=0:
            Done = False

        