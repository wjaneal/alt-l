#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:44:44 2020

@author: wneal
"""
#Task 1 - List the coefficients
p1 = [1,7,12]  # 1*x**2+3*x+4
n = len(p1) #Length of the list
print("The length of the list is ", n)
print("The highest power of x is ",n-1)
for i in range(0,n): #For loop to list the items in the list
    print(p1[i])  #  p1[i] - uses the index i to print each value

print("Coefficient, Index, Power")
#Task 2 - List the index and the power


#This code evaluates a polynomial at given values of x.
fx = 0
for x in range(0,10):
    for i in range(0,n): #For loop to list the items in the list
        #print(p1[i],i,n-1-i)  #  p1[i] - uses the index i to print each value
        fx+=p1[i]*x**(n-1-i)
    print("x: ",x," f(x): ", fx)