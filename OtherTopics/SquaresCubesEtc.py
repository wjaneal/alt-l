#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 09:50:17 2020
Perfect Squares, Cubes and Other 'nth' Powers
@author: wneal
"""
#Import the math library
from math import *

#Print five lines of blank space using a print() statement and a loop
for i in range(5):
    print()
#First row of the table.
for j in range(1,11):
    print(str(j).ljust(j+3),end='')#end='' - print on the same line.
print()

for j in range(1,11):
    print(("_"*j+"___").ljust(j+3),end='')
print()

#For each base from 2 to 10:
for i in range(2,11):
    #For each power from 2 to 10:
    for j in range(1,11):
        #Print out the exponents with "ljust" for spacing.
        #end='' keeps the printout on the same line
        print(str(i**j).ljust(j+1)," ",end='')
    #Print a new row.
    print()
for i in range(5):
    print()