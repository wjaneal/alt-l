#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 21 11:27:28 2020
Special Sums: Powers
@author: wneal
"""
from math import *

#Add the '0' powers:
n = 100
Sum = 0
for i in range(1,n+1):
    Sum+=1 #add 1 to the sum
print(Sum)

#Add the '1' powers:
n = 100
Sum = 0
for i in range(1,n+1):
    Sum+=i #add 1 to the sum
print(Sum)
print(int(n*(n+1)/2))

#Add the squares:
n = 1000
Sum = 0
for i in range(1,n+1):
    Sum+=i*i #add 1 to the sum
print(Sum)
print((n*(n+1)*(2*n+1)/6))

#Add the cubes:
n = 1000
Sum = 0
for i in range(1,n+1):
    Sum+=i*i*i #add 1 to the sum
print(Sum)
print((n*(n+1)/2)**2)

Area = 0
a=0
b=10
n=1
print("Here is a set of approximations of the area as n increases:")
for i in range(0,10):
    print("n: ",n, "Area: ",a**2*(b-a)/(n)+2*a*((b-a)/(n))**2*((n)*(n+1))/2+((b-a)/(n))**3*((n*(n+1)*(2*n+1))/6))
    n*=10
print((b**3-a**3)/3)

