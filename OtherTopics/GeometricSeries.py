#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 11:30:02 2020

@author: wneal
"""
'''

#Zeno's paradox - a.......................................b
distance = 10
d = distance
progress = 0
while progress < distance:
    progress +=d/2
    d/=2
    print("Distance target: ",distance, "Current Length: ", d, "Progress: ", progress)
    
'''
#Geometric Series Also Illustrate the Idea of a Limit:
from math import *
#Total Sum of a Geometric Series:
#a/1-r
def geoSum(a,r): #|r| < 1 (infinite number of terms)
    return a/(1-r)

#Term by Term Sum a(r^n-1)/(r-1)
def geoTermSum(a, r, n):
    return a*(r**n-1)/(r-1)

a=5
r = 0.95
print(geoSum(a,r))

for i in range(1,100):
    print("n = ",i, "Sum = ",geoTermSum(a,r,i))
    






