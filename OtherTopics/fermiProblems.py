#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 12:04:50 2020
Fermi Problems
Finding answers to difficult questions
by guessing!!??
@author: wneal
"""
#Question 1 - how many cars in London?
population = 440000
familySize = 4
carsPerFamily = 2
carsInLondon = population/familySize*carsPerFamily
print(carsInLondon)

#Question 2 - how many aircraft carriers from London
#to Montreal?
distance = 250000 #metres
length = 200 #Length of an aircraft carrier
carriers = distance/length
print(int(carriers))

#Question 3 - How many Basketballs in your house.

#Question 3a - How many basketballs in a room?

#Question 3aa How many basketballs touching the floor in a room?
b = 0.3 #diameter of basketball in metres
n = 35 #basketballs on the floor - 1 layer of basketballs?
l = 7 #number  of layers of basketballs
r = n*l
print(r) #how many basketballs in a room?
bedrooms = 5
otherRooms = 9
totalRooms = bedrooms+otherRooms
totalBasketBalls = totalRooms*r
print(totalBasketBalls)

#Question 4 - how many bears in Ontario?
area=5000
bearsPerkm=2
bearsInLondon=20
bears = area*bearsPerkm+bearsInLondon
print(bears)

#Question 5 - how many piano tuners in Chicago?
population = 500000
pianosPer1000 = 150
thousandsInChicago = population/1000
print(thousandsInChicago)
pianosInChicago = pianosPer1000*thousandsInChicago
print("Pianos in Chicago:", pianosInChicago)
#pianosPerTuner = 
daysWorkedPerYear = 280
pianosPerDay = 5
pianosPerYear = pianosPerDay*daysWorkedPerYear
print("Pianos per year: ",pianosPerYear)
pianoTuners = pianosInChicago/pianosPerYear
print("Piano tuners in Chicago: ", pianoTuners)


#Question 6 - mass of all the ants in London
antsPerkg = 10000
antsPerm2 = 150
m2inLondon = 5000000
percentWithAnts = 0.35
antsInLondon = m2inLondon*antsPerm2*percentWithAnts
print(antsInLondon)
print("Mass of ants: ", antsInLondon/antsPerkg)














