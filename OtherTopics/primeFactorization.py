primes = [2,3,5,7,9,11,13]
factorization = []
print("Prime Factorization for Small Numbers")

number = int(input("Enter a positive integer larger than 1: "))

while number != 1:
   for prime in primes:
       print(number, prime)
       if number%prime == 0:
           number = number/prime
           print("We divided by ",prime, " the number is now", number)
           factorization.append(prime)
factorization.sort() #.sort() is a list method - built into Python.
print(factorization)
