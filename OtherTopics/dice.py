#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 12:43:27 2020
Dice Rolling
@author: wneal
"""
from random import *
numDice = 4
numSides = 6
trials = 1000000
results = [0]*(numSides*numDice+1)
for i in range(trials):
    total = 0
    for d in range(numDice):
        total+=int(random()*6)+1
    total
    results[total]+=1
    #print(d1, d2, result)
print(results)

