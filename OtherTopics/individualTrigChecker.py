"""
Least Squares Fitting Program.
"""
from math import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
res = 10
###################################################################################
# Enter the coordinates of the points here:
points = [[1,11.21],[2,12.67],[3,15.87],[4,18.96],[5,20.19],[6,21.34],[7,20.08],[8,18.78],[9,15.83],[10,12.55],[11,11.18]]
###################################################################################
#Change these values only:
a=5.08
# = 2pi/k
b=pi/5
c=16.66
d=6
###################################################################################
#For this program, change the values of A, a, b and c to make a guess at the function.
data = [[a,b,c,d]]
groupName=["My Guess"]
numGroups = len(groupName)
###################################################################################
numpoints = len(points)
sumSquares =[0 for i in range(numpoints)]
print(sumSquares)
#Define f(x) as an exponential function:
def f(a, b, c, d, x):
    return a*cos(b*(x-d))+c
#Main Loop:
for group in range(numGroups):
    print(groupName[group])
    a=data[group][0]
    b=data[group][1]
    c=data[group][2]
    d=data[group][3]
    sumSquares =[0 for i in range(numpoints)]
    for i in range(0,numpoints):
        #print(f(a,k,c,d,j))
        #print(i, f(A,a,b,c,points[i][0]))
        sumSquares[i]=(f(a,b,c,d,points[i][0])-points[i][1])**2
    print("Square of the difference for each of the points:")
    print(sumSquares)
    print("Results")
    print("The sum of the squares of the errors is ", sum(sumSquares))
