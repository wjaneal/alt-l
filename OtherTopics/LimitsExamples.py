#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 11:30:02 2020

@author: wneal
"""

#Zeno's paradox with a while loop - the computer finds the "limit"
#This is acutally due to a precision error in the calculation
distance = 10
d = distance
progress = 0
while progress < distance:
    progress +=d/2
    d/=2
    print("Distance target: ",distance, "Current Length: ", d, "Progress: ", progress)
    
    
#Geometric Series Also Illustrate the Idea of a Limit:
from math import *
#Total Sum of a Geometric Series:
#a/1-r
def geoSum(a,r):
    return a/(1-r)

#Term by Term Sum a(r^n-1)/(r-1)
def geoTermSum(a, r, n):
    return a*(r**n-1)/(r-1)

a=5
r = 1/2
print(geoSum(a,r))
# 5, 7.5, 8.75....
for i in range(1,100):
    print("n = ",i, "Sum = ",geoTermSum(a,r,i))
    


# 1/n - added for n = 1, 2......
#This series has no limit!!!!
#1/1 + 1/2 + 1/3 .....1/n where n-> infinity
Total = 0
for i in range(1, 100):
    Total += 1/i
    print(i, Total)
    
# 1/n^2 - added for n = 1, 2, .....
#This one has a  maximum beyond which the sum never passes.
#1/1 + 1/4 + 1/9 + 1/16.....+1/(n^2) where n-> infinity
Total = 0
for i in range(1, 100):
    Total += 1/(i*i)
    print(i, Total)








