for a in range(1,10):
    print("Now, we are checking ",a)
    primeCount = 0
    #Checking all numbers up to that number:
    for b in range(1,10):
        if a%b==0: #Does b divide into a evenly?
            #print(a," is divisible by ",b)
            primeCount+=1 #Adds one to the primeCount.
            print(a, "divides evenly by ",b)
        if primeCount > 2:
            print(a, " has been declared 'not prime'; skipping")
            break #Skip the rest of the repetition in this loop
    if primeCount == 2:
        print(a, " is prime")