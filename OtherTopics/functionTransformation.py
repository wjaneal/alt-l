#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 15 22:21:51 2020

@author: wneal
"""
from math import * #Include this at the beginning of each file

#Transformations of functions

def f1(x):
    return (x+1)*(x+2)/(x+3)*(x+4)

def f2(x):
    return (x+1)*(x+2)/((x+3)*(x+4))

a = 5
b = 4
c = -4
d = 12
x = 2
print(a*f1(x))  #Vertical stretch of f1
print(f2(x-d))#Horizontal Translation of f2 
print(f1(x)+c)#Vertical translation of f1
print(f1(b*x))#Horizontal compression












