"""
Least Squares Fitting Program.
"""
from math import *
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
res = 10
#####################################################################################
# Enter the coordinates of the points here:
#(1,90), (2, 1441), (3, 24300), (4, 410854), (5, 6947900)

points = [[1,90],[2,1441],[3,24300],[4,410854],[5,6947900]]
###################################################################################### 
######################################################################################
#For this program, change the values of a, r1, r2, r3 to make a guess at the function.
data = [[5.02345,2.0343,3.9822,4.56565],[666/120,50/3,1,1],[320/83,41/7,1,1],[5.1960323,11.6632,1,0],[1/90,2/41,4,5]]
groupName=["The Local Function", "GM","Mathbiss", "Tree New Bee", "123"]
numGroups = len(groupName)
#####################################################################################
numpoints = 5
sumSquares =[0 for i in range(numpoints)]
print(sumSquares)
#Define f(x) as a cosine function
def f(A, a, b, c, x):
    return A*a**(b*x)+c

for group in range(numGroups):
    print(groupName[group])
    A=data[group][0]
    a=data[group][1]
    b=data[group][2]
    c=data[group][3]
    sumSquares =[0 for i in range(numpoints)]
    for i in range(0,numpoints):
        #print(f(a,k,c,d,j))
        #print(i, f(A,a,b,c,points[i][0]))
        sumSquares[i]=(f(A,a,b,c,points[i][0])-points[i][1])**2
    print(sumSquares)
    print("Results")
    print("The sum of the squares of the errors is ", sum(sumSquares))
#Create x values and y values for three graphs
'''
x = []
y1 = []
#Calculate the x and y values
for i in range(-1*res,11*res):
    x.append(i/res)
    y1.append(f(A,a,b,c,i/res))   
#Plot the three sets of x values against the y values:
plt.plot(x,y1)
for i in range(0,len(points)):
    plt.plot(points[i][0], [points[i][1]], marker='o', markersize=3, color="red")
#ax = plt.plot(x='x', y='y', style='r-', label='point')
plt.xlim(0,5)
plt.ylim(0,500000)
plt.show()
'''
    