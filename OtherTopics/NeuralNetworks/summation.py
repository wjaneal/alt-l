#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 21:07:39 2020
Summation
@author: wneal
"""
LowerLimit = 1
UpperLimit = 12

def formula(k):
    return 3*k-8

Total = 0
for k in range(LowerLimit, UpperLimit+1):
    Total+=formula(k)
    print(formula(k))
print("Here is the total of all the terms: ",Total)
