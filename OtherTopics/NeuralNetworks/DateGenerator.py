#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  3 17:41:41 2020
Program to Compare a List of Dates to a Master List of Dates and Insert Missing Dates.
@author: wneal
"""

import pandas as pd
d = list(pd.bdate_range(start='1/1/2010', end='1/1/2015'))
print(d)
masterDates = []  #Master list of dates
palladiumDates = [] #List of Dates for Comparison
pD = [] #New updated list of dates

#Cast the date string of each date in the master list in a particular format
for item in d:
    masterDates.append(item.strftime("%Y-%m-%d"))

print(masterDates)

#Read the compared list of dates from a file
f = open("palladium.csv","r")
for item in f:
    i = item.split(",")
    palladiumDates.append(i[0]) #For now, just deal with the date, not the attached data
 
#print(masterDates)
print(palladiumDates)


mi = 0 #set the index for the master list of dates to 0
pi = 0 #set the index for the caompared list of dates to 0
currentData = palladiumDates[0]
while mi < len(masterDates) and pi < len(palladiumDates): #Loop through each list of dates until the end of one of the lists is reached.
    print(len(masterDates),len(palladiumDates))
    #print(type(masterDates[m]),type(palladiumDates[p]))
    #print(len(masterDates[m]),len(palladiumDates[p]))
    if str(masterDates[mi]) == str(palladiumDates[pi]): #Compare two dates
        print("Equal")  #If they are equal, generate a date string to append to the new list
        print(palladiumDates[pi])
        y = palladiumDates[pi][:4]
        m = palladiumDates[pi][5:7]
        d = palladiumDates[pi][8:10]
        element = (pd.Timestamp(year=int(y),month=int(m),day=int(d)))
        element = str(element)[:11]
        print("Inserted: ", str(element))
        palladiumDates.insert(pi,element)
        pD.append(palladiumDates[pi])
        mi+=1
        pi+=1    
    else:
        pD.append(masterDates[mi]) #If they are not equel, insert a date from the master list.
        #Insert the data too
        #Update the currentData
        mi+=1
    #currentData = palladiumDates[pi]
    
print("M")
print(masterDates)
print("P")
print(pD)
print(len(masterDates),len(pD))
