#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 21:58:06 2020

@author: wneal
"""

# Example Python Program to plot a polar plot of a circle

# import the numpy and pyplot modules
import numpy as np
import matplotlib.pyplot as plot

plot.axes(projection='polar')

# Set the title of the polar plot
plot.title('Circle in polar format:r=R')

# Plot a circle with radius 2 using polar form
rads = np.arange(0.1, 100, 0.01)

for radian in rads:
    r = radian  #The radius is two times the sine of the angle.
    plot.polar(radian,r,'o') 
     
# Display the Polar plot
plot.show()