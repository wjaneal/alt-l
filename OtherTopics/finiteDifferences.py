#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  2 09:22:23 2020
Finite Differences
@author: wneal
"""


from math import *

#Feel free to make your own polynomial functions here:
def f1(x):
    return 3*x+2

def f2(x):
    return x**2+3*x+2

def f3(x):
    return 5*x**3+x**2+3*x+2

def f4(x):
    return 3*x**4-5*x**3+x**2+3*x+2

def f5(x):
    return x**7-5*x**5+3*x**4+x**2+3*x+2

numPoints = 10
startingPoint = -5
####################################################
levels = 10 #You may increase this if you wish
####################################################
diff = []
for i in range(0,levels+1):
    diff.append([])
for i in range(startingPoint, startingPoint+numPoints):
    ##################################################
    diff[0].append(f5(i)) #Name of the function 
    ##################################################
for level in range(1,1+levels):
    for i in range(len(diff[level-1])-1):
        print(diff[level-1])
        diff[level].append(diff[level-1][i+1]-diff[level-1][i])
print("The function values are as follows:")
print(diff[0])
for i in range(1,levels+1):
    print("Finite differences ", i)
    print(diff[i])
