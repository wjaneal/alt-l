import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-1.5,1.5,num=100) #Set up 100 points from x=-10 to x=10
A=2
a=2
b=2
c=-1
d=0
fx = [] #Set up an empty set of points
fxbase=[]
for i in range(len(x)):
    fx.append(    c)
    fxbase.append(A**(x[i]))
plt.plot(x,fx) #Draw a plot of the function.
plt.plot(x,fxbase)
plt.grid() #Make a grid
plt.axhline() #Draw a horizontal line for the x-axis
plt.axvline() #Draw a vertical line for the y-axis
plt.show()  #Show the graph
