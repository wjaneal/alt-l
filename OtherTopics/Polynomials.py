#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:03:07 2020
Working with Polynomials

@author: wneal
"""

#Let us represent a polynomial using a list:

#p1 = 5x^4-12*x^3+3*x^2+7*x^1+8
p = [2,3]
p1 = [5,-12,3,7,8]
p2 = [1,7,3,8,-9,3]
p3 = [1,0,0,0,0,0,0,0]

#Show the coefficients of the polynomial:
num = len(p1)
print("There are ", num, "coefficients in p1")

#List the coefficients in order:
'''
Sample output:
5
-12
3
7
8
'''


#List the coefficients, their indices and indicate the power of x
'''
Sample output:
Coeff.  Index Power
5       0    4
-12     1    3
3       2    2
7       3    1
8       4    0
'''

#Evaluate a polynomial for a certain value of x
#f(x) = 5*x**4-12*x**3+3*x**2+7*x+8
#Use a loop and add each term
S = 0  #Set the sum to 0
for i in range(0,5):
    #.....add to the sum in here.
print("f(x)= ", S) 



