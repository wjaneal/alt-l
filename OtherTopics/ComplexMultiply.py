#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 21:06:46 2020
Complex Numbers
@author: wneal

"""

#Complex numbers - the heart of mathematics
#z = a+bi; a and b are Real numbers, i = sqrt(-1)
#z1 = a1+b1i; z2 = a2+b2i
from math import *


def complexMultiply(z1,z2):
    a1=z1[0]
    b1=z1[1]
    a2=z2[0]
    b2=z2[1]
    return[a1*a2-b1*b2,a1*b2+a2*b1]
    
z1 = [-1/2,-sqrt(3)/2] #Angle of 0 degrees/
z2 = [-1/2,-sqrt(3)/2]  #This is 30 degrees
print("0 ", z1)
number = 15 #Set a number of multiplications
for i in range(number):
    z = complexMultiply(z1, z2)
    print(str(i+1)+" ", z)
    z1=z
    








