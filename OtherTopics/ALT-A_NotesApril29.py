#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 29 11:41:47 2020
Notes - Programming Challenges - Problems 1-10
@author: wneal
"""


#Modulo operator - remainder of a division
print(42%5)  #42/5 == 8; Remainder == 2

'''
#Type casting: Change a string to a number
#Cast to an integer:
age = int(input("Please enter your age")) #int = integer function
#Changes text or string to an integer.
print(age)

height = float(input("Please enter your height"))
#Changes to a floating point number - with decimals
print(height)

#The length of words: - Use len function
word = "automatically"
print("The length of the word is ", len(word))
'''

list1 = ["Russia", "Ukraine", "Egypt"]
list1.append("Mexico")  #Add an item to the end of the list
print(list1) #The append method adds an item onto a list

#Add an item onto a list:
list3 = []
for i in range(1,100):
    list2.append(i)  #Append the i values as integers
print(list2) 

#The math module - allows for math calculations.
#To use math functions (aside from +,-,*,/,%) import the math module (library)
from math import * #This is the math library - imported
a = 5
print(sqrt(a), sin(a), cos(a), cosh(a), sinh(a), atan(a))


