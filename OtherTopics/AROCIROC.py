#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  7 11:16:12 2020
Calculating AROC and IROC
@author: wneal
"""
from math import * 

def f(x):
    a=5
    b=8
    c=4
    return a*c**b   
    #return x**4-20*x**3+140*x**2-400*x+383   
#AROC
x1 = 0
h = 0.0000001
x2 = x1+h
y1 = f(x1)
y2 = f(x2)
print((y2-y1)/(x2-x1))

#IROC##############################################################
#You only need to change x1 and f(x) to find the IROC of a function.
x1 = 10  #This is our initial guess, a2
#############################5####################################
h = 1 #This is the initial horizontal distance to x2 from x1
x2 = x1+h
while h>0.0000001:
    #The last number is the approximate slope at x=x1
    print(x1,h,(f(x1+h)-f(x1))/(h)) #Calculate the slope
    m = (f(x1+h)-f(x1))/(h)
    h = h/10
#print("f(an) is", f(x1))
print("The slope at x = ",x1, "is about ", m)
#Print an-f(an)/mn - the new guess
#print("The next guess for the root is: ",x1-f(x1)/m) #This is a3
'''

difference = 1
h = 0.0000000001
while difference > 0.000001:
    m = (f(x1+h)-f(x1))/(h)
    print(x1, f(x1),m, x1-f(x1)/m)
    new_x1 = x1-f(x1)/m
    if abs(new_x1-x1)<difference:
        difference = abs(new_x1-x1)
    x1 = new_x1
 '''   