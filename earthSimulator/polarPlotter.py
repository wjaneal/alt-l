#Polar Plotter
#Copyleft 2020, William Neal
#Uses Python Visual Module to plot curves using polar coordinates

#Import the required modules for math and graphics:
import math
from vpython import *

#Set a scale factor to determine the time interval for each calculation:
Scale = 0.01



#Draw points:
P = points(pos = [(0.000001,0,0)], color = color.red,radius = 1)
##############################
#Adjust these:
tmin = 1
tmax = 100000
##############################
#Calculate the curve's points using a loop 
for t in range(tmin,tmax):
    ###############################################
    #Calculate the new angles based on the scale
    #Adjust these:
    Theta =t*Scale   #These are parametric equations
    a = 1.9
    b = 2.7
    c = -0.6
    Radius = a*sin(Theta)+(sin(b*(Theta/c)))**3# The parameter t adjusts Theta and Radius
    ###########################adius = a+b*cos(Theta)#####################
    #Determine the (x,y,z) Coordinates using the transformation function:
    Coords = (Radius*cos(Theta),Radius*sin(Theta),0)
    #Place a new point on the screen
    P.append(Coords)
    #Adjust the rate of the loop so that the animation can be seen:
    rate(100)
    
