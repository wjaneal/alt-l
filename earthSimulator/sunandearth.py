#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 12:43:51 2020
Force of the Sun on the Earth

F = (GM1M2 )/(R2) 

F - Force between 2 objects.
G - Gravitational Constant
M1 Mass of object 1
M2 Mass of object 2
R - Distance between the centres of each of the two objects.

@author: wneal
"""

from math import *   #Force:   kg m/s2 (Force - Newton)
G = 6.67*10**(-11)  #m3 kg–1 s−2 
M1 = 1.989*10**30  #kg
M2 = 5.972*10**24  #kg
R = 1.5676*10**11  #m
print(G, M1, M2,R)


def F(G,M1,M2,R):
    return G*M1*M2/(R**2)


#F = ma
#What is the acceleration of the earth towards the sun?


def a(F,m):
    return F/m

print(a(F(G,M1,M2,R),M2))


print("The force of the sun on the earth is: ",F(G,M1,M2,R)," Newtons")
print(a(F(G,M1,M2,R),M2)) #Every second, how
#many metres per second does the earth accelerate
#towards the sun?
print(asin(a(F(G,M1,M2,R),M2)/29000)*180/pi)

