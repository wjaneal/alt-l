#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 12:52:51 2020

@author: wneal
"""

from math import *
v1= 50
A=0.00001
Arad=pi*A/180
vx=v1*cos(Arad)
vy=v1*sin(Arad)
print(vx,vy)