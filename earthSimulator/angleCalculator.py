#Earth Simulator
#Copyleft 2020, William Neal
#Uses Python Visual Module to Model the flight of the earth around the sun


#Import the required modules for math and graphics:
from math import *
#from vpython import *


#Data - Earth and Sun


#Routine - Find the angle from x and y
def getAngle(x,y):
    yabs = abs(y) #Take the "absolute value" of y
    xabs = abs(x) #Take the "absolute value" of x
    A = atan(yabs/xabs) #This is in radians
    A = deg(A)
    if x<0:
        if y<0:
            return 180+A
        else:
            return 180-A
    else:
        if y<0:
            return 360-A
        else:
            return A
    
#Convert from radians to degrees:
def deg(r): #Takes a radian quantity and returns degrees    
   return 180*r/pi 
    
def rad(d): #Takes a radian quantity and returns degrees    
   return pi*d/180 
    
    
print(getAngle(1,1))
print(getAngle(-1,1))
print(getAngle(-1,-1))
print(getAngle(1,-1))
    
    
    
    
'''

#Graphics Portion

#Set a scale factor to determine the time interval for each calculation:
Scale = 0.001
#Draw points to show where the fly has been:
P = points(pos = [(0.000001,0,0)], color = color.red,radius = 1)
##############################
#Adjust these:
tmin = 1
tmax = 20000
##############################
#Calculate the path of the fly using a loop 
for t in range(tmin,tmax):
    ###############################################
    #Calculate the new angles based on the scale
    #Adjust these:
    Theta =11*t*Scale   #These are parametric equations
    Radius = sin(t*Scale) # The parameter t adjusts Theta and Radius
    ################################################
    #Determine the (x,y,z) Coordinates using the transformation function:
    Coords = (Radius*cos(Theta),Radius*sin(Theta),0)
    #Place a new point on the screen where the fly is now
    P.append(Coords)
    #Adjust the rate of the loop so that the animation can be seen:
    rate(100)
    
'''