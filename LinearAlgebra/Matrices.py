#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  8 13:45:36 2020
Determinant and Inverse of a 3*3 matrix
@author: wneal
"""



def Det2(A):
    a = A[0][0]
    b = A[0][1]
    c = A[1][0]
    d = A[1][1]
    Det = a*d-b*c
    return Det


def Det3(A):
    Det = 0
    for i in range(len(A[0])):
        ASub = [[A[1][(i+1)%3],A[1][(i+2)%3]],[A[2][(i+1)%3],A[2][(i+2)%3]]]
        print(ASub)
        Det+=A[0][i]*Det2(ASub)
    print("The determininant is", Det)
    return Det
        
def Det4(A):
    Det = 0        
    for i in range(len(A[0])):
        ASub = [[A[1][(i+1)%3],A[1][(i+2)%3]],[A[2][(i+1)%3],A[2][(i+2)%3]]]
        print(ASub)
        Det+=A[0][i]*Det2(ASub)

a = 1345
b = 4422
c = 214125
d = 7124412
e = -2412
f = 3241
g = -3142
h = 7412
i = 8412

A = (e*i-f*h)
B = -(d*i-f*g)
C = (d*h-g*e)
D = -(b*i-c*h)
E = (a*i-c*g)
F = -(a*h-b*g)
G =(b*f-c*e)
H = -(a*f-c*d)
I = (a*e-b*d)    

det = a*A+b*B+c*C
print("Here is the determinant")
print(det)
if det!=0:
    print("Here is the cofactor matrix:")
    print([A,D, G])
    print([B,E, H])
    print([C,F, I])
    print("Here is the inverse matrix:")
    print([A/det,D/det, G/det])
    print([B/det,E/det, H/det])
    print([C/det,F/det, I/det ])

else:
    print("There is no inverse as the determinant is 0")

print()

print(Det3([[a,b,c],[d,e,f],[g,h,i]]))