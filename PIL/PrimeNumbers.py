#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 22 09:18:21 2024

@author: wneal
"""

from PIL import Image, ImageDraw
import random
from math import *
num = 10000
XHalf = int(num/2)
YHalf = XHalf

def isPrime(n):
    # Check if n is less than 2 (not prime)
    if n < 2:
        return False
    
    # Check for divisibility up to the square root of n
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True

# Create a new image with mode 'RGB' (3x8-bit pixels, true color) and size 200x200
image = Image.new('RGB', (num, num))

# Get a drawing context
#pixels = image.load()
rad = 20
draw = ImageDraw.Draw(image)
for n in range(num):
    if isPrime(n):
        r = n
        theta = n
        i=r*cos(theta)
        j=r*sin(theta)
        if i < XHalf and j < YHalf and i > -XHalf and j > -YHalf:
            draw.ellipse([i-rad+XHalf, j-rad+YHalf, i+rad+XHalf,j+rad+YHalf], fill="red")
            #pixels[i, j] = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
        

# Save the image
image.save('prime_image.png')
