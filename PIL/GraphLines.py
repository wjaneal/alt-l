#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jan 12 10:32:47 2025

@author: wneal
"""

from PIL import Image, ImageDraw



# Create a new image with a white background
width, height = 1000, 1000
image = Image.new('RGB', (width, height), color='white')

# Create a drawing object
draw = ImageDraw.Draw(image)

# Draw the line
line_color = (0, 0, 255)  # Blue color in RGB
line_width = 2
for i in range(100): #Values of range: 0, 1, 2 ....8, 9
    # Define the coordinates of the two points
    point1 = (50+20*i, 50+35*i)  # x, y for point 1
    point2 = (200+12*i, 200+17*i)  # x, y for point 2
    draw.line([point1, point2], fill=line_color, width=line_width)
'''
# Draw points to mark the ends of the line (optional)
point_color = (255, 0, 0)  # Red color in RGB
point_radius = 5
draw.ellipse([point1[0] - point_radius, point1[1] - point_radius, 
              point1[0] + point_radius, point1[1] + point_radius], fill=point_color)
draw.ellipse([point2[0] - point_radius, point2[1] - point_radius, 
              point2[0] + point_radius, point2[1] + point_radius], fill=point_color)
'''
# Display the image
image.show()