#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug  2 14:34:14 2020

@author: wneal
"""

from pippi import dsp

sound1 = dsp.read('sound1.wav')
sound2 = dsp.read('sound2.flac')

# Mix two sounds
both = sound1 & sound2

# Apply a skewed hann Wavetable as an envelope to a sound
enveloped = sound * dsp.win('hann').skewed(0.6)

# Or the same, via a shortcut method on the `SoundBuffer`
enveloped = sound.env('hann')

# Synthesize a 10 second graincloud from the sound, 
# with grain length modulating between 20ms and 2s 
# over a hann shaped curve.
cloudy = enveloped.cloud(10, grainlength=dsp.win('hann', dsp.MS*20, 2))