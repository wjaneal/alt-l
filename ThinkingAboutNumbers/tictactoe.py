#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 18:10:54 2025
"""
print("    |          |    \          \\n____|\n___________________\n________________________")


def printVars(v1, v2, v3):
    print(f"     {v1}  |  {v2}  |  {v3}   ")

def printBars():
    print("        |     |     ")
    
def printLine():
    print("-------------------------")

def clearScreen():
    for i in range(40):
        print()
        
        
t1 = "X" 
t2 = " "
t3 = "O"   
t4 = "X" 
t5 = "X "
t6 = "O" 
t7 = " " 
t8 = "X"
t9 = "O" 

clearScreen()
printBars()
printVars(t1, t2, t3)
printBars()
printLine()
printBars()
printVars(t4, t5, t6)
printBars()
printLine()
printBars()
printVars(t7, t8, t9)
printBars()
