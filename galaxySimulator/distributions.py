#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 12:03:08 2020
Distributions
@author: wneal
"""
from math import *
from random import *
A = 1
k = 1
a = 2.718281828

def f(x):
    return A*a**(-k*x*x)
#Generate a Random Number
repetitions = 10000
results = [0]*20
for i in range(repetitions):
    rn = random()*10
    #print(rn)
    #Apply the Distribution Formula
    #print (f(rn))
    result = int(f(rn)*20)
    results[result]+=1
print(results)



def normalRandom(x):
    sigma = 0.05
    return exp(-(x*x)/sigma)

sigma = 5

xvalues = 10*[0]
yvalues = 10*[0]
for i in range(3000):
    n = random()
    print(n, normalRandom(n))
    x = int(10*n)
    y = int(10*normalRandom(n))
    xvalues[x]+=1
    yvalues[y]+=1
print(xvalues,yvalues)
import matplotlib.pyplot as plt 
plt.bar(yvalues,height = 1)  
plt.xlabel('x - axis') 
plt.ylabel('y - axis') 
plt.xlim(0,100)
# giving a title to the graph 
plt.title('A Graph!') 
plt.show()





