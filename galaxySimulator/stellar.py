from random import *
from math import *
from PIL import Image
from datetime import *

'''
R = 15
D = 100
T = 19
scaleFactor = 6
perspectiveAngle = pi/6
numStars = 10000000
sigma_t = 0.2
sigma_d = 15
xSize = 1000
ySize = 1000
'''
R = 24 #Radius factor of the spiral
D = 50
T = 100
scaleFactor = 1
perspectiveAngle = pi/6
numStars = 1000000
sigma_t = 0.05
sigma_d = 0.05
xSize = 1000
ySize = 1000


currentDate = str(datetime.today())
filename = "spiralGalaxy"+currentDate+".png"
f = open(filename,"w")
img = Image.new('RGB',(xSize,ySize))
pix = img.load()

infofilename = "info"+currentDate+".txt"
finfo = open(infofilename,"w")
finfo.write("Galaxy Simulator\n")
finfo.write("R: ")
finfo.write(str(R))
finfo.write("\n")
finfo.write("D: ")
finfo.write(str(D))
finfo.write("\n")
finfo.write("T: ")
finfo.write(str(T))
finfo.write("\n")
finfo.write("T: ")
finfo.write(str(T))
finfo.write("\n")
finfo.write("Scale Factor: ")
finfo.write(str(scaleFactor))
finfo.write("\n")
finfo.write("Perspective Angle: ")
finfo.write(str(perspectiveAngle))
finfo.write("\n")
finfo.write("numStars: ")
finfo.write(str(numStars))
finfo.write("\n")
finfo.write("Sigma t: ")
finfo.write(str(sigma_t))
finfo.write("\n")
finfo.write("Sigma t: ")
finfo.write(str(sigma_t))
finfo.write("\n")
finfo.write("Sigma d: ")
finfo.write(str(sigma_d))
finfo.write("\n")
finfo.write("X Size: ")
finfo.write(str(xSize))
finfo.write("\n")
finfo.write("Y Size: ")
finfo.write(str(ySize))
finfo.write("\n")


#Defines the radius of the spiral arm based on a parameter, t
def r(t):
    return R*t

def P_prime(t):
    return [R*(cos(t)-t*sin(t)), R*(sin(t)+t*cos(t)),0]

#Return the Point
def P(t):
    return [R*t*cos(t),R*t*sin(t),0]
     
#Return the direction vector
def dV(t):
    return [-R*(sin(t)+t*cos(t)),R*(cos(t)-t*sin(t)),0]

#Return the 3D coordinate of a star based on distributions
def dAdjustment(theta,phi,P,dV,d):
    return[P[0]+d*cos(phi)*cos(theta),P[1]+d*cos(phi)*sin(theta),P[2]+d*sin(phi)]



#Returns a random number based on the normal distribution
def normalRandom(sigma):
    x = random()
    #return exp(-(x*x)/sigma)
    return abs(sqrt(abs(sigma*log(abs(x)))))

def spiralGalaxyStar(sigma_t, sigma_d):
    t = T*normalRandom(sigma_t)
    d = D*normalRandom(sigma_d)
    d = 100*d/r(t)
    currentPoint = P(t)
    currentPointPrime = P_prime(t)
    directionVector = dV(t)
    theta = atan(directionVector[1]/directionVector[0])
    phi = 2*pi*random()
    coordinates = dAdjustment(theta, phi, currentPoint, dV, d)
    return coordinates

def threeDRender(Point, xSize, ySize, perspectiveAngle, scaleFactor):
    #origin = [int(xSize/2),int(ySize/2)]
    origin = [-500,500]
    return [origin[0]+scaleFactor*(Point[0]+cos(perspectiveAngle)*Point[1]),origin[1]+scaleFactor*(Point[2]+sin(perspectiveAngle)*Point[1])]


for i in range(0,numStars):
    Point = spiralGalaxyStar(sigma_t,sigma_d)
    plotCoordinates = threeDRender(Point,xSize,ySize,perspectiveAngle,scaleFactor)
    X, Y = plotCoordinates[0],plotCoordinates[1]
    #print(X,Y)
    X = int(-X)
    Y = int(Y)
    if X in range(xSize) and Y in range(ySize):
        #print(X,Y)
        pix[X,Y] = (255,255,255)
img.save(filename)
f.close()
