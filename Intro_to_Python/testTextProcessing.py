#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 09:54:09 2021

@author: wneal
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb  4 10:25:45 2021

@author: wneal
"""
class textprocessing:
 def init(self):
  self.t = open("./1000.txt","r")
  self.b = open("./Biology.txt","r")
  self.c = open("./Chemistry.txt","r")
  self.p = open("./Physics.txt","r")
  self.s = open("./Science.txt","r")
  self.word1=[]
  self.biology = []
  self.chemistry = []
  self.physics = []
  self.science = []
  self.thousand = []
  self.masterlist = []
 def Thousand(self):
  for line in self.t:
    self.word1 = line.split(" ")#split them into lists
    self.thousand.append(self.word1[0].split("\n")[0])#store
  self.t.close()
  #print(self.thousand)

 def Biology(self):
  for line in self.b:
    word1 = line.split(" ")#split the file into lists
    Newlist=[]
    for item in word1:
        if item != '\n' and item!='':
          Newlist.append(item)#picking up useful information
    if len(Newlist)==3:
        Newlist[2]=int(Newlist[2])#get the frequncy number into integers
        self.biology.append(Newlist)
  self.b.close()
  #print(self.biology)
#The following three function can refer to the notes in function biology

 def Chemistry(self):
  for line in self.c:
    word1 = line.split(" ")
    Newlist=[]
    for item in word1:
        if item != '\n' and item!='':
          Newlist.append(item)
    if len(Newlist)==3:
        Newlist[2]=int(Newlist[2])
        self.chemistry.append(Newlist)
  self.c.close()     
  #print(self.chemistry)
 def Physics(self):
  for line in self.p:
    word1 = line.split(" ")
    Newlist=[]
    for item in word1:
        if item != '\n' and item!='':
          Newlist.append(item)
    if len(Newlist)==3:
        Newlist[2]=int(Newlist[2])
        self.physics.append(Newlist)
  self.p.close()   
  #print(self.physics)  
 def Science(self):
  for line in self.s:
    word1 = line.split(" ")
    Newlist=[]
    for item in word1:
        if item != '\n' and item!='':
          Newlist.append(item)
    if len(Newlist)==3:
        Newlist[2]=int(Newlist[2])
        self.science.append(Newlist)
  self.s.close()
  #print(self.science)      
 def master(self):
     self.masterlist=[]
     print("It may take a couple minutes to run. Please be patient")
     for i in range(len(self.biology)):#This is the first possibility of three presense
         #print("i:",i)
         a=1  #a stands for how many times it appear in the files
         b=0  #b stands for the sum of the frequency number of the frequncy
         try:
            b+=self.biology[i][2]
         except:
            print("There's a bad record1")
         for j in range(len(self.physics)):
            try:
             if self.biology[i][0] == self.physics[j][0]:
                  a+=1
                  b+=self.physics[j][2]
            except:
                  print("There's a bad record2")
         for z in range(len(self.chemistry)):
            try:
             if self.biology[i][0] == self.chemistry[z][0]:
                  a+=1
                  b+=self.chemistry[z][2]
            except:
                    print("There's a bad record3")
         for y in range(len(self.science)):
            try:
             if self.biology[i][0] == self.science[y][0]:
                  a+=1
                  b+=self.science[y][2]
            except:
                    print("There's a bad record4")
         
         if a>=3 and b>=90 and self.biology[i][0] not in self.thousand:
             self.masterlist.append(self.biology[i][0])
     
     for w in range(len(self.physics)):#This is the second possibility of three presense
         #print("w:",w)
         a=1
         b=0
         try:
             b+=self.physics[w][2]
         except:
             print("There's a bad record")
         for m in range(len(self.chemistry)):
            try:
             if self.physics[w][0] == self.chemistry[m][0]:
                  a+=1
                  b+=self.chemistry[m][2]
            except:
                print("There's a bad record")
         for l in range(len(self.science)):
            try:
             if self.physics[w][0] == self.science[l][0]:
                  a+=1
                  b+=self.science[l][2]
            except:
                print("There's a bad record")
         
         if a>=3 and b>=90 and self.physics[w][0] not in self.thousand:
             self.masterlist.append(self.physics[w][0])
             
 def graphic(self):
     f=open("./masterlist.txt","w")#Print the masterlist
     for m in range(len(self.masterlist)):
       f.write(self.masterlist[m])
       f.write("\n")
     f.close()
     print("Done!!!")
     
 def main(self):
     self.init()
     self.Thousand()
     self.Biology()
     self.Chemistry()
     self.Physics()
     self.Science()
     self.master()
     self.graphic()
M=textprocessing()
M.main()
