#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 11:50:01 2020
2D and multiple dimensional lists
@author: wneal
"""

#1D list
numColumns = 10
numRows = 10
list1 = [[i*j for i in range(numColumns)] for j in range(numRows)] #This generates a "times table"
print(list1)


#2D list - 2:
    
grid = [[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
grid = [[0 for i in range(15)] for j in range(15)]
print(grid)




'''
print(list1[5][4])#Accessing a 2D list
print(list1[6][2])

print(6/7)
print(42//5)'''