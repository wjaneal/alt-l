#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 21:55:07 2021

@author: wneal
"""

A = "-3x^5-4x^4+3x^3-2x^2+x-10"
B = A.split("+")
C = []
count = 0
for item in B:
    if count !=0:
        C.append("+")
    if "-" in item:
        D = item.split("-")
        for subItem in D:
            C.append(subItem)
            C.append("-")
        C.pop(len(C)-1)
    else:
        C.append(item)
    count+=1
D = []
for item in C:
    if item != '':
        D.append(item)
print(D)