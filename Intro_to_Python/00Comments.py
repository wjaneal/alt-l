#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 23 09:08:05 2020

@author: wneal
"""

#Python has been used in Physics, Chemistry,
# Mathematics, Robotics Team, 3D Printing, 
#3D Modeling .....

#Start with very simple Python code.....

a = 5
print(a)

#Move on to much more 
#interesting and exciting projects

#We can learn a great deal in a short time
#by organizing our learning.

#Use computer programming to understand
#ideas in mathematics and science

# - The hash tag - is used in Python to
# create a comment that the computer cannot see.


#The input statement
name = input("What is your name?")
favouriteColour = input("What is your favourite colour?")

print("Your name is ",name)
print("Your favourite colour is ",favouriteColour)















