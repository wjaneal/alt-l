#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 11:25:14 2020

@author: wneal
"""

#Input:  How many Players?
players = input("how many people are playing?") 
players = int(players)

#Input: How many rounds?
rounds = input("how many rounds are you playing?")
rounds = int(rounds)

#Input: Player Names (loop that repeats once for each player); append to a list of player names using input.
playerNames = []
for i in range(players):
    print ("player # ",i + 1)
    pn = input("Please enter your name: ")


#List of Stock Names:
stockNames = ["Gold", "Silver", "Grain", "Bonds", "Oil", "Industrial"]

holdings = [[0 for i in range(len(stockNames))] for j in range(players)]
print(holdings)


#2D List of Holdings - What each player owns of each stock
holdings = [[0 for i in range(len(stockNames))] for j in range(players)] #2D list to track players’ assets

#List of Money (5000 for each player)
Money = [5000]*players
#List of Stock Prices (100 each stock)
Prices = [100]*(len(stockNames))
############################################################################
#Set up Functions
############################################################################

#Buy Function
def buy():
    print("we are buying")

#Sell Function
def sell():    
    print ("we are selling")

#Net Worth - Determines how much money you are worth

#Change Prices

#Dividends

#Main Loop
for r in range(rounds):
    print ("it is round ",i + 1)
    for p in range(players):
        print("player", p +1," it is your turn")
        done = False
        while done == False:
            choice = input("what would you like to do? (B)uy, (S)ell or (F)inish")
            if choice == "B":
                buy()
            elif choice == "S":
                sell()
            elif choice == "F": 
                done=True
            else:            
               #Print a message: Please enter "B", "S" or "F"
               print('Please enter “B”, “S” or “F”.')
    #Change the stock prices
    #Determine which stocks give a 'dividend' - add money to players' accounts
    
#End of Game
print("You have completed the stock ticker game, thank you for playing")
