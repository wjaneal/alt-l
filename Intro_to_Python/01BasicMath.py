#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 21:13:12 2020
This is a multi-line comment.  It is not seen by the Python processor
@author: wneal
"""

from math import * #This command imports the mathematics library - more commands available
print(pi)
#This is a comment.  The Python processor does not see it.
print("This is not a comment. The Python processor prints this.")

#This is an example of a simple mathematical model
#Let numApples represent the number of apples.
numApples=5
#Let appleCost represent the price of the apples.
appleCost=1.50
#Let subTotal represent the basic cost of the apples.
subTotal = numApples*appleCost
#Let taxRate represent the rate at which tax is charged.
taxRate = 1.13
#Let grandTotal represent the total cost of the apples with tax.
grandTotal = taxRate*subTotal
print("Here is the grand total: ",grandTotal)
print(6*(3+2)*(55/11+1)) 
print(cos(1)) #This line evaluates and displays the cosine of one radian
print(1/sin(2))  #This line evaluates and displays the cosecant of two radians
print(tan(45*pi/180)) #This line converts 30 degrees to radians and evaluates the tangent.
print(2**3, 3**2,9**9**4)  #This line displays 8 and 9, the results of 2 raised to the power 3 and 3 raised to the power 2.