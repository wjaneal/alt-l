#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  4 12:07:07 2020

@author: wneal
"""
#Place an if-elif-else statement in a loop.  Have it repeat until a condition
#is met.
a = input("Please enter an integer!") # = is the assignment operator - set a's value to 5
a = int(a)
if a == 5: # comparison operator - is a equal to 5?
    print("A is 5!")
elif a > 17:
    print("A is quite large!")
    if a == 20:
        print("...and it is a lucky number!")
else:
    print("Please choose another value for a.")
    if a < 0:
        print("Please do not use negative numbers!!!")
