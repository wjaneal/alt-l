#Checklist for asking a random math question

#Import the random module

#Set 'a' equal to a random integer

#Set 'b' equal to a random integer

#Make a string to represent the math question:

# question = string of 'a', addition sign (in quotes), string of 'b'
question = str(a)+"+"+str(b)+"="
# print the question

# set the 'answer' equal to an input statement

#Set the answer to an integer.

#Use a decision structure (if statement) to 
#test whether the answer is correct
#Print appropriate messages if yes or no.