#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 23 21:22:00 2020
Functions
@author: wneal
"""
from math import *  #This allows us to use math functions
"""
#Example 1 - Math function
#The name of the function is f
    #The argument given to the function is x
    #Note that there is a tab for each line in the function
"""
def f(x):  #def stands for define; note the colon (:)
    return x*x #This line is tabbed
    

#Function call:
answer = f(2)
print("The answer is: ", answer)


#Example 2 - Volume of a Sphere
def sphereVolume(r):
    return (4/3)*pi*r**3

volume = sphereVolume(6300000)
print("The volume of the Earth is ",volume, " metres cubed." )

#Example 3 - Surface area of a Sphere
def sphereSurfaceArea(r):
    return (4)*pi*r**2

surfaceArea = sphereSurfaceArea(6300000)
print("The surface area of the Earth is ",surfaceArea ,"metres squared.")


#Example 4 - Number of atoms in a given number of mols.
def numAtoms(mols):
    return 6.02*10**23*mols


mols = 25
print("There are ",numAtoms(mols), "atoms in ",mols, " mols.")













