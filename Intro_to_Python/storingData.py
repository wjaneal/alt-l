#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 11:03:00 2020
How to store and access data
@author: wneal
"""

#We combine loops and lists in order to store and access data.

#Example 1 - Generate a list of random numbers, 
#then access it, printing it out in the process
from random import * #import the random library
'''
listSize = 100
#Define an empty list
l1 = []
#Populate the list with random numbers
for i in range(listSize):
    l1.append(random())
#Print out the items in the list
for item in l1:
    print(item)
print("The last item in the list is ", l1[-1],".")
print("The third item in the list is:", l1[2])

#Example 2 - Cars
#Populate a list of cars:
cars = ["Mercedes","Porsche","BMW","Toyota","Ford", "Honda","Ferarri","Konigsegg","McLaren","Rimac"]
#Select a random car from the list.
print("Here is the length of the list: ", len(cars))
randIndex = int(random()*len(cars)) #This selects a random index value from the list
print("Here is a random index:", randIndex)
print("Here is a random car from the list: ", cars[randIndex])
'''
#Example 3 - Changing Items in a List
print("This program displays your favourite cars.")
print("It allows you to make changes to your list of favourite cars.")
cars = ["Mercedes","Porsche","BMW","Toyota","Ford", "Honda","Ferarri","Konigsegg","McLaren","Rimac"]
done = False
while done == False:
    print("Here is the list of your favourite cars:")
    for i in range(0,len(cars)):
        print(i, " ", cars[i])
    action = input("Which car would you like to modify? (Enter a number, -1 if you are done")
    action = int(action)
    if action == -1:
        done = True
    else:
        newCar = input("What is the car you would like to insert in the list?")
        cars[action] = newCar


