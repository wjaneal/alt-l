#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 22 14:05:30 2020
Keeping track of assignment marks.
@author: wneal
"""


names = ["Ted","Sue","Eren","Duy"]
assignmentNames = ["Test 1", "Assignment 1","Test 2"]
assignments = [[83,67,98],[92,98,91],[85,78,98],[67,98,78]]


for i in range(0,len(names)):
    print(names[i].ljust(7),end="")
    for j in range(0, len(assignmentNames)):
        print(assignments[i][j]," ",end="")
    print()
    
