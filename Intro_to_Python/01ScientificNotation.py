#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 16 21:42:18 2020
How to use scientific notation.
@author: wneal
"""
from math import * #This command imports the mathematics library - more commands available
#Avogadro's Number
#A = 602300000000000000000000 #Too many zeros!!!!
A = 6.023*10**23 #Scientific Notation
print(A)

#Mass of the Earth
M = 5.9722*10**24

#Gravitational Constant
G = 6.67*10**(-11)
print(G)

#Math Problems:
#What is the volume of the earth's atmosphere in m**3?'
earthRadius = 6.378*10**6 #metres
atmosphereThickness = 100*1000 #metres
r1 = earthRadius
r2 = earthRadius+atmosphereThickness
V1 = (4/3)*pi*r1**3 #Volume of earth
V2 = (4/3)*pi*r2**3 #Volume of earth+atmosphere
V = V2-V1
print("The volume of the earth's atmosphere (m**3) is",V,".")



