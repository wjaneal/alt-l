#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 10:35:18 2021
Analysis of a brief program.
@author: wneal
"""


for a in range(1,10):
    print("Now, we are checking ",a)
    primeCount = 0
    #Checking all numbers up to that number:
    for b in range(1,10):
        if a%b==0: #Does b divide into a evenly?
            #print(a," is divisible by ",b)
            primeCount+=1 #Adds one to the primeCount.
            print(a, "divides evenly by ",b)
        if primeCount > 2:
            print(a, " has been declared 'not prime'; skipping")
            break #Skip the rest of the repetition in this loop
    if primeCount == 2:
        print(a, " is prime")



#This is a loop
for a in range(1,10): #a counts from 1 to 9!?
    print(a)
    

#This is a nested loop
for a in range(1,10): #a counts from 1 to 9!?
    for b in range(1,10):
        print(a,b) #This code executes 81 times
        
    
#The Modulo operator - %
print(42%5) #The % operator asks how much is left over from a division.
print(42%6)
print(42%7)
print(42%8)

primeCount = 0 #Define a variable, primeCount.  Now we may use this variable.

#The Decision Structure - Asks a question
a = 42
b = 6
if a%b==0: #Does b divide into a evenly?
    #Is it true? Yes? Then execute the following commands
    primeCount+=1 #Adds one to the primeCount.
    print(a, "divides evenly by ",b)
            
#Comparison Operators - Compare two variables or values
if a == 4:  #Is a equal to 4?
    print("A is equal to 4")
            
if a > 4:  #Is a greater than 4?
    print("A is greater than 4")
            
if a != 4:  #Is a not equal to 4?
    print("A is not equal to 4")

            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            