#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 20:53:08 2020

@author: wneal
"""

triangles=[]
print("a *******************************")
for a in range(1,1001):#assume that a<=b<c
    for b in range(a,1001):
        c=(a**2+b**2)**(1/2)
        if c==int(c) and c<=1000:
            triangles.append([a,b,c])
print(triangles) #Put this in the left column so it does not repeat
print(len(triangles))
triangles=[]
print("b ______________________________")
for a in range(1,334):#assume that a<b<c   
    for b in range(a,1001-a):#Maybe it is still not an well optimized way?
        c=(a**2+b**2)**(1/2)
        d=a+b+c
        if d==int(d) and d<=1000:
            triangles.append([a,b,c])
print(triangles)
print(len(triangles))