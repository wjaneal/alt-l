#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 20:23:28 2020
Adding series of numbers
@author: wneal
"""
from math import * #This is for using math functions like pi

total=0  #Set the total to 0
value = 0
n = 100
for i in range(1,n+1):
    value = value*3+2 #Recursive formula for a sequence of numbers
    total = total + value #When we add a sequence we get a series
    #print("Value: ",value, "Total: ",total)
print("The sum of the numbers is:",total)


def f(x):
    return 5*x**4-3*x**3+10*x**2-4*x+32


#Lets add rectangle areas:

#    (1)  length of each rectangle is f(xi)
#    (2)  xi is a sequence of numbers: 0,1,2,3,....9
#    (3)  the width of each rectangle is 1.
#    (4)  the area of each rectangle is l*w = f(xi)
#    (5)  Keep a running total using the 'total' variable
total = 0
for xi in range(0,10):
    total = total+f(xi)
    #print(total,f(xi))
print(total)


a = 0
b = 10
n = 1000000
#Let us generalize this code for x0=a to x(n-1)=b and n rectangles
total = 0
w = (b-a)/n  #This is the width of each rectangle
for i in range(0,n): #Create a loop
    xi=a+i*w #The xi value starts at a and adds some rectangle widths
    l = f(xi) #Find the length of the rectangle
    area = l*w #Find the area of the rectangle
    total = total + area #Add the rectangle area to the total
    #print(area, total)
print(total) #Print the final total area.

# As n approaches infinity, A approaches 333.33333........










