#Programming Toolbox:
#Variables:
x=5
name = "Ted"
#Decision Structures:
if x < 10:
    print("This is a small number")
elif x== 10:
    print("This number is just right")
else:
    print("This number is too large")
#Input:
name = input("Hi.  What is your name?")
number = input("Please enter an integer")
number = int(number)  #Typecast to integer
#Print several things:  use commas:
print(x,name, "These are printed on the same line")
#For Loop:
for i in range(0,10):
    print(i)
#While Loop
done = False
while done==False:
    print("Do some things....")
    #....