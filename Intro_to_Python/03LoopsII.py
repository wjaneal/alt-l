#ALT-L Examples
#May 16, 2020
#Loops
#You may use comments (# or '''...''') to activate or deactivate the following code 
#and try it in your Python interpreter. (Spyder, for example)
#Example 1 - Use a for loop to iterate through the items in a List:
#This is a list - a variable with several items.
'''
fruits = ["apple", "banana", "cherry","mango","dragon fruit", "star apple"] #This is a list of four fruit
#This for loop iterates through the list.
for fruit in fruits: #For loops repeat tasks
    print(fruit)




#Example 2 - Use a for loop to iterate through the items in a string (a word - banana)
sentence = "This is a complete sentence."  #The sentence is an example of a string
for letter in sentence:
  print(letter)



#Example 3a - for loop - This for structure loops through the values 0....9 using i as the variable.
#Each time the loop repeats, the value of i increases by 1.
print("Example 3a")
for i in range(10): #i is the index variable - it changes as we iterate through the loop
    print("The current value of i:",i)
    #The changing of the values of i is extremely important in computer science

#Example 3b - This for structure loops through the values 5...24 using i as the index variable.    
print("Example 3b")
for i in range(5,25):
    print(i)

#Example 3c:   
#25, 27, 29, 31
print("Example 3c")
for i in range(25,32,2):  #The third argument is the "step" - we step by 2 here
    print(i)
    

#Example 3d:
#31, 29, 27, 25
print("Example 3d")
for i in range(31,24,-2):#The third argument is the "step" - we step by -2 here
    print(i)
 

#Example 4 - for loop with step greater than one. This prints the numbers 1, 4, ....28,
#stepping by three at each iteration.
for i in range(1,31,3):
    print(i)
'''
#Example 5 - while loop

n=0 #Set the value of n to 0
while n < 1000000000000: #Continues repeating while a condition is true.
    print(n)    #The lines that are 'tabbed' repeat
    n = 3*n-2 #This += adds a quantity to n "The new value of n is ....
    #(some function of the old value of n)
'''   

#Example 6
done = False #done is a Boolean variable - either True or False
while done == False: #While we're not done,....
    a = input("Please enter the password.") #A is a text variable
    if a == "password1": #    == is equal to?
        done = True
    else:
        print("That is incorrect.")
print("You have entered the correct password.")



#Challenge: Write a program that asks a Trivia question.
#Let the program loop until the correct answer is given.
questions = ["What is the capital of France?",
             "What is the largest country?", 
             "What mountain range is the most prominent in Morocco?"
             ]

answers = ["Paris", "Russia", "The Atlas Mountains"]

score = 0
for i in range(0,len(questions)):
    question = questions[i]
    answer = answers[i]
    done = False #done is a Boolean variable - either True or False
    a = input(question)
    if a == answer:
        print("You have answered the question correctly")
        score+=1
    else:
        print("That is incorrect.")
print("Your total score is ", score,".")    
'''





