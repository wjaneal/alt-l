#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 24 08:41:13 2020
Lists
Collections of objects or items under one variable name.
Items are separated by commas.
Lists are contained in square brackets.
Each item is accessed by the list index.
@author: wneal
"""
d1 = {0:1,1:6,2:5,3:8,4:5,"animal":"dog",False:True}  #This is a dictionary to model l1
#print(d1[0])
#print(list(d1.keys()))
#print(list(d1.values()))
#print(int(d1[0]))
#Defining lists
l1 = [1,6,5,8,5] #Integers
l2 = [1.7679, 2.4644, 7.6576, 4.5987, 6.3672] #Floating Point
l3 = ["Polynomial", "Trigonometric", "Exponential", "Logarithmic"] #Strings / text
l4 = [6,8,4]
l5 = [4,1.6777, "Hello", True, (4,555,4333,333), [5,6,7,"Hello"],{0:"How are you?"}] 
#Lists can contain a great number of different types of objects (if you like.)
#Tuples are lists that are 'immutable' - they cannot be changed
 
t1 = (1,6,5,8,5) #This is a tuple - it cannot be changed.
t2 = (1.7679, 2.4644, 7.6576, 4.5987, 6.3672) #Floating Point
t3 = ("Polynomial", "Trigonometric", "Exponential", "Logarithmic") #Strings / text
t4 = (6,8,4)
t5 = (4,1.6777, "Hello", True, (4,555,4333,333), [5,6,7,"Hello"],{0:"How are you?"})

#Accessing List Items - index starts from 0.
print(l1[0]) #This will print "1"
print(l1[3]) #This will print "8"
print(l2[1:3])#index values 1 and 2
print(l3[2:]) #index values 2 and beyond
print(l1[:3]) #index values up to but not including 3
print(l2[-1]) #Last item in the list
print(l3[-2]) #Second last item in the list
print(l1[-3:]) #Starting from the third last item in the list
'''
#Joining Lists (Concatenating)
print(l1+l4)
l6 = l1+l3+l2
print(l6)
print(3*l1+2*l4)
print(2*l3)
     
#Iterating through lists
print(l3)
for item in l3:
    print(item)
   
#The len() function gives us the length of a list
print ("Here is the length of list 1: ", len(l1))
for i in range(0,len(l1)):
    print(l1[i])

print("Now, we list the items in reverse using negative steps:")
for i in range(len(l1)-1,-1,-1): #-1 is the 'last' number we never reach
    #print(i)
    print(l1[i])

   
#List Methods
print(sum(l1)) #This adds all the numbers in the list.
print(l1)
print("Append a 7 to l1:")
l1.append(7) #Append an item to the end of the list
print(l1)
print("Insert a 20 in index location 2:")
l1.insert(2,20) #Inserts a 20 at index 2
print(l1)
print("Print the index of '8' in the list:")
print(l1.index(8)) #Print the index number of 8 in the list
print("Print the index of '5' in the list:")
print(l1.index(5))
print("Pop removes an item at a certain index (3)")
l1.pop(3) #Removes the item at index 3
print(l1)
print("Let's reverse the list:")
l1.reverse() #Reverses all the items in a list
print(l1)
print("Now, let's sort the list:")
l1.sort() #Sorts all the items in a list
print(l1)


#Empty List:
l7 = []
for i in range(0,10):
    l7.append(2*i-5)
    print(l7)
print(l7)


#This list contains a text string, an integer, a tuple of integers,
#a dictionary, a Boolean variable and a floating point variable
#listToSort = ["bear", 5, (4,6,8), {0:"origin"}, True, 5.6555]
#listToSort.sort() #Sorting a list of various data types does not work!
#print(listToSort)

#Place some random numbers in a list:
from random import *
randomList = [] #Create an empty list
for i in range(0,5):
    randomList.append(random())
print("Here is a list of 5 random numbers:")
print(randomList)
randomList.sort()
print("Here is the same list of numbers sorted:")
print(randomList)

#Challenge:
#(1)Create a list of random numbers.  Use a loop to multiply each of the numbers
# in the list by 6; print out the original list and the final list
#(2)Make each of the numbers in the list an integer; print out the list
#(3)Add one to each of the numbers in the list; print out the list

def f(x):
    return 3*x*x*x+2*x*x+x

points = [] #Create an empty list of points
#Generating Points for a Graph.
for x in range(1,11):
    y = f(x)
    points.append((x,y))
print("Here is a list of points on a curve listed as tuples:")
print(points)
    
    


#Boolean sort?
#Create a list of Boolean variables
b = [True, False, False, True, False, True, False]
b.sort()
print(b)
#For each item in the list, turn it into an integer. 
#Note that this does not change the items in the list
for item in(b):
    item = int(item)
print(b)

#A slightly different approach changes the items in the list 
#to integers:  False===0; True===1
for i in range(len(b)):
    b[i]= int(b[i])
print(b)


grid = [[0 for i in range(5)] for j in range(5)]
print(grid)






#print a  list in a reverse order.
aList = [100, 200, 300, 400, 500]
aList = aList[::-1]
print(aList)

#2D list:
grid = [[0 for i in range(15)] for j in range(15)]
print(grid)
'''











