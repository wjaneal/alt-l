#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 21:16:37 2020
Example from COMC math contest.
@author: wneal
"""

#n**k, where n>=2 and k>=2
#values of n: 2,3,4,5,.......
#values of k: 2,3,4,5,.......
max_n = 6
max_k = 6
values = []  #Create an empty list
#This is a nested loop
for n in range(2,max_n+1):
    for k in range(2,max_k+1):
        print(n,k)
        if n**k not in values:
            values.append(n**k) #Append n**k to the list
values.sort()
print(values)