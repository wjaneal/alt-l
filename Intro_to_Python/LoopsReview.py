#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 11 11:15:26 2020

@author: wneal
"""
from random import * #Import the random number library
"""
#Repeated, regular patterns can usually be accomplished with loops
print(100)
print(102)
print(104)
print(106)
#....
#....
print(1000)
#!!!!/????


for i in range(1,11):
    print(i)
    
for i in range(100,1001):
    print(i)
    
for i in range(100,1001,2):
    print(i)
    
for i in range(50,501):
    print(2*i)
    
for i in range(100,1001):
    if i%2==0:
        print(i)
    
#Ask the user how many numbers to input (numNums).
numNums = input("How many numbers would you like to input?")
#Convert numNums to an integer.
numNums = int(numNums)
l1=[] #This is an empty list
#Use a loop to input that many numbers, appending them to l1 (a list)
for i in range(numNums):
    nums = input("Please enter a number.")
    l1.append(nums)
#Print out each of the numbers (use a loop)
for numNums in l1:
    print(numNums)
    
    
#Challenge - have this code only allow integers or floating point
    
    
    
   """ 
#Exercise 11:
'''    
Have the user enter two points, (x1, y1) and (x2, y2) 
as represented by variables x1, y1, x2, y2.  
Use the pythagorean theorem to determine the length of the line.
Side a is the difference between x1 and x2.  
Side b is the difference between y1 and y2.  
Side c, the length of the line, is also the hypotenuse of the triangle.  
Use the code from problem 8 to determine this. 
''' 
"""
from math import *
#Input x1:
print("This script calculates the length of the line from (x1,y1) to (x2,y2)")
x1=input("Please input x1.")
y1=input("Please input y1.")
x2=input("Please input x2.")
y2=input("Please input y2.")
#Typecast the variables to floating point
x1=float(x1)
y1=float(y1)
x2=float(x2)
y2=float(y2)
#Set the values of a and b:
a=x2-x1
b=y2-y1
#Calculate c:
c = sqrt(a**2+b**2)
print("The length of the line is ", c)
"""
listSize = 100

#Generate a list of 10 random numbers
List1 = [0 for i in range(listSize)] #This generates a list of 0s
print(List1) 
for i in range(0,listSize): #This is a way to access all items in a list
    List1[i]=int(random()*6)+1 #This generates a 6-sided die.
print(List1)    

#Generate a list of 10 lists of 10 random numbers
List2 = [[0 for i in range(listSize)] for j in range(listSize)]
print(List2)
for i in range(0,listSize): #This is a way to access all items in a list
    for j in range(0,listSize):
        List2[i][j]=int(random()*6)+1 #This generates a 6-sided die.
print(List2)  

#Generate 10 lists of 10 lists of 10 random numbers
List3 = [[[0 for i in range(listSize)] for j in range(listSize)] for k in range(listSize)]
print(List3)
for i in range(0,listSize): #This is a way to access all items in a list
    for j in range(0,listSize):
        for k in range(0,listSize):
            List3[i][j][k]=int(random()*6)+1 #This generates a 6-sided die.
print(List3)
print(List3[listSize-1][listSize-1][listSize-1])

'''
#Single Loop:
for i in range(0,listSize):
    print(i)

#Double Nested Loop - how do the two loops and their index variables interact?
for i in range(0,listSize):
    for j in range(0,listSize):
        print(i," ",j)


#Triple Nested Loop - how do the two loops and their index variables interact?
for i in range(0,listSize):
    for j in range(0,listSize):
        for k in range(0,listSize):
            print(i," ",j, " ",k)

'''


#Nested Loop - Times Table
for i in range(1,11):
    for j in range(1,11):
        print((str(i*j)).ljust(4),end="")
    print()



