#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 14:12:48 2020
Assignment 6 Toolbox
@author: wneal
"""
from random import *

#Generate a set of numbers
numNumbers = 100
numbers = [] #Create an empty list
for i in range(numNumbers): #Create a loop
    numbers.append(int(random()*100)+1) #Append a random number
print(numbers)#Display the numbers

#Finding the average of a set of numbers:
#Set the Total to 0:
Total = 0
#Create a loop
for n in numbers:
    Total+=n
print("The total of the numbers is:",Total)
print("The average of the numbers is ", Total/numNumbers)

#Create a 2D list:
numRows = 3
numColumns = 4
data = [[int(random()*100)+1 for i in range(numColumns)] for j in range(numRows)]
print(data)

#Add row 2 of a list:
rowTotal = 0
for item in data[2]:
    rowTotal += item
print("The total of row 2 is ",rowTotal)
    
#Add a column 1 of a list:
columnTotal = 0
for item in data:
    columnTotal+= item[1]
print("The total of column 1 is ",columnTotal)
    
#Display the information in a 2D list:
for row in numRows:
    for column in numColumns:
        print(data[row][column]," ",end="")
        #In your program, you may input data into the data list here.
        #Example: print("Please enter the data for",names[row], "for the ", courses[column], "course."})
        #Example: data[row][column]=input("Enter grade here.")
    print()


