#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 12 12:00:34 2020
This green text is a comment. It is contained within
two sets of three quotes.
@author: wneal
"""

'''
This is also a comment.  It has two
sets of three single quotes.
'''

"""
#This is a single line comment.
# x is a variable. It is an integer variable
x = 34 #This is an inline comment.
y = 27
print('Here are the values of x and y:  ', x, ",",y) #Here is an output statement - print
#print statements have brackets (), a list of items to print
# separated by commas
#Use variables to print quantities wherever possible
#This allows us to re-use code
"""



#Variables - Ways of storing data in the program
#a, b and c are integers (no decimal places):
a = 45
b = 65
c = 12345
#d, e, f are floating point numbers (decimal places)
d = 1.4534
e = 1232.233257
f = -0.67474
#Text variables:
name = "Artem"
car = "Honda"
location = 'school'
textVariable = "12345" #This is text even though it looks like a number
#Boolean variables:
answer = True
finished = False

print("Hello!")
#Output - display the values of these variables:
print("Here are the numbers: ", a, b, c, d, e, f)
print('Here is some data:', name, car, location, answer, finished)

'''

#Input - allowing the user to enter data
userName = input("Please enter your name.")
print("The name you entered is: ", userName,".")
    
age = input("Please enter your age.")
print("The age you entered is: ", age,".")
age = int(age) #Typecast the age into an integer
#Typecasting changes the data type
#We need to change the string into an integer
print("In three years your age will be ", age+3)
    
side = input("Please enter the side length of a square.")
side = float(side)#Typecast the height into a floating point variable.
print("The area of the square with side length ",side)
print("is ",side*side,".")



cityName = input("What is the name of your city?")
p = input("How many people live in your city?") #This statement lets you type and hit enter.
p = int(p) #Typecast p to integer
print("The population of your city is ",p)
d = 7500000000/p #This is the ratio 
print("The population of the world is 7500000000.")
print("That is ", d," times more than ",cityName,".")
'''
'''
#Operators - ways to change or compare variables

#arithmetic operators +, -, *, / add, subtract, multiply or divide
# % - modulo operator - remainder of a division

x = 42
print(x+3, x-5, x*7,x/5,x%5)
print(x+3, x-5, x*7,x//5,x%5)

# assignment operator - these change the values of variables
x = 5 # = is the assignment operator
print("Here is the original value of x: ", x)
#other assignment operators:
x += 5 #adds 5 to the value of x
print("Add 5 to x: ", x) #add 5 to x
x -= 5 #subtracts 5 from the value of x
print("Subtract 5 from x: ", x) #subtract 5 from x
x *=5
print("Multiply x by 5: ",x ) #multiply x by 5
x /= 5
print("Divide x by 5: ",x ) # divide x by 5

print("x divided by 2; the remainder: ", x%2)


x=5
# == is a comparison operator:
print(x == 5) #This asks a question - is x equal to 5?
#other comparison operators:
print(x > 5)  #greater than
print(x < 5) #less than
print(x <= 5) #less than or equal to
print(x >= 5) #greater than or equal to 
print(x != 5) #not equal to

#True and False - Boolean quantities
print("True typecasts to 1 as an integer:")
print("False typecasts to 0 as an integer:")
print(int(True)) #The computer considers Boolean values to be 1 or 0
print(int(False))

'''
'''
#Programming Challenge:
Make a program that does the following:
(1) Ask the user to input two integers, a and b
(2) Convert (typecast) a and b to integers.
(3) Make a calculation with a and b:  c=3*a-5*b
(4) Print a message with the value of c

Be careful to remember brackets, equal signs, quotation marks for text
Commas in print statements to separate different items.
'''
