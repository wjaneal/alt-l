#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 18 12:56:34 2020
Functions

A function in Python is a special "subroutine" of a program which
may be "called" to execute when needed within the main program or
even another function.

Arguments sent to functions are variables used by functions to run their 
code. 

Functions may optionally 'return' variables to the line in the program
from which they were called.


Mathematical functions are relations between sets of x and y values for which
each allowed x value has one and only one y value.

Python functions may be used to model mathematical functions.

@author: wneal
"""
from math import * #This is needed for use of exponents (**) in calculations

#Example 1 - Simple function with one print statement, no arguments, no return
def printSomething(): #def.....name of function, (), :
    print("Something") #The lines in a function are tabbed in.
    
printSomething() #This is a function call.  There are brackets required; no arguments are in this call.


#Example 2 - Simple function that prints its argument
#Scope - the concept regarding the 'places' in a program where a variable name applies
#The scope of a variable declared in a function is limited to the function.
def printVariable(x): #This version of 'x' only exists inside the function.
    print(x)  # The function prints the value of x here.
    abc=123
 
   
y=5
printVariable(y) #Send the variable with the function call.
z=63
printVariable(z) #Send the variable with the function call.
#print(abc) #This is outside the scope of the declaration of abc
#abc is only available inside the function

#Example 3 - Mathematical function - linear; returns a response to its argument
def f1(x):
    return 3*x+2  #Returns a function of the form y=mx+b

print(f1(5)) #Print the function call. The return statement sends the 'response' of the function.
print(f1("apple")) #This results in an error - expected a number.

#Example 4 - Mathematical function - quadratic
def f2(x):
    return 3*x**2+4*x-2  #Returns a function of the form y=ax^2+bx+c

for i in range(-10,11): # A loop may repeatedly access a function to complete many calculations.
    print(f2(i))

#Example 5 - Mathematical function - cubic; 
def f3(x):
    return x**3-20*x**2+5*x**1-10*x**0  #Returns a function of the form y=ax^3+bx^2+cx+d
for i in range(-10,30): # A loop may repeatedly access a function to complete many calculations.
    print(f3(i))


#Example 6 - Mathematical function with two variables/arguments:
def g(x,y):    
    return 10*x**2 - y**3 #Returns a number based on a calculation made with x and y
    #This may be used to create 3D maps.
    
   
print(g(5,6))

#Example 7 - Calling another function:
def h(x):
    return x*f1(x)

print(h(5))
'''
#Calling functions
#Functions without returns are just called.
printSomething()
printVariable("Print this, please.")
#Functions with returns may be printed or set equal to a variable:
print("f1(10) is: ", f1(10),".")
print("f2(10) is: ", f2(10),".")
print("f3(10) is: ", f3(10),".")
print("g(5,3) is: ", g(5,3),".")
hvalue = h(10)
print("The value of hvalue is: ", hvalue)

'''