#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 20:05:02 2020
Sequences and Series
@author: wneal
"""

#Example of an arithmetic sequence:
#    5,8,11,14,17,...........
# The first number is 5.  We call this 'a'
# The difference between the numbers is 3
# We call this 'd'
# The number of terms is n. n=5

#We may use a loop to display an arithmetic sequence:
for i in range(1, 6): #This code is not generalized
    print(5+(i-1)*3)

#We may generalize our sequence using a, d and n:
#...and a function
def arithmeticSequence(a,d,n): #This code is generalized
    for i in range(1, n+1):
        print(a+(i-1)*d)
        
arithmeticSequence(1410.5235, -71.24, 420)

#Example of a geometric sequence:
# 2, 6, 18, 54, 162....
#The first number is 2.  We call that 'a'
#The common ratio is 3.  We call this 'r'
#There are five terms (n terms)


#We may use a loop to display a geometric sequence:
for i in range(1, 6): #This code is not generalized
    print(2*3**(i-1))
    
    
#We may generalize our sequence using a, r and n:
#...and a function
def geometricSequence(a,r,n): #This code is generalized
    for i in range(1, n+1):
        print(a*r**(i-1))
    
geometricSequence(20,30,50)  
    


#Series - Added sequences

    
    
    
    
    