#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 30 21:14:02 2020
Challenge Problem:
    
(a) Find all right triangles with integer sides up to a maximum length of 1000 for each side
(b) Find all right triangles with integer sides up to a maximum perimeter of 1000 for each triangle
@author: wneal
"""


def f(x):
    return x*x
def g(x):
    return x**(1/2)

x=5
y=12
c=g(f(x)+f(y))
triangles = [] #Create an empty list of triangles
print("When the length of cathetuses are", x,y)
print("the length of the hypotenuse is", c)


#Toolbox:
    
#Determine if the side is an integer
if c == int(c):
    print(c, "is an integer.")
    triangles.append([x,y,int(c)]) #Add a triangle to the list.
else:
    print(c, " is not an integer.")
    
    
print(triangles)