#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul 23 11:14:02 2020

@author: wneal
"""

applePrice = 1.49
money = 10
maxApples = int(money/applePrice)
print(maxApples)
done = False
while done == False:
    print("Welcome to the apple cart.")
    print("Apples cost $",applePrice,".")
    print("You have $",money,".")
    try:
        numApples = input("How many apples would you like?")
        numApples =  int(numApples)  #Typecast to integer
    except:
        print("Please enter an integer!")
        numApples = 0
    #We need to make a decision:  Can we afford this many apples?
    if numApples <= maxApples and numApples>=0: #Change this line so I can't buy a negative number
        print("You just bought ",numApples, " apples.")
        money -= numApples*applePrice #Take the money for the apples.
        print("You have $",money, " left")
        done = True
        
    else:
        print("You can't afford that many apples.")
    
#Plan to improve the program:
#Put lines 14-26 in a loop:
    
#done = False
#while done == False: 
    #Do some things
    #if ...
        #done=True
    
    
    
    
    
    
    
    
    