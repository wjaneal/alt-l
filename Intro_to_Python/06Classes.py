#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Introduction to Classes and Object-oriented Programming
@author: wneal
"""
from random import * #Random number library.
'''
#Example 1:
#Here is a simple example of a class with two variables:
class sampleClass:
    #Constructor - Initializes the class.
    #This function executes when an instance of the class is created.
    def __init__(self):
        self.var1=5
        self.var2="Hello"
        
#Invoke the class by creating an instance of the class:
S = sampleClass() #S is an instance of the class. 
print(S.var1,S.var2) #

#Example 2:
#Here is a simple example of a class with two variables and a function:
class sampleClass:
    def __init__(self):
        self.var1=5
        self.var2="Hello"
    
    #"welcome" is a function in the class.
    def welcome(self):
        print("Welcome to the sample class!")
    
S = sampleClass() #Create an instance of the class
print(S.var1,S.var2) #Print the variables in the class
S.welcome() #This line calls the 'welcome' function.
#We need to end references to the functions with brackets.
    

#Example 3:
#Here is a simple example of a class with two variables and a function with arguments:
class sampleClass:
    def __init__(self, v1, v2, message):
        self.var1=v1 #Set the class variable self.var1 to 
        self.var2=v2
        self.message = message
        #var3 = 5 #This variable is only used inside the class
        
    def welcome(self):
        print("Welcome to the sample class!")
        print(self.message) #self.message accesses the class variable, self.message.

S = sampleClass(5, "Car", "Have a great day!") #Invoke the class with three arguments.
print(S.var1,S.var2)
S.welcome()
S1 = sampleClass(7, "Van", "Who are you?")
S1.welcome()
'''

class chessGame:  #Define a new class
    #A class is a collection of functions and variables
    #under a common name.
    def __init__(self, player1, player2):
        #The chessGame class is initialized by passing in arguments for each of the players' names
        self.P1 = player1
        self.P2 = player2
        
    def showPlayers(self):
        print("Here are the players in the game:")
        print("Player 1: ",self.P1)
        print("Player 2: ",self.P2)
   
    def runGame(self):
        print("Let the chess game begin")
        done = False #This variable is only in runGame
        while done == False:
            print(self.P1,  " moved")
            print(self.P2, " moved")
            if random()> 0.98:
                result=random()
                if result<0.3:
                    print("Checkmate!",self.P1," wins!")
                elif result >0.7:
                    print("Checkmate! ",self.P2, " wins!")
                else:
                    print("The game is a draw")
                done = True
        print("The game is now done")
            
            
G = chessGame("Ted", "Fred") #G is an instance of the class "chessGame"
G.showPlayers()  
G.runGame()       
            



















            
            
            
            
    
    
