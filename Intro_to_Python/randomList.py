#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  4 11:57:27 2020

@author: wneal
"""


list1 = [] #Make an empty list
cinq = 50    #Create a variable with the value of 5
#Use a for loop to make the list  - in range(0,  cinq  ).
for i in range (0, cinq):
    #In the for loop:  append a random() to the list
    list1.append(random()*6+1)
print(list1)
#Francis: Make a for loop that repeats 5 times
for i in range (0, cinq):
    list1[i] = int(list1[i])
print(list1)
