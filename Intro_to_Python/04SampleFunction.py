
#This is a function
def f(x): #def stands for define. f is the function name
    # x is an argument - a quantity or variable sent to a function
    return 2*x+1 #return "sends" a value back to the function call

#This is also a function.  It has two arguments, q and a.
def question(q,a):
    print(q)
    answer = input("What is your answer?")
    score = 0
    if answer == a:
        print("Correct!")
        score = 1
    else:
        print("Incorrect")
    return score
total = 0
s = question("What is the capital of Kazakhstan?","Nur Sultan")
total +=s
print("Your score is ",total)





