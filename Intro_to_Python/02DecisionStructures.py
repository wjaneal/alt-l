#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 16 07:48:17 2020
Comparison Operators, Logical Operators and Decision Structures
@author: wneal
"""
#Assignment of Variables

x=5 #Set the value of x to 5
'''
#***Comparison Operators***
#‘==’ is a comparison operator.  It asks the question is <first item> equal to <second item>?
#For example:
print(x==5) #Returns True after the previous statement x=5.
print(x==6) #Returns False after the previous statement x=5. 


#***Other Comparison Operators***
print(x > 5) #Returns false > is the greater-than operator 
print(x < 6) #Returns true. < is the less-than operator 
print(x >= 2) #Returns true. >= is the greater-than-or-equal-to operator
print(x <= 0) #Returns false. <= is the less-than-or-equal-to operator
print(x!=10) #Returns true. != is the not-equal-to operator

#***Logical Operators*** (Also Known as Boolean Operators)
print (x>3 and x<10)  #This returns true, assuming x is 5.
print(x<3 or x<10) #This returns true, assuming x is 5 - only one of the two conditions needs to be met.
print(x>3) #This is true
print(not(x>3)) #This will print False

#not(5>3) Returns False. 

This is because 5 is greater than 3 while the not operator reverses the state from True to False or from False to True.’’’  

***Decision Structures***
The previous several sections lead up to the concept of the decision structure.  
The ‘if’ statement is used for making decisions in several languages including Python.
There are several key details to notice in decision structures as they repeat throughout the Python language.  
First, a decision structure consists of the word ‘if’ followed by a comparison expression followed by a colon 
(:).  T
he next line or linest must be tabbed over by one "Tab”. (Spaces may also be used.  
Mixing tabs and spaces may cause problems.)  
Many statements in Python end with a colon and have the next line or lines tabbed over.  
The tabbed lines are a logical block of code that executes as part of the main statement or because of the 
main statement..
'''

#Decision structures
#Decision structures need five items:
#(1)'if'
#(2)Comparison
#(3)Colon
#(4)Tab (tabs) on the following line(s).
#(5) A statement or statements that follow (tabbed)


name="Ted" #This is a string variable
if name=="Ted":  #Main statement - an if statement
    print("The name is 'Ted.'") #This code executes as part of the if statement.
    print('The name is "Ted."')

if x==5: #is x equal to 5? (if, condition and colon(:))
    print("x is 5.")
    print("The lines that are tabbed are printed if the ‘if’ statement is True.")
print("This statement returns to the original level of indentation and will be printed regardless.")

name="Ted"
#Decision Structures - else and elif.
if name=="Ted":  #Main statement - an if statement
    print("The name is ‘Ted.’") 
else:
    print("The name is not 'Ted.'")
x=6
if x==5:
    print("x is 5.")
elif x==6:
    print("x is 6.")
else:
    print("x is neither 5 nor 6.")

#Next: Loops


