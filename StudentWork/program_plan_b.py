#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 29 18:11:29 2020

@author: nikitamanannikov
"""

import PIL
import cv2
import random
import math
from PIL import Image

img = Image.open("dog.jpeg")
#img1 = cv2.imread("/Users/nikitamanannikov/Desktop/dog.jpeg")
i=img.size
print(i)
c=random.randint(1,i[0]*i[1])
g=0
while g<c:
    a=random.randint(1,i[0]-1)
    b=random.randint(1,i[1]-1)
    #print(a)
    #print(b)
    #img.putpixel((random.randint(0,i[0]),random.randint(0,i[1])) , (random.randint(0,255), random.randint(0,255), random.randint(0,255)))
    img.putpixel((a, b), (random.randint(0,255), random.randint(0,255), random.randint(0,255)))
    g=g+1

img.save("modified.jpg")
