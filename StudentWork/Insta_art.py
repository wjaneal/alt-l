#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 25 20:24:23 2020

@author: nikitamanannikov
"""

import cv2
import random
from PIL import Image, ImageDraw
image = cv2.imread("dog.jpeg")
window_name = 'Image'
center_coordinates = (random.randint(0,600), random.randint(0,400)) 
radius = random.randint(1,250)
color = (random.randint(0,255), random.randint(0,255), random.randint(0,255)) 
thickness = 2
image = cv2.circle(image, center_coordinates, radius, color, thickness) 
color1 = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
image = cv2.rectangle(image,(random.randint(0,600), random.randint(0,400)),(random.randint(0,600), random.randint(0,400)),color1,2) 
center_coordinates1 = (random.randint(0,600), random.randint(0,400)) 
radius1 = random.randint(1,250)
color2 = (random.randint(0,255), random.randint(0,255), random.randint(0,255)) 
thickness1 = -1
image = cv2.circle(image, center_coordinates1, radius1, color2, thickness1)
image = cv2.rectangle(image,(random.randint(0,600), random.randint(0,400)),(random.randint(0,600), random.randint(0,400)),(random.randint(0,255), random.randint(0,255), random.randint(0,255)),-1)
center_coordinates = (120, 100) 
axesLength = (random.randint(1,400), random.randint(1,400)) 
angle = random.randint(0,360)
startAngle = random.randint(0,360)
endAngle = random.randint(0,360)
color4 = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
thickness = 2
image = cv2.ellipse(image, center_coordinates, axesLength, angle, startAngle, endAngle, color4, thickness) 
center_coordinates = (120, 100) 
axesLength = (random.randint(1,400), random.randint(1,400)) 
angle = random.randint(0,360)
startAngle = random.randint(0,360)
endAngle = random.randint(0,360)
color4 = (random.randint(0,255), random.randint(0,255), random.randint(0,255))
thickness = -1
image = cv2.ellipse(image, center_coordinates, axesLength, angle, startAngle, endAngle, color4, thickness) 
#image.save("karti1.jpg")
cv2.imshow("Image", image) 
cv2.waitKey(0)
cv2.destroyAllWindows()
