#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 18:34:37 2024


#Template for a basic text-based "adventure game" in Python

@author: wneal
"""

#The following variables include lists of items and a map of the rooms
#The set of all data describing the game is known as the "game state"
directions = ["north", "east", "south", "west"]
rooms = ["Staircase", "Dungeon", "Courtyard", "Portal"]
roomMap = [[-1,1,2,-1], [-1,-1,3,0], [0,-1,-1,-1], [1,-1,-1,-1], [-1,2,-1,3],[-1,1,-1,-1]]
items = ["Sword", "Scroll", "Goblin"]
itemLocations = [0, 2, 1]
currentRoom = 2
loopDone = False

#The following code is known as the main loop of the program.
#The object of the player is to "solve" the game by completing the tasks required therein
#From a coding perspective, the game is "solved" when the loopDone variable is set to "True"
#This happens when the player completes the game's required tasks.
while loopDone == False:
    print("You have entered the", rooms[currentRoom])
    print("You may move in the following directions:")
    
    
    #Show the items that are in the room
    print("You see the following items")
    for i in range(0,len(itemLocations)):
        if itemLocations[i]==currentRoom:
            print(items[i])
            
    #Challenge: show the items that the player has (hint - they are in room "-1")
    
    #List the directions in which the player may proceed
    print("")
    print("You may proceed in the following directions:")
    for i in range(0,4): 
        if roomMap[currentRoom][i] > -1:
            print(directions[i])
    
    #Get input from the player.
    #Challenge - create a two-command text parsing system by adding code as shown in the "textParser.py" file
    print("What would you like to do?")
    action = input()
    commandWords = action.split(" ")
    if len(commandWords)==1:
        if action in directions:
        
            if action == "north": 
                direction = 0
            if action == "east": 
                direction = 1
            if action == "south": 
                direction = 2
            if action == "west": 
                direction = 3
            #A magical statement to switch to the next room
            currentRoom = roomMap[currentRoom][direction]
    if len(commandWords)==2:
         if commandWords[0]=="get":
             print("You said 'get'.")
             if commandWords[1] == "flask":
                 print("You get the flask.")
             print("You said 'say'.")
             #Add code to parse commands here
    
    
    

