#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 24 10:04:15 2024

@author: wneal
"""

#This program reads the yaw angle each second and prints it out.
#Values of the yaw angle range from 1799 (179.9 degrees counterclockwise) 
#to -1799 (-179.9 degrees clockwise)

from hub import motion_sensor
import time
 
#Set the 'up' direction to the top face of the Hub:
motion_sensor.up_face()

#Set the yaw to 0:
motion_sensor.reset_yaw(0)

while(1):
    #Read the tilt angles:
    angles = motion_sensor.tilt_angles()
    #Print the yaw (first item in the tilt_angles list):
    print(angles[0])
    time.sleep(1)
    
    