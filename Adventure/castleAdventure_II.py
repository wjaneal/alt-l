#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 19 2024


#Example of a basic text-based "adventure game" in Python

@author: wneal
"""

#The following variables include lists of items and a map of the rooms
#The set of all data describing the game is known as the "game state"
directions = ["north", "east", "south", "west"]
rooms = ["Staircase", "Dungeon", "Courtyard", "Portal"]
roomMap = [[-1,1,2,-1], [-1,-1,-1,0], [0,-1,-1,-1], [1,-1,-1,-1], [-1,2,-1,3],[-1,1,-1,-1]]
items = ["sword", "scroll", "goblin"]
itemLocations = [0, 2, 1]
currentRoom = 2
loopDone = False

#The following code is known as the main loop of the program.
#The object of the player is to "solve" the game by completing the tasks required therein
#From a coding perspective, the game is "solved" when the loopDone variable is set to "True"
#This happens when the player completes the game's required tasks.
while loopDone == False:
    print("__________________________________________________________________")
    print("You have entered the", rooms[currentRoom])
    print("------------------------------------------------------------------")
      
    #Show the items that are in the room
    print("You see the following items:")
    for i in range(0,len(itemLocations)):
        if itemLocations[i]==currentRoom:
            print(items[i])
    print("------------------------------------------------------------------")        
    #Challenge: show the items that the player has (hint - they are in room "-1")
    
    #List the directions in which the player may proceed
    print("")
    print("You may proceed in the following directions:")
    for i in range(0,4): 
        if roomMap[currentRoom][i] > -1:
            print(directions[i])
    print("------------------------------------------------------------------")
    #Get input from the player.
    #Challenge - create a two-command text parsing system by adding code as shown in the "textParser.py" file
    print("What would you like to do?")
    action = input()
    commandWords = action.split(" ")
    if len(commandWords)==1:
        if action in directions:
        
            if action == "north": 
                direction = 0
            if action == "east": 
                direction = 1
            if action == "south": 
                direction = 2
            if action == "west": 
                direction = 3
            #A magical statement to switch to the next room
            currentRoom = roomMap[currentRoom][direction]
    if len(commandWords)==2:
        verb = commandWords[0]
        noun = commandWords[1]
        if verb =="get":
             #Check if this item even exists:
             if noun in items:
                 #Find the index of the item in the list of items
                 itemIndex = items.index(noun)
                 #Check if the item is in the current room:
                 if itemLocations[itemIndex]==currentRoom:
                     #Pick up the item
                     itemLocations[itemIndex]= -1
                     #Special cases for certain items:
                     if noun == "goblin":
                         print("The goblin does not take kindly to an invasion of its personal space. <further messy details redacted> Unfortunately for you, the game is up. Good luck in your next life.")
                         loopDone = True
             else:
                 #Cause the player to take stock of personal sanity:
                 print("It seems that item does not exist in this universe....")
        if verb == "attack":
            if noun in items:
                #Find the index of the item in the list of items
                itemIndex = items.index(noun)
                
                    
                #Check if the item is in the current room:
                if itemLocations[itemIndex]==currentRoom:
                    swordIndex = items.index("sword")
                    #Check that goblin is in the room 
                    if noun == "goblin" :
                        #Check that the player has the sword:
                        if itemLocations[swordIndex]==-1:
                            print("In a desperate melee, you succeed in vanquishing the goblin.  Nice use of your sword!")
                            #Cast the goblin into the void (room #-2):
                            itemLocations[itemIndex]=-2
                            #Open a pathway from the dungeon to the portal:
                            roomMap[1][2]=3
                        else:
                            print("Uhhh...perhaps your combat skills were not as advanced as you thought! <further messy details redacted>")
                            loopDone = True
                    else:
                        print("Why would you want to attack a",noun,"?")
    
        if verb == "say":
            #Check if the player is in the portal (room 3)
            if currentRoom==3:
                if noun == "away":
                    print("In a bright flash of light, your body is hurled through the transcendent cosmos. After some time, you wake up and realize that it is now time for school. ")
                    loopDone = True
            #Otherwsie, the magic spell does not work as expected, leading to a lengthy stay in a place far far away:
            else:
                print("In a bright flash of light, your body is hurled at near infinite speed towards...the void. Good luck finding your way back home.")
                loopDone = True

