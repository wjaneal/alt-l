#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 18:34:37 2024

@author: wneal
"""

directions = ["north", "east", "south", "west", "up", "down","tuesday"]
rooms = ["basement", "kitchen", "bedroom", "living room", "washroom", "entrance", "office", "exit", "location"]
roomMap = [[-1,-1,-1,-1,3,-1,8], [3,-1,-1,7,-1,-1,-1], [-1,4,-1,-1,-1,3,-1], [5,-1,1,6,2,0,-1], [-1,-1,-1,2,-1,-1,-1],[-1,-1,3,-1,-1,-1,-1],[-1,3,-1,-1,-1,-1,-1],[1,-1,-1,-1,-1,-1,-1],[-1,-1,-1,-1,-1,-1,0]]
items = ["key", "ryft", "gun", "book"]
itemLocations = [1,4,5,0]

currentRoom = 5    
#Repetitive code.....
'''
print("You see the following items")
for i in range(0,4):
    if itemLocations[i]==currentRoom:
            print(items[i])
    print("")
    print("You may proceed in the following directions:")
    for i in range(0,6): 
        if roomMap[currentRoom][i] > -1:
            print(directions[i])
'''
    
loopDone = False
while loopDone == False:
    print("You have entered the", rooms[currentRoom])
    print("")
    for i in range(0,4):
        if itemLocations[i]==currentRoom:
            print("You see the following items:")
            print(items[i])
            print("")
    print("You may proceed in the following directions:")
    for i in range(0,7): 
        if roomMap[currentRoom][i] > -1:
            print(directions[i])
    print("What would you like to do?")
    action = input()
    if action in directions:
        if action == "north": 
            direction = 0
        if action == "east": 
            direction = 1
        if action == "south": 
            direction = 2
        if action == "west": 
            direction = 3
        if action == "up":
            direction = 4
        if action == "down":
            direction = 5
        if action == "tuesday":
            direciton = 6
        #A magical statement to switch to the next room
        currentRoom = roomMap[currentRoom][direction]
     


