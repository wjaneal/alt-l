import motor_pair
from hub import port
from hub import motion_sensor

motor_pair.pair(motor_pair.PAIR_1, port.A, port.B)

#This function turns the robot a certain angle at a certain speed
def turnDegrees(angleToTurn,speed):
    yawValue = 0
    motion_sensor.reset_yaw(0)

    if angleToTurn > 0:
        while yawValue < angleToTurn*10:
            motor_pair.move_tank(motor_pair.PAIR_1, speed*10, 0)
            angles = motion_sensor.tilt_angles()
            yawValue = angles[0]
        motor_pair.move_tank(motor_pair.PAIR_1,0,0)
    else:
        while yawValue > angleToTurn*10:
            motor_pair.move_tank(motor_pair.PAIR_1, 0, speed*10)
            angles = motion_sensor.tilt_angles()
            yawValue = angles[0]
        motor_pair.move_tank(motor_pair.PAIR_1,0,0)

turnDegrees(45,30) #Turn left 45 degrees at 30% speed
turnDegrees(-45,60) #Turn right 45 degrees at 60% speed

#-----------------------------------------------------------------------------------------------------------------------------
# New Program
#-----------------------------------------------------------------------------------------------------------------------------

#This program reads the yaw angle each second and prints it out.
#Values of the yaw angle range from 1799 (179.9 degrees counterclockwise) 
#to -1799 (-179.9 degrees clockwise)

from hub import motion_sensor
import time
 
#Set the 'up' direction to the top face of the Hub:
motion_sensor.up_face()

#Set the yaw to 0:
motion_sensor.reset_yaw(0)

while(1):
    #Read the tilt angles:
    angles = motion_sensor.tilt_angles()
    #Print the yaw (first item in the tilt_angles list):
    print(angles[0])
    time.sleep(1)