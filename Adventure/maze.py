#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 18:34:37 2024


#Template for a basic text-based "adventure game" in Python

@author: wneal
"""

#The following variables include lists of items and a map of the rooms
#The set of all data describing the game is known as the "game state"
directions = ["north", "east", "south", "west"]
rooms = ["basement", "exit", "kitchen", "bedroom", "livingroom","bathroom"]
roomMap = [[-1,-1,-1,2], [-1,5,-1,-1], [4,-1,3,-1], [1,4,5,-1], [-1,2,-1,3],[-1,1,-1,-1]]
items = ["key", "ryft", "gun", "book"]
itemLocations = [2,2,2,4]
currentRoom = 2
loopDone = False

#The following code is known as the main loop of the program.
#The object of the player is to "solve" the game by completing the tasks required therein
#From a coding perspective, the game is "solved" when the loopDone variable is set to "True"
#This happens when the player completes the game's required tasks.
while loopDone == False:
    print("You have entered the", rooms[currentRoom])
    print("You may move in the following directions:")
    
    
    #Show the items that are in the room
    print("You see the following items")
    for i in range(0,4):
        if itemLocations[i]==currentRoom:
            print(items[i])
            
    #Challenge: show the items that the player has (hint - they are in room "-1")
    
    #List the directions in which the player may proceed
    print("")
    print("You may proceed in the following directions:")
    for i in range(0,4): 
        if roomMap[currentRoom][i] > -1:
            print(directions[i])
    
    #Get input from the player.
    #Challenge - create a two-command text parsing system using the te
    print("What would you like to do?")
    action = input()
    
    if action in directions:
        direction = directions.index()
        
        if action == "north": 
            direction = 0
        if action == "east": 
            direction = 1
        if action == "south": 
            direction = 2
        if action == "west": 
            direction = 3
        #A magical statement to switch to the next room
        currentRoom = roomMap[currentRoom][direction]
     


