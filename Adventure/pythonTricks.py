#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  7 16:43:58 2024

@author: wneal
"""
verbs = ["run", "jump", "hide","advocate", "transmogrify", "get"]
nouns = ["flask", "pomegranate", "aardvark", "shipping container"]
itemLocations = [1,2,3,4]
rooms = ["basement", "exit", "kitchen", "bedroom", "livingroom","bathroom"]

#Accessing items in lists
print(rooms[1]) 
print(rooms[-2]) #Counts from the end backwards
print(rooms[2:])
print(rooms[:-4])


print(rooms.index("exit"))
#print(rooms.index("aardvark"))

testCommand = "get flask"
commandWords = testCommand.split(" ")
print(commandWords)
verb = commandWords[0]
noun = commandWords[1]
print(verb)
print(noun)


roomNumber = 1
if verb in verbs:
    if noun in nouns:
        if noun == "flask":
            if itemLocations.index("flask") == roomNumber:
                print("You pick up the flask")
                itemLocations[itemLocations.index("flask")] = -1 #This is the location for "picked up"

    
