#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May  7 16:43:58 2024


This is sample code that may be used to add a two-word text parser to an "adventure game" - a text-based program in Python


@author: wneal
"""
verbs = ["run", "jump", "hide","advocate", "transmogrify", "get"]
nouns = ["flask", "pomegranate", "aardvark", "shipping container"]
itemLocations = [1,2,3,4]
rooms = ["basement", "exit", "kitchen", "bedroom", "livingroom","bathroom"]

#Accessing items in lists
print(rooms[1]) 
print(rooms[-2]) #Counts from the end backwards
print(rooms[2:])
print(rooms[:-4])


print(rooms.index("exit"))
#print(rooms.index("aardvark"))


#The following testCommand variable would be input through an input statement in your adventure game's main loop
testCommand = "get flask"
#The split command breaks a set of words separated by spaces into a list of individual words without spaces.
commandWords = testCommand.split(" ")
print(commandWords)
verb = commandWords[0]
noun = commandWords[1]
#After having "split" the command into a verb and a noun, you may work with the verb and noun as you see fit.
print(verb)
print(noun)


#For example, see the following code....

#Set the room number
roomNumber = 1

#Check whether the first word, the verb, is in the list of allowed verbs:
if verb in verbs:
    #Likewise, check whether the noun is in the list of allowed nowus
    if noun in nouns:
        #If both the verb and noun are in the allowed lists, check individualk cases:
        if noun == "flask":
            #Is the flask  in the current room?
            if itemLocations.index("flask") == roomNumber:
                print("You pick up the flask")
                #If yes, pick up the flask by setting its room location to -1, indicating that it is in the player's personal inventory
                itemLocations[itemLocations.index("flask")] = -1 #This is the location for "picked up"

    
