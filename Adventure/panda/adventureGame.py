
from gameBase import *

#This loads a settings file that ensures that items are not cached, causing Python to reload graphics files every time
#This is great for development as frequent changes to files are made
loadPrcFile('settings.prc')

########################################################################################################################################
#Main Class of the game
########################################################################################################################################
class AdventureGame(gameBase):
    def __init__(self):
        gameBase.__init__(self)
        self.setupAdventure()

    def setupAdventure(self):
        #The following variables include lists of items and a map of the rooms
        #The set of all data describing the game is known as the "game state"
        self.directions = ["north", "east", "south", "west", "n","e","s","w"]
        self.rooms = ["Antechamber", "Dungeon", "Courtyard", "Void"]
        self.roomMap = [[-1,1,2,-1], [-1,-1,-1,-1], [0,-1,-1,-1], [-1,-1,-1,-1]]
        self.items = ["sword", "scroll", "goblin"]
        self.itemLocations = [0, 2, 1]
        self.roomCoordinates = [(60,10,10),(60,-50,3),(0,10,0),(60,-100,0)]

        self.currentRoom = 2
        self.loopDone = False
        self.playerAlive = True
    
    
    def setText(self,textEntered):
        self.textObject.setText(textEntered)
        #The following code is added in this descended class:
        self.processCommand(textEntered)
    
    def roomItems(self):
        itemsList = ""
        for i in range(len(self.itemLocations)):
                if self.itemLocations[i] == self.currentRoom:
                    itemsList+=self.items[i]+" "
        if itemsList == "":
            return "nothing noteworthy!"
        else:
            return itemsList

    def availableDirections(self):
        directionCount = 0
        directionText = ""
        for i in range(4):
            
            if self.roomMap[self.currentRoom][i] > -1:
                directionText+=self.directions[i]+" "
                directionCount+=1 
        if directionCount == 0:
            return "There is no way out of here!!!"
        else:
            return directionText


    def processCommand(self, textEntered):
        print("Processing Command:")
        print(textEntered)
        commandWords = textEntered.split(" ")
        print(commandWords)
        if len(commandWords)==1:
            #Check if the goblin is here:
            if self.currentRoom == self.itemLocations[2]:
                self.playerAlive = False
                self.loopDone = True
                self.setText("Not so fast! The goblin rushes towards you and ...<uhhh...better not elaborate...>.")
            else:
                action = commandWords[0]
                if action in self.directions:
                    if action == "north" or action=="n":
                        direction = 0
                    if action == "east" or action=="e":
                        direction = 1
                    if action == "south" or action=="s":
                        direction = 2
                    if action == "west" or action=="w":
                        direction = 3
                    #A magical statement to switch to the next room
                    self.currentRoom = self.roomMap[self.currentRoom][direction]
                    self.setText("You head "+action+", entering the "+self.rooms[self.currentRoom]+".")
                    x = self.roomCoordinates[self.currentRoom][0]
                    y = self.roomCoordinates[self.currentRoom][1]
                    z = self.roomCoordinates[self.currentRoom][2]
                    self.steve.setPos(x,y,z)
                    self.steve.reparentTo(render)
                if action == "reset":
                    #Drop all objects and widgets into the void:
                    self.reset()
                    #Reload objects and widgets
                    self.setup()
                    #Reset adventure-specific data
                    self.setupAdventure()
        if len(commandWords)==2:
            verb = commandWords[0]
            noun = commandWords[1]
            if verb =="get":
                #Check if this item even exists:
                if noun in self.items:
                     #Find the index of the item in the list of items
                     itemIndex = self.items.index(noun)
                     #Check if the item is in the current room:
                     if self.itemLocations[itemIndex]==self.currentRoom:
                         #Pick up the item
                         self.itemLocations[itemIndex]= -1
                         self.setText("You pick up the "+noun+".")
                         #Special cases for certain items:
                     if noun == "goblin":
                         self.setText("The goblin does not take kindly to an invasion of its personal space.\n <further messy details redacted> Unfortunately for you, the game is up. \n")
                         self.steve.removeNode()
                         self.loopDone = True
                         self.playerAlive = False
                else:
                     #Cause the player to take stock of personal sanity:
                     self.setText("It seems that item does not exist in this universe....")
            if verb == "attack":
                print("Attacking")
                if noun in self.items:
                    #Find the index of the item in the list of items
                    itemIndex = self.items.index("sword")
                    self.setText("In a bright flash of light, your body is hurled at near infinite speed towards...the void.\n Good luck finding your way back home.")
                    playerAlive = False
                    loopDone = True 
                    

                    #Check if the item is in the current room:
                    if self.itemLocations[itemIndex]==self.currentRoom:
                        swordIndex = self.items.index("sword")
                        #Check that goblin is in the room 
                    if noun == "goblin":
                        print("Gobleiooon")
                        #Check that the player has the sword:
                        if self.itemLocations[swordIndex]==-1:
                            self.setText("In a desperate melee, you succeed in vanquishing the goblin.  Nice use of your sword!")
                            #Cast the goblin into the void (room #-2) (not to be confused with the Void, one of the rooms in the adventure):
                            self.itemLocations[itemIndex]=-2
                            self.suh_phere.removeNode()
                            #Open a pathway from the dungeon to the portal:
                            self.roomMap[1][1]=3
                            self.roomMap[1][3]=2
                        else:
                            self.setText("Uhhh...perhaps your combat skills were not as advanced as you thought!\n <further messy details redacted>")
                            self.loopDone = True
                            self.playerAlive = False
                            self.steve.removeNode()
                    if noun == "scroll": 
                        #the scroll runs away and a butterfly lands on your shoulder! a nice little friend c:
                        #self.scrollicus.removeNode()
                        #self.buterfli.reparentTo(render)
                        self.setText("the scroll is running away!!!!!!! but woohoo!!!!!!!!!\n a butterfly!!!!!!!!!!!!!!!!!!!!!!!! a cute little butterfly lands on your sholder\n and you befriend it and now you two are besties for life!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! WOOHOOO!!!!!!! YIPPEEEE@!!!!")
                        self.itemLocations[1]=-2
            else:
                    self.setText("Why would you want to attack a "+noun+"?")
            if verb == "say":
                #Check if the player is in the portal (room 3)
                if self.currentRoom==3:
                    if noun == "away":
                        self.setText("In a bright flash of light, your body is hurled through the transcendent cosmos.\n After some time, you wake up and realize that it is now time for school. ")
                        self.dungeon.removeNode()
                        self.Old_Study = loader.loadModel('Old_Study_II.glb')
                        self.Old_Study.setPos(0,0,0)
                        self.Old_Study.setHpr(0,0,0)
                        self.Old_Study.setScale(5)
                        self.Old_Study.reparentTo(render)
                        self.steve.setPos(17,50,25)
                        self.steve.setHpr(90,0,90)
                        self.steve.setScale(7)
                        self.loopDone = True
                    else:
                        self.setText("Seems your spell-casting skills are a little rusty and you appear deep within a mysterious ocean.\nEven though the immense pressure is unbearable,\n it does not matter as you are suddenly eaten for breakfast by a cosmic coelecanth.")
                        self.playerAlive = False
                        self.loopDone = True
                        self.removeLight()
                #Otherwsie, the magic spell does not work as expected, leading to a lengthy stay in a place far far away:
                else:
                    self.setText("In a bright flash of light, your body is hurled at near infinite speed towards...the void.\n Good luck finding your way back home.")
                    playerAlive = False
                    loopDone = True
       
        self.setFocus()
        if self.currentRoom == 3:
            self.dungeon.removeNode()
        if self.loopDone == False:
            message = "You are in the "+self.rooms[self.currentRoom]+". You see: "+self.roomItems()
        else:
            if self.playerAlive == True:
                message = "Well done!"
            else:
                message = "Enjoy the void!"
        
        message+="\nYou may head in the following directions: "+self.availableDirections()
        
        self.setTextAux(message)
    #clear the text
    def clearText(self):
        self.entry.enterText('')


adventure = AdventureGame()
adventure.run()
