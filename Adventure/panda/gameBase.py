#import direct.directbase.DirectStart
from direct.gui.OnscreenText import OnscreenText
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.showbase.ShowBase import ShowBase
from panda3d.core import WindowProperties
from panda3d.core import TransparencyAttrib
from math import *

#This loads a settings file that ensures that items are not cached, causing Python to reload graphics files every time
#This is great for development as frequent changes to files are made
loadPrcFile('settings.prc')

########################################################################################################################################
#Main Class of the game
########################################################################################################################################
class gameBase(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.setup()
    
    #Load Objects, Lights and Text. Set up camera and controls
    #Add the update function to the program cycle
    def setup(self):
        self.loadObjects()
        self.loadLights()
        self.loadText()
        self.setupCamera()
        self.setupControls()
        taskMgr.add(self.update,"update")

    #This routine is called when the program is reset
    def reset(self):
        #Delete all objects and widgets:
        try:
            self.Old_Study.removeNode()
        except:
            pass
        try:
            self.steve.removeNode()
        except:
            pass
        try:
            self.dungeon.removeNode()
        except:
            pass
        #Revmove loaded data structures from memory
        self.textObject.removeNode()
        self.auxTextObject.removeNode()
        self.mainLightNodePath.removeNode()
        self.ambientLightNodePath.removeNode()
    
    #Remove the lights from memory
    def removeLight(self):
        self.render.clearLight(self.mainLightNodePath)
        self.mainLightNodePath.removeNode()
        self.render.clearLight(self.ambientLightNodePath)
        self.ambientLightNodePath.removeNode()

        
    #This function is called every cycle of the program
    def update(self, task):
        dt = globalClock.getDt()
        #md = self.win.getPointer(0)

        #Set movement speeds
        playerMoveSpeed = 10
        cameraMoveSpeed = 1
        x_movement = 0
        y_movement = 0
        z_movement = 0
        self.cameraSwingFactor = 10
        #Get data about the current position and orientation of the camera
        currentH = self.camera.getH()
        currentP = self.camera.getP()
        currentX = self.camera.getX()
        currentY = self.camera.getY()
        currentZ = self.camera.getZ()
        print(currentH)
        currentAngle = pi*currentH/180
        dx = cameraMoveSpeed*sin(currentAngle)
        dy = cameraMoveSpeed*cos(currentAngle)
        dh = cameraMoveSpeed
        dz = cameraMoveSpeed
        #Adjust camera position according to keyboard input
        if self.keyMap['forward']:
            self.camera.setX(currentX-dx)
            self.camera.setY(currentY+dy)
            #self.steve.setX(currentX+dx)
        if self.keyMap['backward']:
            self.camera.setX(currentX+dx)
            self.camera.setY(currentY-dy)
            
            #self.steve.setX(currentY-dy)
        if self.keyMap['left']:
            self.camera.setHpr(currentH+dh,0,0)
            #self.steve.setY(currentY+dy)
        if self.keyMap['right']:
            self.camera.setHpr(currentH-dh,0,0)
            #self.steve.setY(currentY-dy)
        if self.keyMap['up']:
            self.camera.setPos(currentX,currentY,currentZ+dz)
        #Return that the task has completed successfully
        return task.cont
    
    #Specify the controls of the program    
    def setupControls(self):
        self.keyMap = {
            "forward": False,
            "backward": False,
            "left": False,
            "right": False,
            "up": False,
            "pbackward": False,
            "pleft": False,
            "pright": False,
            
        }

        self.accept('escape',self.releaseMouse)
        self.accept('mouse1',self.captureMouse)
        self.accept('tab',self.setFocus)
        self.accept('arrow_up', self.updateKeyMap, ['forward', True])
        self.accept('arrow_up-up', self.updateKeyMap, ['forward', False])
        self.accept('arrow_down', self.updateKeyMap, ['backward', True])
        self.accept('arrow_down-up', self.updateKeyMap, ['backward', False])
        self.accept('arrow_left', self.updateKeyMap, ['left', True])
        self.accept('arrow_left-up', self.updateKeyMap, ['left', False])
        self.accept('arrow_right', self.updateKeyMap, ['right', True])
        self.accept('arrow_right-up', self.updateKeyMap, ['right', False])
        self.accept('w', self.updateKeyMap, ['up', True])
        self.accept('w-up', self.updateKeyMap, ['up', False])
        self.accept('a', self.updateKeyMap, ['pleft', True])
        self.accept('a-up', self.updateKeyMap, ['pleft', False])
        self.accept('s', self.updateKeyMap, ['pbackward', True])
        self.accept('s-up', self.updateKeyMap, ['pbackward', False])
        self.accept('d', self.updateKeyMap, ['pright', True])
        self.accept('d-up', self.updateKeyMap, ['pright', False])        
        

    #Adjust the keymap according to the current state of the keyboard
    def updateKeyMap(self, key, value):
        self.keyMap[key] = value
    
    #Set the focus on the entry widget - allows for repeated text entries during game play
    def setFocus(self):
        self.entry['focus'] = True

    #Capture the mouse within the window
    def captureMouse(self):
        md = self.win.getPointer(0)
        self.lastMouseX = md.getX()
        self.lastMouseY = md.getY()

        properties = WindowProperties()
        properties.setCursorHidden(True)
        properties.setMouseMode(WindowProperties.M_relative)
        self.win.requestProperties(properties)
        return

    #Release the mouse to the wider operating system
    def releaseMouse(self):
        properties = WindowProperties()
        properties.setCursorHidden(False)
        properties.setMouseMode(WindowProperties.M_absolute)
        self.win.requestProperties(properties)
        return

    #Setup the camera to its default configuration
    def setupCamera(self):

        #Setup for overhead view
        self.disableMouse()
        #self.camera.setPos(25,25,150)
        #self.camera.setHpr(0,-90,90)
        #self.camLens.setFov(80)


        self.camera.setPos(0,0,5)
        self.camera.setHpr(270,0,0)
        self.camLens.setFov(80)

    #Load meshes
    def loadObjects(self):
        self.dungeon = loader.loadModel('dungeon.glb')
        self.dungeon.setScale(5)
        self.dungeon.reparentTo(render)
        
        self.suh_phere = loader.loadModel('suh-phere.glb')
        self.suh_phere.setScale(0.5)
        self.suh_phere.setPos(70,-50,10)
        self.suh_phere.reparentTo(render)
        
        self.steve = loader.loadModel('steve.glb')
        self.steve.setPos(0,-3,4)
        self.steve.setHpr(90,0,0)
        self.steve.setScale(1)
        self.steve.reparentTo(render)
    
    #Initialize the text boxes
    def loadText(self):
        #add some text
        self.bk_text = "Enter a command"
        self.aux_text = "Try n, s, e, w, get, attack, etc. Use one or two words."
        self.textObject = OnscreenText(text=self.bk_text, pos=(0, -0.64), scale=0.06, fg=(1, 0.5, 0.5, 1), align=TextNode.ACenter, mayChange=1)
        self.auxTextObject = OnscreenText(text=self.aux_text, pos=(0, -0.80), scale=0.06, fg=(1, 0.5, 0.5, 1), align=TextNode.ACenter, mayChange=1)
        self.entry = DirectEntry(text = "", scale=.05, command=self.setText, initialText="Type Something", numLines = 3, focus=1, focusInCommand=self.clearText, pos=(0.5,0.5,-0.95))
    
    #Set up lighting
    def loadLights(self):
        #Add a main light - directional
        self.mainLight = DirectionalLight('main light')
        self.mainLightNodePath = render.attachNewNode(self.mainLight)
        self.mainLightNodePath.setHpr(30,-60,0)
        self.render.setLight(self.mainLightNodePath)
        
        #Add secondary light - ambient
        self.ambientLight = AmbientLight('ambient light')
        self.ambientLight.setColor((0.7,0.7,0.7,1))
        self.ambientLightNodePath = render.attachNewNode(self.ambientLight)
        self.render.setLight(self.ambientLightNodePath)
    
    #Update the textbox during game play
    def setText(self,textEntered):
        self.textObject.setText(textEntered)
        #The following code depends on the descended class:
        #self.processCommand(textEntered)
        
    #Update the auxiliary box during game play    
    def setTextAux(self,textEntered):
        self.auxTextObject.setText(textEntered)
    
    #Clear the text box
    def clearText(self):
        self.entry.enterText('')


