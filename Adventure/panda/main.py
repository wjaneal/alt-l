#import direct.directbase.DirectStart
from direct.gui.OnscreenText import OnscreenText
from direct.gui.DirectGui import *
from panda3d.core import *
from direct.showbase.ShowBase import ShowBase
from panda3d.core import WindowProperties
from panda3d.core import TransparencyAttrib
from math import *

#This loads a settings file that ensures that items are not cached, causing Python to reload graphics files every time
#This is great for development as frequent changes to files are made
loadPrcFile('settings.prc')

########################################################################################################################################
#Main Class of the game
########################################################################################################################################
class AdventureGame(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)
        self.setup()

    def setup(self):
        self.loadObjects()
        self.loadLights()
        self.loadText()
        self.setupCamera()
        #self.captureMouse()
        self.setupControls()
        taskMgr.add(self.update,"update")


        #The following variables include lists of items and a map of the rooms
        #The set of all data describing the game is known as the "game state"
        self.directions = ["north", "east", "south", "west", "n","e","s","w"]
        self.rooms = ["Antechamber", "Dungeon", "Courtyard", "Void"]
        self.roomMap = [[-1,1,2,-1], [-1,-1,-1,-1], [0,-1,-1,-1], [-1,-1,-1,-1]]
        self.items = ["sword", "scroll", "goblin"]
        self.itemLocations = [0, 2, 1]
        self.roomCoordinates = [(60,10,10),(60,-50,3),(0,10,0),(60,-100,0)]

        self.currentRoom = 2
        self.loopDone = False
        self.playerAlive = True
    
    def update(self, task):
        dt = globalClock.getDt()
        #md = self.win.getPointer(0)

        playerMoveSpeed = 10
        cameraMoveSpeed = 1

        x_movement = 0
        y_movement = 0
        z_movement = 0

        #mouseX = md.getX()
        #mouseY = md.getY()
        

        self.cameraSwingFactor = 10

        currentH = self.camera.getH()
        currentP = self.camera.getP()
        currentX = self.camera.getX()
        currentY = self.camera.getY()
        
        '''
        if currentX >= 0 and currentY >=0:
            currentTheta = 180*atan(currentX/currentY)/pi
        if currentX < 0 and currentY >= 0:
            currentTheta = 180*atan(currentX/currentY)/pi
        if currentX < 0 and currentY <0:
            currentTheta = 180*atan(currentX/currentY)/pi+180
        if currentX >= 0 and currentY <0:
            currentTheta = 180+180*atan(currentX/currentY)/pi

        if currentTheta > 180:
            while currentTheta > 180:
                currentTheta-=currentTheta
        if currentTheta < -180:
            while currentTheta < -180:
                currentTheta += currentTheta
        '''
        currentTheta = 0
        dx = cameraMoveSpeed
        dy = cameraMoveSpeed

        if self.keyMap['forward']:
            self.camera.setX(currentX+dx)
        if self.keyMap['backward']:
            self.camera.setX(currentX-dx)
        if self.keyMap['left']:
            self.camera.setY(currentY+dy)
        if self.keyMap['right']:
            self.camera.setY(currentY-dx)



        #mouseChangeX = mouseX-self.lastMouseX
        #mouseChangeY = mouseY-self.lastMouseY

        #self.camera.setHpr(currentH-mouseChangeX*dt*self.cameraSwingFactor,-90,0)

        #self.camera.setHpr(currentH-mouseChangeX*dt*self.cameraSwingFactor,min(90,max(-90,currentP-mouseChangeY*dt*self.cameraSwingFactor)),0)

        #self.lastMouseX = mouseX
        #self.lastMouseY = mouseY

        return task.cont
        
    def setupControls(self):

        self.keyMap = {
            "forward": False,
            "backward": False,
            "left": False,
            "right": False,
            "pforward": False,
            "pbackward": False,
            "pleft": False,
            "pright": False,
        }

        self.accept('escape',self.releaseMouse)
        self.accept('mouse1',self.captureMouse)
        self.accept('tab',self.setFocus)
        self.accept('arrow_up', self.updateKeyMap, ['forward', True])
        self.accept('arrow_up-up', self.updateKeyMap, ['forward', False])
        self.accept('arrow_down', self.updateKeyMap, ['backward', True])
        self.accept('arrow_down-up', self.updateKeyMap, ['backward', False])
        self.accept('arrow_left', self.updateKeyMap, ['left', True])
        self.accept('arrow_left-up', self.updateKeyMap, ['left', False])
        self.accept('arrow_right', self.updateKeyMap, ['right', True])
        self.accept('arrow_right-up', self.updateKeyMap, ['right', False])
        self.accept('w', self.updateKeyMap, ['pforward', True])
        self.accept('w-up', self.updateKeyMap, ['pforward', False])
        self.accept('a', self.updateKeyMap, ['pleft', True])
        self.accept('a-up', self.updateKeyMap, ['pleft', False])
        self.accept('s', self.updateKeyMap, ['pbackward', True])
        self.accept('s-up', self.updateKeyMap, ['pbackward', False])
        self.accept('d', self.updateKeyMap, ['pright', True])
        self.accept('d-up', self.updateKeyMap, ['pright', False])        
        


    def updateKeyMap(self, key, value):
        self.keyMap[key] = value
    
    def setFocus(self):
        self.entry['focus'] = True

    def captureMouse(self):
        md = self.win.getPointer(0)
        self.lastMouseX = md.getX()
        self.lastMouseY = md.getY()

        properties = WindowProperties()
        properties.setCursorHidden(True)
        properties.setMouseMode(WindowProperties.M_relative)
        self.win.requestProperties(properties)
        return

    def releaseMouse(self):
        properties = WindowProperties()
        properties.setCursorHidden(False)
        properties.setMouseMode(WindowProperties.M_absolute)
        self.win.requestProperties(properties)
        return

    def setupCamera(self):
        self.disableMouse()
        self.camera.setPos(25,25,150)
        self.camera.setHpr(0,-90,90)
        self.camLens.setFov(80)

    def loadObjects(self):
        self.dungeon = loader.loadModel('dungeon.glb')
        self.dungeon.setScale(5)
        self.dungeon.reparentTo(render)

        self.steve = loader.loadModel('steve.glb')
        self.steve.setPos(0,-3,4)
        self.steve.setHpr(90,0,0)
        self.steve.setScale(1)
        self.steve.reparentTo(render)
    
    def loadText(self):
        #add some text
        self.bk_text = "Enter a command"
        self.aux_text = "Try n, s, e, w, get, attack, etc. Use one or two words."
        self.textObject = OnscreenText(text=self.bk_text, pos=(0, -0.64), scale=0.06, fg=(1, 0.5, 0.5, 1), align=TextNode.ACenter, mayChange=1)
        self.auxTextObject = OnscreenText(text=self.aux_text, pos=(0, -0.80), scale=0.06, fg=(1, 0.5, 0.5, 1), align=TextNode.ACenter, mayChange=1)
        self.entry = DirectEntry(text = "", scale=.05, command=self.setText, initialText="Type Something", numLines = 3, focus=1, focusInCommand=self.clearText, pos=(0.5,0.5,-0.95))
    
    def loadLights(self):
        #Add a main light - directional
        self.mainLight = DirectionalLight('main light')
        self.mainLightNodePath = render.attachNewNode(self.mainLight)
        self.mainLightNodePath.setHpr(30,-60,0)
        self.render.setLight(self.mainLightNodePath)
        
        #Add secondary light - ambient
        self.ambientLight = AmbientLight('ambient light')
        self.ambientLight.setColor((0.7,0.7,0.7,1))
        self.ambientLightNodePath = render.attachNewNode(self.ambientLight)
        self.render.setLight(self.ambientLightNodePath)
        
    def setText(self,textEntered):
        self.textObject.setText(textEntered)
        self.processCommand(textEntered)

    def setTextAux(self,textEntered):
        self.auxTextObject.setText(textEntered)
    
    def roomItems(self):
        itemsList = ""
        for i in range(len(self.itemLocations)):
                if self.itemLocations[i] == self.currentRoom:
                    itemsList+=self.items[i]+" "
        if itemsList == "":
            return "nothing noteworthy!"
        else:
            return itemsList

    def availableDirections(self):
        directionCount = 0
        directionText = ""
        for i in range(4):
            
            if self.roomMap[self.currentRoom][i] > -1:
                directionText+=self.directions[i]+" "
                directionCount+=1 
        if directionCount == 0:
            return "There is no way out of here!!!"
        else:
            return directionText


    def processCommand(self, textEntered):
        print("Processing Command:")
        print(textEntered)
        commandWords = textEntered.split(" ")
        print(commandWords)
        if len(commandWords)==1:
            #Check if the goblin is here:
            if self.currentRoom == self.itemLocations[2]:
                self.playerAlive = False
                self.loopDone = True
                self.setText("Not so fast! The goblin rushes towards you and ...<uhhh...better not elaborate...>.")
            else:
                action = commandWords[0]
                if action in self.directions:
                    if action == "north" or action=="n":
                        direction = 0
                    if action == "east" or action=="e":
                        direction = 1
                    if action == "south" or action=="s":
                        direction = 2
                    if action == "west" or action=="w":
                        direction = 3
                    #A magical statement to switch to the next room
                    self.currentRoom = self.roomMap[self.currentRoom][direction]
                    self.setText("You head "+action+", entering the "+self.rooms[self.currentRoom]+".")
                    x = self.roomCoordinates[self.currentRoom][0]
                    y = self.roomCoordinates[self.currentRoom][1]
                    z = self.roomCoordinates[self.currentRoom][2]
                    self.steve.setPos(x,y,z)
                    self.steve.reparentTo(render)
                if action == "reset":
                    self.setup()
        if len(commandWords)==2:
            verb = commandWords[0]
            noun = commandWords[1]
            if verb =="get":
                #Check if this item even exists:
                if noun in self.items:
                     #Find the index of the item in the list of items
                     itemIndex = self.items.index(noun)
                     #Check if the item is in the current room:
                     if self.itemLocations[itemIndex]==self.currentRoom:
                         #Pick up the item
                         self.itemLocations[itemIndex]= -1
                         self.setText("You pick up the "+noun+".")
                         #Special cases for certain items:
                     if noun == "goblin":
                         self.setText("The goblin does not take kindly to an invasion of its personal space.\n <further messy details redacted> Unfortunately for you, the game is up. \n")
                         self.steve.removeNode()
                         self.loopDone = True
                         self.playerAlive = False
                else:
                     #Cause the player to take stock of personal sanity:
                     self.setText("It seems that item does not exist in this universe....")
            if verb == "attack":
                if noun in self.items:
                    #Find the index of the item in the list of items
                    itemIndex = self.items.index(noun)


                    #Check if the item is in the current room:
                    if self.itemLocations[itemIndex]==self.currentRoom:
                        swordIndex = self.items.index("sword")
                        #Check that goblin is in the room 
                    if noun == "goblin" :
                        #Check that the player has the sword:
                        if self.itemLocations[swordIndex]==-1:
                            self.setText("In a desperate melee, you succeed in vanquishing the goblin.  Nice use of your sword!")
                            #Cast the goblin into the void (room #-2):
                            self.itemLocations[itemIndex]=-2
                            #Open a pathway from the dungeon to the portal:
                            self.roomMap[1][1]=3
                        else:
                            self.setText("Uhhh...perhaps your combat skills were not as advanced as you thought! <further messy details redacted>")
                            self.loopDone = True
                            self.playerAlive = False
                    else:
                        self.setText("Why would you want to attack a ",noun,"?")
            if verb == "say":
                #Check if the player is in the portal (room 3)
                if self.currentRoom==3:
                    if noun == "away":
                        self.setText("In a bright flash of light, your body is hurled through the transcendent cosmos.\n After some time, you wake up and realize that it is now time for school. ")
                        self.dungeon.removeNode()
                        self.Old_Study = loader.loadModel('Old_Study_II.glb')
                        self.Old_Study.setPos(0,0,0)
                        self.Old_Study.setHpr(0,0,0)
                        self.Old_Study.setScale(5)
                        self.Old_Study.reparentTo(render)
                        self.steve.setPos(17,50,25)
                        self.steve.setHpr(90,0,90)
                        self.steve.setScale(7)
                        self.loopDone = True
                #Otherwsie, the magic spell does not work as expected, leading to a lengthy stay in a place far far away:
                else:
                    self.setText("In a bright flash of light, your body is hurled at near infinite speed towards...the void.\n Good luck finding your way back home.")
                    playerAlive = False
                    loopDone = True
       
        self.setFocus()
        if self.currentRoom == 3:
            self.dungeon.removeNode()
        if self.loopDone == False:
            message = "You are in the "+self.rooms[self.currentRoom]+". You see: "+self.roomItems()
        else:
            if self.playerAlive == True:
                message = "Well done!"
            else:
                message = "Enjoy the void!"
        
        message+="\nYou may head in the following directions: "+self.availableDirections()
        
        self.setTextAux(message)
    #clear the text
    def clearText(self):
        self.entry.enterText('')


adventure = AdventureGame()
adventure.run()
