
from gpiozero import Servo
from time import sleep

servo_pin_1 = 27
servo_pin_2 = 17
servo_1 = Servo(servo_pin_1)
servo_2 = Servo(servo_pin_2)
travel = 0.60

def play(servo, x, time):
    try:
        for i in range(x):
            servo.value=travel
            sleep(time)
            servo.value=-travel
            sleep(0.5)
        
    finally:
        servo_2.detach
        servo_1.detach

play(servo_2, 2, 0.5)
play(servo_1, 2, 0.5)
play(servo_2, 2, 0.5)
play(servo_2, 1, 1)
play(servo_1, 2, 0.5)
play(servo_1, 1, 1)
play(servo_1, 1, 0.5)
play(servo_2, 1, 0.5)
play(servo_2, 1, 1)
play(servo_1, 4, 0.5)
play(servo_2, 4, 0.5)
play(servo_1, 4, 0.5)
play(servo_1, 1, 2)
