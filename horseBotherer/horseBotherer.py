#Horse Botherer v.0.1
import bluetooth
import os
os.system("sudo pigpiod")
from gpiozero import AngularServo
from time import sleep
from gpiozero.pins.pigpio import PiGPIOFactory
factory = PiGPIOFactory('127.0.0.1')
from random import random

#nearby_devices = bluetooth.discover_devices()
#for addr in nearby_devices:
#    print("Device Name:", bluetooth.lookup_name(addr))
#    print("Device MAC Address:", addr)
target_device = "D8:37:3B:4A:BD:1F"

# establish a connection with the device
#print("Now Connecting")
#sock = bluetooth.BluetoothSocket(bluetooth.L2CAP)
#sock.connect((target_device, 23))
#print("Connected")

chanceOfBotheration = 0.9 # Percentage Chance of Botheration in a given time interval
timeInterval = 0.5 #How many seconds between possible Botherations?
botherationTypes = ["Flag", "Sound"]
botherationNoises = ["Electric Razor 1", "Electric Razor 2", "Gunshot 1", "Gunshot 2", "Tractor 1", "Tractor 2"]
botherationSounds = ["razor1.mp3", "razor2.mp3", "ShotgunClose.mp3", "ShotgunFar.mp3", "tractor1.mp3","tractor2.mp3"]
botherationFlags = ["Flag 1", "Flag 2"]

servo_pin_1 = 27 #Flag 1 Address
servo_pin_2 = 17 #Flag 2 Address

#Initialize the Flags
servo_1 = AngularServo(servo_pin_1,pin_factory=factory)
servo_2 = AngularServo(servo_pin_2,pin_factory=factory)
servos = [servo_1, servo_2]
flagRestAngle = -90
flagActiveAngle = 90
flagRepetitions = 5

def moveFlag(servo, rest, active,repetitions):
    print("Now moving a flag")
    print(servo)
    for i in range(repetitions):
        servo.angle=active
        sleep(2)
        servo.angle=rest
        sleep(1)

def playSound(soundFile):
    print("Now playing a sound:",soundFile)
    command = "vlc --intf dummy --play-and-exit "+soundFile
    os.system(command)

currentTimeInterval = 0
while 1==1:
    currentTimeInterval+=1
    print("Current Time Interval: ", currentTimeInterval)
    print("Current Elapsed Time: ", currentTimeInterval*timeInterval)
    if random()< chanceOfBotheration:
        botheration = True
    else:
        botheration = False
    print("Botheration?", botheration)
    
    sleep(timeInterval)
    if botheration:
        botherationType = int(len(botherationTypes)*random())
        if botherationTypes[botherationType] =="Flag":
            servoIndex = int(random()*len(servos))
            print("Servo Index:",servoIndex)
            currentServo = servos[servoIndex]
            moveFlag(currentServo, flagRestAngle, flagActiveAngle,flagRepetitions)
        if botherationTypes[botherationType] == "Sound":
            currentSound = int(random()*len(botherationNoises))
            playSound(botherationSounds[currentSound])

