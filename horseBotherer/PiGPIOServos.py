from gpiozero import AngularServo
from time import sleep
from gpiozero.pins.pigpio import PiGPIOFactory
factory = PiGPIOFactory('127.0.0.1')

s1 = AngularServo(17,pin_factory = factory)
s2 = AngularServo(16, pin_factory = factory)

while 1==1:
    sleep(2)
    s1.angle = -90
    s2.angle = 90
    sleep(2)
    s1.angle = 90
    s2.angle = -90
