
from gpiozero import Servo
from time import sleep

servo_pin_1 = 27
servo_pin_2 = 17
servo_1 = Servo(servo_pin_1)
servo_2 = Servo(servo_pin_2) 
rest = -1.05

servo_1.value=rest
servo_2.value=rest