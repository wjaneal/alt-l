
from gpiozero import Servo
from time import sleep

servo_pin_1 = 27
servo_pin_2 = 17
servo_1 = Servo(servo_pin_1)
servo_2 = Servo(servo_pin_2) 

travel = 0.45

def play(servo, x, time, sleept):
    try:
        for i in range(x):
            servo.value=travel
            sleep(time)
            servo.value=-(travel + 0.2) 
            sleep(sleept)
        
    finally:
        servo_2.detach
        servo_1.detach
for i in range(5):
    servo_1.value = 0.9
    servo_2.value = 0
    sleep(3)
    servo_1.value = 0
    servo_2.value = 0.9
    sleep(1)


'''
play(servo_2, 2, 0.5, 1)
play(servo_1, 2, 0.5, 1)
play(servo_2, 2, 0.5, 1)
play(servo_2, 1, 1, 1) 
play(servo_1, 2, 0.5, 1)
play(servo_1, 1, 1, 1)
play(servo_1, 1, 0.5, 1)
play(servo_2, 1, 0.5, 1)
play(servo_2, 1, 1, 1)
play(servo_1, 4, 0.5, 1)
play(servo_2, 4, 0.5, 1)
play(servo_1, 4, 0.5, 1)
play(servo_1, 1, 2, 1)
exit()
'''
