#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 14 09:14:49 2021
Binary File Reader
@author: wneal
"""



#fileToRead = "./t10k-labels-idx1-ubyte"
fileToRead = "./t10k-images.idx3-ubyte"
#fileToRead = "./t10k-images.idx3-ubyte"
#fileToRead = "./t10k-images.idx3-ubyte"

import idx2numpy
from numpy import *
f_read = open(fileToRead, 'rb')
ndarr = idx2numpy.convert_from_file(f_read)
s = idx2numpy.convert_to_string(ndarr)
for b'item' in s:
    print(b'item')