#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 28 09:37:07 2021
This program loads mnist images for a neural network
@author: wneal
"""

from mnist import MNIST
mndata = MNIST('/home/wneal/Downloads/mnist/python-mnist/data')
images, labels = mndata.load_training()



def getImage(i):
    if i > len(images):
        return "Error!!"
    else:
        return images[i], labels[i]


'''
#This code loads 5 images as a test.
for i in range(5):
    for row in range(28):
        for column in range(28):
            if images[i][28*row+column]!=0:
                print("*",end = "")
            else:
                print(" ",end = "")
        print()
    
for i in range(2):
    print(labels[i])
'''