#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 15 09:10:43 2020
Design of a Basic Neuron
The implementation of this code is based on chapters 1 and 2 of 
www.neuralnetworksanddeeplearning.com
Libraries such as numpy were not used.
This code was written as an exercise and not for efficiency.
@author: wneal
"""

#Import the required libraries
from random import * 
from math import * 
from mnist_images import * 
    
#The code is mostly contained in the neuralNetwork class
#The original  code included two classes - the neuralNetwork and the neuron
#Coding this became awkward because the neuron encapsulated individual weights and biases away from each other.
#Redesigning the code around the central lists of weights and biases made it much more manageable.
class neuralNetwork:
    def __init__(self,networkDesign,trainingData,learningRate):
        #Make a list of neurons in each layer:
        self.eta = learningRate #Declare the learning rate
        self.networkDesign = networkDesign  #This is a list of the number of neurons in each layer   
        self.L = len(self.networkDesign)-1 #L is the index of the final layer
        self.layerIndices = []
        layerCount = 0
        for i in range(0,len(self.networkDesign)):
            self.layerIndices.append([layerCount,layerCount+self.networkDesign[i]-1])
            layerCount+=self.networkDesign[i]
        print(self.layerIndices)
        self.generate()
        self.neuronCount = sum(self.networkDesign)
        self.a = [0]*self.neuronCount #Set of inputs for the network
        self.z = [0]*self.neuronCount #Set of inputs for the network
        self.dl = [0]*self.neuronCount
        self.trainingData = trainingData
        
    def generate(self):
        #Generate 3D lists of random weights and biases
        self.weights = []
        self.biases = []
        #Layers of weights are required between the layers of neurons.
        #Thus, L layers of neurons yields L-1 layers
        for l in range(0,len(self.networkDesign)-1):
            w = []
            b = []
            #Create the links to the "current layer"
            for i in range(0,self.networkDesign[l]):
                w1 = []
                b1 = []
                dl1 = []
                #Create the links to the "next layer"
                for j in range(self.networkDesign[l+1]):
                    w1.append(random()) #Append a random 
                    b1.append(random()) #Append a random 
                w.append(w1)
                b.append(b1)
            self.weights.append(w)
            self.biases.append(b)
            
    def calculate(self,inputVector):
        #Set the inputs according to the inputVector
        for i in range(len(inputVector)):
            self.a[i] = self.sigmoid(inputVector[i])
            self.z[i] = inputVector[i]
        #Calculate the outputs of each neuron (the inputs of the subsequent layers) using self.a
        #self.z is the raw output of each neuron
        for currentLayer in range(0,len(self.networkDesign)-1):
            #For each neuron in the current layer...
            for i in range(self.networkDesign[currentLayer+1]):
                z = 0
                i_index = self.layerIndices[currentLayer+1][0]+i
                #For each neuron in the next layer...    
                for j in range(self.networkDesign[currentLayer]):
                    j_index = self.layerIndices[currentLayer][0]+j
                    #Calculate the output of the neuron times the associated weight then add the associated bias:
                    amount = self.a[j_index]*self.weights[currentLayer][j][i]+self.biases[currentLayer][j][i]
                    z += amount
                #Record the raw outputs and the sigmoid outputs
                self.z[i_index] = z
                self.a[i_index] = self.sigmoid(z)
        
        
    def backPropagate(self,outputVector):
        #Set the minimum and maximum values of i based on the last layer of the network:
        i_min = self.layerIndices[-1][0]
        i_max = self.layerIndices[-1][1]
        for i in range(i_min,i_max+1):
            self.dl[i] = (self.a[i]-outputVector[i-self.layerIndices[-1][0]])*self.sigmoidPrime(self.z[i])
        #Count backwards through the layers
        for currentLayer in range(self.L-1,0,-1):
            i_min = self.layerIndices[currentLayer][0]
            i_max = self.layerIndices[currentLayer][1]
            for i in range(i_min,i_max+1):
                j_min = self.layerIndices[currentLayer+1][0]
                j_max = self.layerIndices[currentLayer+1][1]
                deltaSum = 0
                for j in range(j_min,j_max+1):
                    deltaSum += self.weights[currentLayer][i-i_min][j-j_min]*self.dl[j]*self.sigmoidPrime(self.z[i])
                self.dl[i] = deltaSum
        #Gradient Descent          
        for currentLayer in range(len(self.networkDesign)-2,-1,-1):
            i_min = self.layerIndices[currentLayer][0]
            i_max = self.layerIndices[currentLayer][1]
            for i in range(i_min,i_max+1):
                j_min = self.layerIndices[currentLayer+1][0]
                j_max = self.layerIndices[currentLayer+1][1]
                for j in range(j_min,j_max+1):
                    self.weights[currentLayer][i-i_min][j-j_min] -= (self.eta)*self.dl[j]*self.a[i]
                    self.biases[currentLayer][i-i_min][j-j_min] -= self.eta*self.dl[j]
     
    #The sigmoid function - it has a gentle slope which allows the neural network to "learn"   
    def sigmoid(self,x):
        return 1/(1+exp(-x))
    
    #The derivative of the sigmoid function used in the calculations
    def sigmoidPrime(self,x):
        return self.sigmoid(x)*(1-self.sigmoid(x))
    
    def crossEntropy(self,x):
        return x

######################################################################################################################################
#Set up the neural network:
######################################################################################################################################

#This parameter decides how quickly the neural netowrk "learns"
learningRate = 1
            

#Sample data sets for testing - the number system converters are trivial examples which demonstrate the basic correct operation of the neural network

#Binary to hexadecimal converter.
dataSet = [[[0,0,0,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1]],
           [[0,0,0,1],[0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0]],
           [[0,0,1,0],[0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0]],
           [[0,0,1,1],[0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0]],
           [[0,1,0,0],[0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0]],
           [[0,1,0,1],[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0]],
           [[0,1,1,0],[0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0]],
           [[0,1,1,1],[0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0]],
           [[1,0,0,0],[0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0]],
           [[1,0,0,1],[0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0]],
           [[1,0,1,0],[0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0]],
           [[1,0,1,1],[0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0]],
           [[1,1,0,0],[0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0]],
           [[1,1,0,1],[0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0]],
           [[1,1,1,0],[0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0]],
           [[1,1,1,1],[1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]]]

networkDesign = [784,15,10] #This is the design used with mnist data

#Binary to decimal converter
#dataSet = [[[0,0],[0,0,0,1]],[[0,1],[0,0,1,0]],[[1,0],[0,1,0,0]],[[1,1],[1,0,0,0]]]
#networkDesign = [2,3,4]

#This command initializes the neural network with a layers/neurons design, dataset and learning rate 
N = neuralNetwork(networkDesign,dataSet,learningRate) #This calls the __init__
#How many learning repetitions?
epochs = 10
numImages = 1000 #This is for use of the mnist data

#Main learning loop of the neural network
#This code has been adapted to use with the mnist data:
for epoch in range(epochs):
    print("Epoch: ",epoch)
    for i in range(numImages):
        image, label = getImage(i)
        N.calculate(image)
        expectedOutput = 10*[0]
        expectedOutput[label] = 1
        N.backPropagate(expectedOutput)
#####################################################
#Display the results of the learning.


#We need to use the test data here; using training data for the moment - testing
print("Results: ..............................")
for i in range(numImages):
    image,label = getImage(i)
    print("Input: ",)
    N.calculate(image)
    print("Outputs:")
    for j in range(N.networkDesign[-1]):
        print(N.a[N.layerIndices[-1][0]+j])
    print("Expected: ", label)

