#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 15 09:10:43 2020
Design of a Basic Neuron
@author: wneal
"""
from random import *
from math import *


class neuralNetwork:
    def __init__(self,networkDesign):
        #Make a list of neurons in each layer:
        self.networkDesign = networkDesign  #This is a list of the number of neurons in each layer   
        self.L = len(self.networkDesign)-1 #L is the index of the final layer
        self.layerIndices = []
        layerCount = 0
        for i in range(0,len(self.networkDesign)):
            self.layerIndices.append([layerCount,layerCount+self.networkDesign[i]-1])
            layerCount+=self.networkDesign[i]
        print(self.layerIndices)
        self.Network = []  #This will be a list of neurons
        self.bias = 0.1
        self.trainingData = []
        #Set up the back-propagation lists for delta and sigma:
        self.sigma_l = []
        self.delta_l = []
        for i in range(len(self.networkDesign)):
            self.sigma_l.append([0]*self.networkDesign[i])
            self.delta_l.append([0]*self.networkDesidn[i])
   
    def dataImport(self,d):
        self.trainingData = d
        self.n = len(self.trainingData)

    def generate(self):
        for layer in self.networkDesign:
            for i in range(layer):
                self.Network.append(neuron("sigmoid"))
        neuronCount = 0 #Keep track of the index numbers of the neurons
        for layer in range(len(self.networkDesign)-1): #Repeat for each layer except the last.
            for currentNeuron in range(self.networkDesign[layer]):
                for nextNeuron in range(self.networkDesign[layer+1]): #Tie to the next layer
                    weight = 2*random()-1
                    #This makes the connection with a neuron ID, weight and bias:
                    print("Data:", neuronCount, currentNeuron, layer, nextNeuron)
                    self.Network[neuronCount+currentNeuron].next.append([neuronCount+nextNeuron+self.networkDesign[layer], weight, self.bias])
            neuronCount+=self.networkDesign[layer] #Move the count up by one layer
            
    def calculate(self,inputVector):
        for i in range(0,self.networkDesign[0]):
            self.Network[i].inputSum = inputVector[i]
            self.Network[i].output = self.Network[i].getResult()
        layerIndex = 0    
        for layer in range(1,len(self.networkDesign)):
            previousLayerCount = self.networkDesign[layer-1]
            currentLayerCount = self.networkDesign[layer]
            for currentLayerNeuron in range(layerIndex+previousLayerCount,layerIndex+previousLayerCount+currentLayerCount):
                self.Network[currentLayerNeuron].inputSum = 0
                for previousLayerNeuron in range(layerIndex,layerIndex+previousLayerCount):
                    #print("Adding weight for neuron", previousLayerNeuron, " to ", currentLayerNeuron, " ...",self.Network[previousLayerNeuron].next[currentLayerNeuron-layerIndex-previousLayerCount][1])
                    self.Network[currentLayerNeuron].inputSum += self.Network[previousLayerNeuron].next[currentLayerNeuron-layerIndex-previousLayerCount][1]*self.Network[previousLayerNeuron].output
                self.Network[currentLayerNeuron].getResult() #Put the vector sum through the sigmoid
            layerIndex+=self.networkDesign[layer-1]   
    
    def backPropagate(self):
        #Select a data pair based on a random entry in the training data
        r = randint(0,self.n-1)
        inputVector = self.trainingData[r][0]
        outputVector = self.trainingData[r][1]
        print(inputVector,outputVector)
        self.calculate(inputVector)
        #Calculate the Error Vector
        errorVector = [0]*len(outputVector)
        print(errorVector)
        
        #Calculate the change to the weights in the final layer
        
        for i in range(self.layerIndices[self.L][0],self.layerIndices[self.L][1]+1):
            j = i-self.layerIndices[self.L][0]  #Align the errorVector indices with the indices of the neurons in the final layer
            errorVector[j] = outputVector[j]-self.Network[i].output
            self.delta_l[L][j] = errorVector[j]*self.sigmoidPrime(self.Network[i].inputSum)
            
        
        print("Delta_l", delta_l)    
      
        #Calculate the change to the weights in the previous layers
        for l in range(len(self.networkDesign)-2,1,-1):
            minIndex = self.layerIndices[l][0]
            maxIndex = self.layerIndices[l][1]
            #For each Neuron:
            for neuronIndex in range(minIndex,maxIndex+1):
                for nextNeuron in self.Network[neuronIndex].next:
                    self.delta_l[l][neuronIndex]+=
        
        #Calculate the change to the biases in the final layer
      
        #Propagate the bias changes to previous layers.
        

    def displayNeurons(self):
        print("###################Displaying Neurons#####################")
        for n in self.Network:
            print(n.next)
                
    def displayOutputs(self):
        print("*******************Displaying Outputs**********************")
        
        print("Here are the input sums for each neuron")
        for n in self.Network:
            print(n.inputSum)
        print("Here are the outputs for each neuron")
        for n in self.Network:
            print(n.output)
        print("Here are the weights and biases:")
        for n in self.Network:
            print(n.next)
     
    def costFunction(outputLayer, expectedOutputs):
        Cost = 0
        n = len(outputLayer)
        for O in range(len(outputLayer)):
            C+=(outputLayer[O]-expectedOutputs[0])**2
        return Cost
       
    def sigmoid(self,x):
        return 1/(1+exp(-x))
    
    def sigmoidPrime(self,x):
        return self.sigmoid(x)*(1-self.sigmoid(x))
            
class neuron:
    #This constructor creates a new neuron.
    #The constructor should specify the activation 
    #function with a string
    def __init__(self,activationFunction):
        print("We are activating a neuron")
        print("Its activation function is", activationFunction)
        #This allows us to experiment with different activation functions
        self.activationFunction = activationFunction
 
        self.inputSum = 0
        self.output = 0
        #We need to link to previous neurons here:
        self.next = [] #This will be a list of lists.  
        #Each sub-list contains a neuron ID, a weight and a bias
        
    def slope(x):
        h = 0.000000001
        return (self.sigmoid(x+h)-self.sigmoid(x))/h


    #This section      
    def sigmoid(self,x):
        return 1/(1+exp(-x))
    
    def sigmoidPrime(self,x):
        return self.sigmoid(x)*(1-self.sigmoid(x))
    
    def step(self, x):
        if x < 0:
            return 0
        else:
            return 1
        
    def activationCalculate(self, x):
        print("This is the activation function")
        #We select an activation function based on the 
        #neuron's activation function string
        if self.activationFunction == "step":
            return self.step(x)
        if self.activationFunction == "sigmoid":
            return self.sigmoid(x)
    
     
        
    #We need code here to store the result of the activation function - a function
    def getResult(self):
         self.output = self.activationCalculate(self.inputSum)
         return self.output
     

dataSet = [[[0,0],[0,0,0,1]],[[0,1],[0,0,1,0]],[[1,0],[0,1,0,0]],[[1,1],[1,0,0,0]]]
networkDesign = [2,3,4]
N = neuralNetwork(networkDesign) #This calls the __init__
N.dataImport(dataSet)
N.generate() #Creating the weights between particular pairs of neurons
N.backPropagate()
#N.displayNeurons() #Display
#N.displayOutputs() #Display outputs
#N.calculate([1, 0]) #Try an input vector
#print("Here are the outputs of each layer")
#N.displayOutputs() #Show the outputs for each neuron.



#N.displayOutputs() #Show the outputs for each neuron.
