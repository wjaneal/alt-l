#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 12:34:32 2020
Have the user input 5 words. 
Store each new word in a list called ‘words’ using the .append() method. 
Display each of the words alongside the length of each word
@author: wneal
"""

#Define an empty list:
L1 = []
#Set the loop
    #Enter a word
    #Append the word to the list
#For each item in the list:
    #print the item and the length of the item
    


#(1) Use a loop to enter the five words.
numWords = 5
for i in range(0,numWords):
    print("Enter a word here:")


# (2) Enter a word
word = input("Please enter a word.")


# (3) Appending to a list
L1 = ["cat","dog", "parrot"]
L1.append("elephant")
print(L1)


# (4) List all of the items in a list:
#Example 1 - Use a for loop to iterate through the items in a List:
#This is a list - a variable with several items.
fruits = ["apple", "banana", "cherry","mango"]
#This for loop iterates through the list.
for fruit in fruits:
    print(fruit)
    
    
# (5) Printing the length of a word:
word = "special"
print(len(word))

