#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 12:20:57 2020
Find and display all numbers from 1 to 1000 that are even, multiples of 7 and multiples of 19.
@author: wneal
"""

#(1) Finding multiples of 7:

#Generate the multiples of 7:
#7, 14, 21, 28, 35, 42, 49......

for i in range(7,1001,7):
    print(i)


#(2) Generate the multiples of 7 with modulo (%)
for i in range(1,31):
    if i%7==0:
        print(i, " is a multiple of 7")


#(3) Boolean logic:  'and'
from random import * #Import the random module
repeats = 1000
for i in range(0,repeats):
    a = int(random()*10+1)
    b = int(random()*10+1)
    if a == 5 and b == 7:
       print("a is 5 and b is 7!")
    else:
        print("a and b are not 5 and 7 respectively")




#(4) Boolean logic:  and twice
        
from random import * #Import the random module
repeats = 1000
for i in range(0,repeats):
    a = int(random()*10+1)
    b = int(random()*10+1)
    c = int(random()*10+1)
    if a == 5 and b > 7 and c <= 6:
       print("The condition is met! a=",a," b= ",b, " c= ",c)
    else:
        print("The condition is not met! a=",a," b= ",b, " c= ",c)