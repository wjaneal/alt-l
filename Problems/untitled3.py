#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 11:06:04 2020

@author: wneal
"""

#Remainder of a division:
print(40/5)   #8  Division of floating point / real numbers (decimals)
print(42//5)  #8 (Integer division) (No decimals)
print(42%5)   #2 - '%' is the modulo operator - remainder of a division
