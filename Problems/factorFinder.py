#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 20:07:33 2025

@author: wneal
"""

number = 182395879# - Prime!
#number = 18239587957
#number = 587295
factors = [] #Create an empty list

for n in range(1,number+1):
    if number % n == 0: #Modulo
        factors.append(n) #Append
        
print(factors)

'''
def f(x):
    return (x-5)*(x-6)


for i in range(100):
    print(f(i))'''