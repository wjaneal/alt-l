#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 12:26:46 2020

@author: wneal
"""
Total = 0
numNums = 10
print("Please enter ", numNums, " numbers. This program will find the average.")
ordinals = ["first", "second", "third", "fourth", "fifth", "sixth", "seventh", "eight", "ninth", "tenth"]
for i in range(0, numNums):
    print("Please enter your ", ordinals[i]," number.")
    a = float(input())
    Total+=a
print("The average of the ", numNums, " numbers is ", Total/numNums)
