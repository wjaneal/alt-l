#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 12:09:10 2020


Have the user enter five numbers. 
Calculate the average of these numbers and display the result.
@author: wneal
"""


#Puzzle pieces:

#(1) Enter a number using input:
n=input("Please enter a number") #n is a text variable
#Typecase n as a number:
n = int(n) #Typecast n as an integer.


#(2) Finding an average.
Total = 456
numItems = 12
Average = Total/numItems
print(Average)


#(3) How do we do something five times?
numTimes = 5
for i in range(0,numTimes):
    print("Do something")

#(4) Get a total:
#First set 'Total' to 0:
Total = 0
for i in range(0,numTimes):
    #In a loop, adjust the value of total
    Total += i
print(Total)
