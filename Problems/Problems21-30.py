    #!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 18 11:38:32 2020
Problems 21-30
@author: wneal
"""
'''
#Example 1 - Is an integer a factor of another integer?
#Does an integer divide evenly into another integer?
a = 100
b = 25
#Does b divide into a?
#Use the modulo operator
print(a%b) #This provides the remainder of a division
#If the remainder is 0, then 'b' divides into 'a' evenly
#That is, bk = a, k is an integer

#Example 2: 
a = 100000
factors = []
for i in range(1,a//2+1):
    if a%i == 0:
        factors.append(i)
        print(i, "divides ", a, ".")
print(factors)

#Example 3 - Generate multiples of a number
n = 23 #How many multiples will we generate?
a = 7 #Multiples of a
l1 = []
for i in range(1,n+1):
    print(i*a)
    l1.append(i*a)
print(l1)

#Example 4 - Is a number in a list?
b = 91
if b in l1: #The 'in' keyword determines whether a number is 'in' the list
    print(b, " is in the list!")
else:
    print(b, " is not in the list!")
    '''
#Example 5
listSize = 5
#Define an empty list
l1 = []
#Populate the list with random numbers
for i in range(listSize):
    l1.append(random())
print(l1)
smallest = l1[0]
for i in range(1,len(l1)):
    if l1[i]<smallest:
        smallest = l1[i]
print("The smallest item in the list is:", smallest)

#Example 6:
#Mathematical Function
def f(x):
    return 2*x**2+3*x+4

for i in range(0,101):
    print(f(i))








