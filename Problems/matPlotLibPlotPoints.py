#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 20 12:50:09 2020

@author: wneal
"""

import matplotlib.pyplot as plt 
x = [1,2,3,4] 
y = [2,4,1,5] 
plt.plot(x, y)  
plt.xlabel('x - axis') 
plt.ylabel('y - axis') 
# giving a title to the graph 
plt.title('A Graph!') 
plt.show()
