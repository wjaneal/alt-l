# -*- coding: utf-8 -*-
"""
Created on Mon May 11 22:40:03 2020

@author: Aya
"""

'''
# example 11
# determine the length of the line between two numbers 
print("Enter the two points in order") 
a = float(input("a: ")) 
b = float(input("b: ")) 
c = float(input("c: ")) 
d = float(input("d: ")) 

import math 
e = (math.fabs(a - c)) 
f = (math.fabs(b - d)) 
print(e,f) 

g = (math.sqrt(e**2 + f**2)) 
print("The length of the line is:", g)
'''

'''
# example 12 
# determine the value of a1 , a2 , a3 
print("Enter the value of m and b from the equation y = mx+b") 
m = float(input("m: ")) 
b = float(input("b: ")) 

a1 = m 
a2 = -1 
a3 = b
print("a1=", a1) 
print("a2=", a2)
print("a3=", a3) 
'''

'''
# example 13
# determine the value of m and b from the equation a1*x+a2*y+a3=0 
print("Enter the value of a1 , a2 , a3:") 
a1 = float(input("a1: "))
a2 = float(input("a2: ")) 
a3 = float(input("a3: ")) 

m = a1
b = a3
print("m=", m)
print("b=", b) 
'''

'''
# example 14 
# determine the value of m and b 
print("Enter the two points numbers in order") 
x1 = float(input("x1: ")) 
y1 = float(input("y1: ")) 
x2 = float(input("x2: ")) 
y2 = float(input("y2: "))  

m = (y2 - y1) / (x2 - x1) 
print("m =", m)  
print("b =", (y1-m*x1)) 
'''

'''
# example 15 
# determine if the point is on the line 
m = int(2) 
b = int(4) 
x1 = float(input("x1: ")) 
y1 = float(input("y1: ")) 

if y1 == m*x1+b: 
    print("The point is on the line") 
elif y1 != m*x1+b: 
    print("The point is not on the line") 
'''

'''
# example 16 
# list of the squares of the first two thousands integer 
list1=int(2000) 
counter = 1
for counter in range(1,list1):
    a = counter*counter
    if a >= list1:
        break
    print(a) 
'''

'''
# example 17 
# check whether a number is a perfect square or not 
print("Enter a number between 1 and 4000000") 
n = int(input("n: ")) 
import math 
for i in range(1,4000001):
    if n == i*i: 
        print("n is a perfect square!, it equals:", math.sqrt(n)) 
'''

'''
# example 18 
# print from 1 to 5 in a certain pattern  
list1 = ["1", "2", "3", "4", "5"]   
list2 = ["1", "2", "3", "4", "5"]  

for x in list1:
    for y in list2:
        print(x,y) 
'''

'''
# example 19
# print from 1 to 1000 in a certain pattern 
for x in range(1, 1001):
  for y in range(1, 1001): 
    print(x,y)  
'''

'''
# example 20 
# print out all the right angled triangles sides between 1 and 1000 
import math 
for a in range(1, 1001): 
    for b in range(1, 1001): 
        c = math.sqrt(a**2 + b**2) 
        print(a, b, c) 
'''