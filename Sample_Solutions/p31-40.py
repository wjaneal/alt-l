#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun May 17 11:39:20 2020

@author: wneal
"""

#For loops generate arithmetic sequences!
for i in range(0,10): #The default increment or step is 1
    print(i)
    
#a = 0
#d = 1
#n = 10
count = 1
for i in range(1,1000,5): #The step is 5
    print(count, i)
    count+=1
#a = 1
#d = 5
    
# tn = a + (n-1)*d
# t0 = a + 0*d = a = 1
# t1 = a + 1*d = 1+5 = 6
#tn = a + (n-1)*d = 1 + (n-1)*d = 996
#  n-1 = (tn-a)/d
#  n = 1+(tn-a)/d
    
def numTerms(a,d,tn):
    return(int(1+(tn-a)/d))
    
print("The number of terms is: ", numTerms(1,5,996))


#This is an example of a series - the sum of a sequence
S = 0
for i in range(0,10):
    S+=i
    print(i)
print("The sum of the numbers is ", S)
    
    
#