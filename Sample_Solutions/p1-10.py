#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May  6 11:12:00 2020

@author: wneal
"""

#Example 1 - Solution for Problem 3
'''
#Set the total variable:
total = 0
#'  "  quotation marks
#Enter five numbers:
#Use a loop:
for i in range(0,5): #i will be 0,1,2,3,4 in the repetitions in the loop
    number = input("Please enter a number:") #This 'number' is a string
    number = float(number)  #Change it to a floating point number (decimals)
    print("The number you entered was", number)
    total += number #Add the number to the total
print("The total is: ", total)
print("Since there were five items, ")
print("the average is :", total/5, ".")
'''

'''
#Example 2 - Solution for Problem 3 - Enter as many numbers as you like
numbers = input("Please enter a set of numbers separated by spaces.")
print(numbers)
#Break the input into a list by splitting at the spaces:
numbers = numbers.split(" ") #This splits the string into a list of strings
print(numbers)
#Now, use a loop to make each member of the list into a float:
length = len(numbers)
for i in range(0,length):
    numbers[i] = float(numbers[i])  #Change each member of the list to a float
print(numbers)
print("The sum of the numbers is ", sum(numbers), ". ")
print("The average of the numbers is ", sum(numbers)/length, ". ")
'''
"""
#Modulo Arithmetic
print(42%5) #If we divide 42 by 5, what is the remainder?
print(42%7)
print(42%19)
#Sample solution for problem 4
l = [] #Create an empty list
for i in range(1,2001):
    '''if i %2 == 0:
        print(i, "is even (divisible by 2).")
    if i %7 == 0:
        print(i, "is divisible by 7.")
    if i %19 == 0:
        print(i, "is divisible by 19.")'''
    if i%2 == 0 and i%7 == 0 and i%19 == 0: #You can use 'or', 'and', others
        print(l)
        #and and or are called boolean operators
        #print(i, " is divisible by 2, 7 and 19.")
        l.append(i) #Append i to the list
print(l) #Print the list
"""
#Sample solution for problem 6:
#Print out 1,2,4, 8......4096
from math import * #We need to 'import the ** -> exponent operator
for i in range(0,13):
    print (2**i) #** is for exponents
#Sample solution for problem 6: 
x = 1
while x<4097: #Repeats until a condition is false
    print(x)
    x = x * 2 #You may also use x *=2













































